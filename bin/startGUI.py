import sys

try:
    import _preamble
except ImportError:
    sys.exit(-1)

from smartcellsimulator.scripts.startGUI import run
print("starting GUI")
run()

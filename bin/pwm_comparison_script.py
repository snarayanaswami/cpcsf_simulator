'''
Created on 6 Apr 2015

@author: matthias.kauer
'''

import sys

try:
    import _preamble
except ImportError:
    sys.exit(-1)

from smartcellsimulator.scripts.pwm_comparison import run_mini_pwmcomparison, run_med_pwmcomparison
# run_mini_pwmcomparison()
run_med_pwmcomparison()
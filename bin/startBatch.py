import sys

try:
    import _preamble
except ImportError:
    sys.exit(-1)

from smartcellsimulator.scripts.startBatch import run
run()
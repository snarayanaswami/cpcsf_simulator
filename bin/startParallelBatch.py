#solution adapted from:
#http://stackoverflow.com/questions/884650/how-to-spawn-parallel-child-processes-on-a-multi-processor-system

import sys
import os
import multiprocessing
import subprocess
import time 

def work(iteration):
    print('Processing iteration: '+str(iteration))
    print('Parent process: '+ str(os.getppid()))
    print('Process id: '+ str(os.getpid()))
    cmd = [ "python","startBatch.py",str(iteration) ]
    return subprocess.call(cmd, shell=False)

if __name__ == '__main__':

    numberOfIterations=int(sys.argv[1])
        
    num_processes = multiprocessing.cpu_count()
    
    threads = []
    
    iteration=0

    # run until all the threads are done, and there is no data left
    while iteration<numberOfIterations:
        while len(threads) < num_processes and iteration<numberOfIterations:
            # if we aren't using all the processors AND there is still data left to
            # compute, then spawn another thread
    
            p = multiprocessing.Process(target=work,args=[iteration])
            
            iteration+=1
    
            p.start()
    
            print(p, p.is_alive())
    
            threads.append(p)
    
            
        for thread in threads:
            if not thread.is_alive():
                threads.remove(thread)
                        
        time.sleep(1)   
    print("All batch iterations have finished!")         
        
import sys
from cx_Freeze import setup, Executable
# from distutils.core import setup


# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    #choosing this supresses console window on Windows; we still like to see the output there, no?
    # base = "Win32GUI"
    print("base", base)

include_files = ['Readme.md', 'smartcellsimulator/qt/mainwindow.ui']

options = {
        'build_exe': {
            'compressed': True,
            'include_files' : include_files,
            # 'excludes' : ['PyQt4.QtWebKit']
            # "packages":  ['smartcellsimulator', 'smartcellsimulator.scripts',
            #     'smartcellsimulator.scsimu', 'smartcellsimulator.tfdynsimple'],
            }
        }

setup(name = "scsimu",
        version = "100",
        description = "yadda yadda",
        author = "myself and I",
        author_email = "email@someplace.com",
        url = "whatever",
        #Name the folder where your packages live:
        #(If you have other packages (dirs) or modules (py files) then
        #put them into the package directory - they will be found 
        #recursively.)
        packages = ['smartcellsimulator', 'smartcellsimulator.scripts',
            'smartcellsimulator.scsimu', 'smartcellsimulator.tfdynsimple'],
        #'package' package must contain files (see list above)
        #I called the package 'package' thus cleverly confusing the whole issue...
        #This dict maps the package name =to=> directories
        #It says, package *needs* these files.
        package_data = {'smartcellsimulator' : ["qt/*"] },
        #'runner' is in the root.
        executables = [Executable('smartcellsimulator/scripts/startGUI.py', base=base), 
            Executable('smartcellsimulator/scripts/startBatch.py', base=base)],
        # scripts = ["runner"],
        long_description = """Really long text here.""" ,
        options=options,
        #
        #This next part it for the Cheese Shop, look a little down the page.
        #classifiers = []     
        ) 

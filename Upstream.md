##Contributing upstream via git subtree
###General git subtree tutorials

* [Fuentes from medium](https://medium.com/@v/git-subtrees-a-tutorial-6ff568381844)
* [Atlassian article](https://www.atlassian.com/git/articles/alternatives-to-git-submodule-git-subtree/): My favorite

###Caveats I figured out

####Push doesn't have a --squash option even though the manuall says so.
This has been corrected only in a [May 2015 pull request](https://github.com/git/git/commit/6ccc71a9d02726d59c1de818febf85a836f4e418).

####<a id="asdf"></a>Upstream history required for merging (bad object during subtree split or push) 
If there's a merge conflict locally and you cannot split it may be necessary to fetch history from upstream first.
See [Stackoverflow #1](http://stackoverflow.com/questions/12486326/fatal-bad-object-error-using-git-subtree-push) and [Stackoverflow #2](http://stackoverflow.com/questions/5760331/git-subtree-is-not-retaining-history-so-i-cannot-push-subtree-changes-how-can-i) for some background. They point out that local clones have history of upstream via hardlink and are not 100% identical to regular clones/pulls.

These commits actually show in your local history and they also wound up on bitbucket now. See [Stackoverflow #3](http://stackoverflow.com/questions/20102594/git-subtree-push-squash-does-not-squash) for details. Replacing ```git log --all``` with ```git log --branches``` should easy my pain locally. For bitbucket, there's no way around this issue I suppose. Unfortunately, having tags there that refer only to a subrepo isn't actually what I wanted. 

####Avoiding tags in main repo
[makingsoftware blog, comment](https://makingsoftware.wordpress.com/2013/02/16/using-git-subtrees-for-repository-separation/) points out that 
```
git remote add –no-tags
```
avoids bringing in tags from the main repo.

####Concrete steps for pushing upstream
Clone upstream repository locally so you can double-check before you push upwards! 
```
git clone https://matthias_kauer@bitbucket.org/matthias_kauer/transferdyn.git
```
Move back to the downstream (this project's) folder.

Before you can push your changes you may or may not need more history information from the upstream repository into this repository. See [Upstream history](#asdf).
Try without this step but come back here if necessary.
```
git remote add tfdyn C:\dvl\transferdyn --no-tags 
git fetch tfdyn     #try without fetching first!
```
Now localize the SHA of the commit you merged. Chances are you'll see a dangling commit saying *Squashed 'fastsimu/transferdyn/' content from <sha>* somewhere.
As an alternative to using hash values you may also create a tag in the local upstream repo. 
```
git tag upstream-<date> #optional
```
Push to this commit. You can merge the branches in your local copy of the upstream repo later.
```
git subtree push --prefix=fastsimu/transferdyn/ tfdyn 330eccaf79286c1d4f235a64a617d836e29af50d
```
You can now go to the (local) upstream repo and merge your new commits if necessary.
```
git merge <sha>
```
Also consider deleting the <sha> named branch you may have just created.
This seems to be what ```git subtree split``` does. And push is performing that first. 
In my case
```
git branch --all
git branch -d 330eccaf79286c1d4f235a64a617d836e29af50d
```
Once the local upstream repo looks like you want it, you can push all changes online. Consider double-checking your remote settings so you push to the right repo.
```
git remote -v
git push
```

####Concrete steps for pulling from upstream
Add upstream repository.
```
git remote add --no-tags upstream https://bitbucket.org/matthias_kauer/transferdyn.git
```

Pull (includes merge) changes from there:
```
git subtree pull --prefix=smartcellsimulator/transferdyn/ upstream master --squash
```

'''
Created on 02 Apr, 2015

@author: matthias
'''
import unittest
import numpy as np
import random
from ..dyninterface import DynInterface
from ..archinterface import TransferResistance
import itertools


class BatteryDynamicsTest(unittest.TestCase):
    def test_chargeEnergyConversion(self):
        dyns = DynInterface.getImplementations()
        np.random.seed(123)
        
        for dynClass in dyns:
            db = dynClass()  
            with self.subTest(db=db):
                #https://docs.python.org/3/library/unittest.html#distinguishing-test-iterations-using-subtests      
            
                energies = 1 + 100 * np.random.rand(1000)
                for nrg in energies:
                    q1 = db.getCharge(E=nrg)
                    nrg2 = db.getEnergy(Q=q1)
                    q2 = db.getCharge(E=nrg2)
                    self.assertAlmostEqual(q1, q2, places=7,
                       msg="converting charge to energy and back must be equal")
                    
                    self.assertAlmostEqual(nrg, nrg2, places=7,
                       msg="converting energy to charge and back must be equal")
            
    def test_chargeSocConversion(self):
        dyns = DynInterface.getImplementations()
        
        np.random.seed(123)
        for dynClass in dyns:
            dyn = dynClass()
            charges = 0 + np.random.rand(1000) * dyn.param.Qfull
            
            for q in charges:
                soc1 = dyn.getSoc(Q=q)
                q2 = dyn.getCharge(soc=soc1)
                soc2 = dyn.getSoc(Q=q2)
            
            self.assertAlmostEqual(q, q2, places=7,
               msg="converting charge to soc and back must be equal")
            
            self.assertAlmostEqual(soc1, soc2, places=7,
               msg="converting soc to charge and back must be equal")
    
    def test_VoltageEnergyConversion(self):
        dyns = DynInterface.getImplementations()
        
        np.random.seed(123)
        for dynClass in dyns:
            dyn = dynClass()
            
            p = dyn.param
            voltages = p.voltBound[0] + np.random.rand(1000) * p.voltSlope
            
            for v in voltages:
                E1 = dyn.getEnergy(V=v)
                v2 = dyn.getVoltage(E=E1)
                E2 = dyn.getEnergy(V=v2)

                self.assertAlmostEqual(E1, E2, places=7,
                   msg="converting energy to voltage and back must be equal")
                
                self.assertAlmostEqual(v, v2, places=7,
                   msg="converting voltage to energy and back must be equal")

    def test_VoltageSocConversion(self):
        dyns = DynInterface.getImplementations()
        
        np.random.seed(123)
        for dynClass in dyns:
            dyn = dynClass()
            
            p = dyn.param
            voltages = p.voltBound[0] + np.random.rand(1000) * p.voltSlope
            
            for v in voltages:
                soc1 = dyn.getSoc(V=v)
                v2 = dyn.getVoltage(soc=soc1)
                soc2 = dyn.getSoc(V=v2)

                self.assertAlmostEqual(soc1, soc2, places=7,
                   msg="converting soc to voltage and back must be equal")
                
                self.assertAlmostEqual(v, v2, places=7,
                   msg="converting voltage to soc and back must be equal")
                
         
    def test_getImplementations(self):
        
        impl_list = DynInterface.getImplementations()
        
        for impl in impl_list:
            self.assert_(issubclass(impl, DynInterface))


    def test_getTimeReqForBalancingEnergy(self):
        dyns = DynInterface.getImplementations()

        dEvals = [10, 60, 100, 250, 500, 1500];       
        for dynClass in dyns:
            dyn = dynClass() 
            dyn2 = dynClass()
            
            dyn.setLossInclusion(simu_transfer=False, simu_switch=False)
            dyn2.setLossInclusion(simu_transfer=False, simu_switch=False)
            res = TransferResistance(Rtx = 1.0, Rrx=1.5)
            
            for dE in dEvals:
                iP = np.sign(dE)
                Eold = dyn.getEnergy()        
#                 I = np.sign(dE)
                dT = dyn.getBalancingTimeEstTillTarget(toCell=dyn2, resistances=res, iPeakAmps=iP, E=Eold-dE)
                # dyn.applyBalancingCurrent(Rcircuit, I, dT)
                tRet = dyn.sendBalancing(toCell=dyn2, resistances=res, iPeakAmps=iP, tSec=dT)
                Enew = dyn.getEnergy();
                self.assertAlmostEqual(Eold - dE, Enew, places=3, 
                    msg="getTimeReqForBalancingEnergy; Enew != Eold + dE (%f); batt_dyn: %s" % (dE, dyn.getName()))
                        
    def test_balancingStopsAtSocBoundary(self):
        res = TransferResistance(Rtx = 1.0, Rrx=1.5)
        iP = 1.0

        dyns = DynInterface.getImplementations()
        for dynClass in dyns:
            dyn = dynClass() 
            dyn2 = dynClass()

            dyn.setSoc(soc=0.0)
            tRet = dyn.sendBalancing(toCell=dyn2, resistances=res, iPeakAmps=iP, tSec=1.0)
            self.assertEqual(tRet, 0, msg="for soc=0, no more output possible; batt_dyn: %s" %  dyn.getName())
            
            dyn.setSoc(soc=0.7)
            dyn2.setSoc(soc=1.0)
            tRet = dyn.sendBalancing(toCell=dyn2, resistances=res, iPeakAmps=iP, tSec=1.0)
            self.assertEqual(tRet, 0, msg="for soc=1, no more input possible; batt_dyn: %s" %  dyn.getName())
            
            
            dyn.setSoc(soc=0.001)
            dyn2.setSoc(soc=0.25)
            tSend = 100.0
            tRet = dyn.sendBalancing(toCell=dyn2, resistances=res, iPeakAmps=iP, tSec=tSend)
            self.assertLess(tRet, tSend, msg="for soc very small, balancing must stop early batt_dyn: %s" %  dyn.getName())
            self.assertEqual(dyn.param.voltBound[0], dyn.getVoltage(), 
                             msg="for soc very small, balancing must stop at Vmin")
            
            dyn.setSoc(soc=0.5)
            dyn2.setSoc(soc=0.999)
            tSend = 100.0
            tRet = dyn.sendBalancing(toCell=dyn2, resistances=res, iPeakAmps=iP, tSec=tSend)
            self.assertLess(tRet, tSend, msg="for soc very large, balancing must stop early batt_dyn: %s" %  dyn.getName())
            self.assertEqual(dyn2.param.voltBound[1], dyn2.getVoltage(), 
                             msg="for soc very big, balancing must stop at Vmax")
            

## move this test to another file
#     def test_getBalancingTimeEstConsistent(self):
#         dyns = DynInterface.getImplementations()
#         np.random.seed(123)
#         
#         for dynClass in dyns:
#             if issubclass(dynClass, DynBox):
#                 continue #energye time consistency not given for dynbox!
#             dyn = dynClass()
#             dynRx = dynClass()
# 
#             #sending energy must be equal to recv energy (w/o losses)
#             #this doesn't hold for Soc!
#             energies = 1 + 100 * np.random.rand(1000)
#             for dE in energies:
#                 ttx, itx = dyn.getBalancingTimeEstTillTarget(1.0, E=dyn.getEnergy()-dE)
#                 trx, irx = dynRx.getBalancingTimeEstTillTarget(1.0, E=dynRx.getEnergy()+dE)
#             
#                 self.assertAlmostEqual(ttx, trx, places=4, 
#                     msg='for transfer of fixed amount of energy: trx != ttx; dyn: %s' % dyn.getName())


#### copy this over to dyn const volt at some point
#     def test_applyCurrentReturnTimeConsistent(self):
#         dyns = DynInterface.getImplementations()
#     
#         dTs = [0.5, 5, 10, 250, 500];
#         currents = [-1.0, -0.5, 0.5, 1.0]  
#             
#         for dynClass in dyns:
#             dyn = dynClass()
#            test_dyn.setLossInclusion(simu_transfer=False, simu_switch=False)
#             Rcircuit = 1.0;
#             for dT in dTs:
#                 for cur in currents: 
#                     tRet = dyn.applyBalancingCurrent(Rcircuit=Rcircuit,iPeakAmps=cur, tSec=dT)
#                     self.assertEqual(tRet, dT, 
#                     msg="If no boundary is hit, applyBalancingCurrent should return full dT, dyn: %s" %dyn.getName())
#                     
#                     tRet = dyn.applyLoadCurrent(iAmps=cur, tSec=dT)
#                     self.assertEqual(tRet, dT, 
#                         msg="If no boundary is hit, applyLoadCurrent should return full dT, dyn: %s" % dyn.getName());
                
        
    def test_applyLoadCurrent(self):
        dyns = DynInterface.getImplementations()
        for dynclass in dyns:
            dyn = dynclass()

            #TODO: Include value exceeding boundary
            
            times = [1200 * random.random() for i in range(100)]
            currents = [-1.0, 1.0]
            for i, t in zip(currents, times):
                dyn.setSoc(soc=0.5)
                tRet = dyn.applyLoadCurrent(iAmps=i, tSec=t)
                self.assertEqual(t, tRet, msg="for times that are short enough (don't hit thresh) tRet=t")
            
            dyn.setSoc(soc=0.0)
            tRet = dyn.applyLoadCurrent(iAmps=-1.0, tSec=1.0)
            self.assertEqual(tRet, 0, msg="for soc=0, no more output possible")
            
            dyn.setSoc(soc=1.0)
            tRet = dyn.applyLoadCurrent(iAmps=1.0, tSec=1.0)
            self.assertEqual(tRet, 0, msg="for soc=1, no more input possible")

            dyn.setSoc(soc=0.999)
            tRet = dyn.applyLoadCurrent(iAmps=1.0, tSec=100.0)
            self.assertLess(tRet,100, msg="for soc=0.999, only small input possible")
            self.assertAlmostEqual(dyn.getVoltage(), dyn.param.voltBound[1], places=4,
                             msg="for soc very big, load current must close to Vmax (Vmax-tfL to be precise)")

            dyn.setSoc(soc=0.001)
            tRet = dyn.applyLoadCurrent(iAmps=-1.0, tSec=100.0)
            self.assertLess(tRet,100, msg="for soc=0.001, only small output possible")
            self.assertAlmostEqual(dyn.param.voltBound[0], dyn.getVoltage(), places=4, 
                             msg="for soc very small, load current must stop close to Vmin (-tfl to be precise)")


    def test_LossInclusionWorks(self):
        dynClasses = DynInterface.getImplementations()
        for dynClass in dynClasses:
            for simulvl in itertools.product([False, True], [False, True]):       
                dyns = [dynClass() for i in range(2)]
                for c in range(2):
                    dyns[c].setLossInclusion(simu_transfer=simulvl[0], simu_switch=simulvl[1])
                    
                
                Rcirc = 0.05; iPeak = 1.0; txtime = 0.25
                res = TransferResistance(Rtx=Rcirc*0.9, Rrx=Rcirc*1.1)
                dyns[0].sendBalancing(toCell=dyns[1], resistances=res, iPeakAmps=iPeak,
                         tSec=txtime)
                
                if(simulvl[0]): #transfer losses must be there
                    for c in range(2):
                        self.assertGreater(dyns[c].losses.tfL, 0.0, msg="transfer losses > 0")
                      
                if(simulvl[1]): #transfer losses must be there
                    for c in range(2):
                        self.assertGreater(dyns[c].losses.swL, 0.0, msg="switching losses > 0")
                        
#     @unittest.skip("too harsh")
    def test_transfer_loss_is_constant(self):
        dynClasses = DynInterface.getImplementations()

        Rcirc = 0.05; iPeak = 1.0; txtime = 0.25
        res = TransferResistance(Rtx=Rcirc*0.9, Rrx=Rcirc*1.1)
        for dynClass in dynClasses:
            dyns = [dynClass() for i in range(4)]
            for c in range(2):
                dyns[c].setLossInclusion(simu_transfer=True, simu_switch=False)
            for c in range(2,4):
                dyns[c].setLossInclusion(simu_transfer=False, simu_switch=False)
                     
            Ebeg = dyns[0].getEnergy()
            dyns[0].sendBalancing(toCell=dyns[1], resistances=res, iPeakAmps=iPeak,
                     tSec=txtime)

            dyns[2].sendBalancing(toCell=dyns[3], resistances=res, iPeakAmps=iPeak,
                     tSec=txtime)
            self.assertEqual(dyns[0].losses.tfL, dyns[2].losses.tfL, msg="tfl must be equal for equal transfer")
            

    def test_transfer_losses_are_Energy_difference(self):
        
        simParamList = itertools.product(
            0.1 + 3.0 * np.random.rand(4), #currents
            0.2 + 0.5 * np.random.rand(4,2), #startSocs
#             0.1 + 0.8 * np.random.rand(4), #startSoc2
#             0.001 + 2.0 * np.random.rand(4), #resistances
            [0.1, 10.0, 250.0], #tx times
                                         )

        for iPeak, soc, txtime in simParamList:
            Rcirc = 1.5 / iPeak #must ensure that iPeak * R < V
            print(iPeak, soc, Rcirc, txtime)

#         Rcirc = 0.05; iPeak = 10; txtime = 100.0
            res = TransferResistance(Rtx=Rcirc*0.9, Rrx=Rcirc*1.1)
            for dynClass in DynInterface.getImplementations():
                dyns = [dynClass() for _ in range(2)]
                dyns[0].setSoc(0.7); dyns[1].setSoc(0.4)
                for c in range(2):
                    dyns[c].setLossInclusion(simu_transfer=True, simu_switch=False)
                    dyns[c].setSoc(soc=soc[c])
                    
                Ebeg = sum([c.getEnergy() for c in dyns])

                dyns[0].sendBalancing(toCell=dyns[1], resistances=res, iPeakAmps=iPeak,
                         tSec=txtime)
                dyns[1].sendBalancing(toCell=dyns[0], resistances=res, iPeakAmps=iPeak,
                         tSec=txtime)
                Eend = sum([c.getEnergy() for c in dyns])

                dE = Ebeg-Eend;
                tfl_sum = dyns[0].losses.tfL+dyns[1].losses.tfL
                
                np.testing.assert_allclose(dE, tfl_sum, rtol=0.07, verbose=True,
                    err_msg="after transfer back and forth, tfl and dE must be of same order")
    #             self.assertAlmostEqual(dE, tfl_sum, places=2,
    #                  msg="after transfer back and forth, tfl ~ dE (roughly)")
        
        

    def test_switching_loss_is_constant(self):
        dynClasses = DynInterface.getImplementations()

        Rcirc = 0.05; iPeak = 1.0; txtime = 0.25
        res = TransferResistance(Rtx=Rcirc*0.9, Rrx=Rcirc*1.1)
        for dynClass in dynClasses:
            dyns = [dynClass() for i in range(4)]
            for c in range(2):
                dyns[c].setLossInclusion(simu_transfer=False, simu_switch=True)
            for c in range(2,4):
                dyns[c].setLossInclusion(simu_transfer=False, simu_switch=False)
                     
            Ebeg = dyns[0].getEnergy()
            dyns[0].sendBalancing(toCell=dyns[1], resistances=res, iPeakAmps=iPeak,
                     tSec=txtime)

            dyns[2].sendBalancing(toCell=dyns[3], resistances=res, iPeakAmps=iPeak,
                     tSec=txtime)
            self.assertEqual(dyns[0].losses.swL, dyns[2].losses.swL, msg="swl must be equal for equal transfer")
            

            #this must not hold b/c simu_transfer cannot always be turned off
#             dyns[1].sendBalancing(toCell=dyns[0], resistances=res, iPeakAmps=iPeak,
#                      tSec=txtime)
#             Eend = dyns[0].getEnergy()
#             self.assertAlmostEqual(Ebeg-Eend, dyns[0].losses.swL, places=4,
#                  msg="after transfer back and for, swl ~ dE")

    def test_setSoc_and_getSoc_correspond(self):
        dynClasses = DynInterface.getImplementations()
        for dynClass in dynClasses:
            dyn = dynClass()
            
            for soc in np.linspace(0.0, 1.0, 50):
                dyn.setSoc(soc)
                self.assertAlmostEqual(soc, dyn.getSoc(), places=7)
            
            dyn.param = dyn.param._replace(voltBound=(3.4,4.16))
            for soc in np.linspace(0.0, 1.0, 50):
                dyn.setSoc(soc)
                self.assertAlmostEqual(soc, dyn.getSoc(), places=7)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

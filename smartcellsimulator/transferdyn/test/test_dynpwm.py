'''
Created on 27 Mar 2015

@author: matthias.kauer
'''
import unittest
import numpy as np
import itertools

from ..archinterface import TransferResistance
from ..dynconstpwm import DynConstPWM
from ..dynconstpwmstep import DynConstPWMStep
from ..timer import Timer
from ..archparam import ArchParam

class Test(unittest.TestCase):
    def test_VxHelper(self):
        alph = 0.9999999995775866
        beta = 0.9999999996013151
        gamm = 0.0
        thet = 8.079567909580285e-10
        
        Vbeg = 3.15
        Vend = DynConstPWM._VxHelper(Vrx0=Vbeg, Vtx0=3.2, numCycle=0, alph=alph, beta=beta, thet=thet, gamm=gamm)
        
        self.assertAlmostEqual(Vbeg, Vend, places=6, msg='For zero cycles, helper should be identity!')

    def test_pwmErrorTrackingWorks(self):

        #seed is necessary b/c certain V/I/R combinations won't work 
        #(V < i*R => no transfer in RL; here math domain error)
        
        np.random.seed(253)
        simParamList = itertools.product(
            0.1 + 2.0 * np.random.rand(4), #currents
            0.1 + 0.8 * np.random.rand(4,2), #startSocs
#             0.1 + 0.8 * np.random.rand(4), #startSoc2
            0.001 + 2.0 * np.random.rand(4), #resistances
            [10.0], #tx times
                                         )

        for iP, soc, R, txtime in simParamList:
            dyns = [DynConstPWM(diodeCurrCorrection=True) for i in range(2)]
            for c in range(2):
                dyns[c].setSoc(soc=soc[c])
            res = TransferResistance(Rtx=R*0.9, Rrx=R*1.1)

            dyns[0].sendBalancing(toCell=dyns[1], resistances=res, iPeakAmps=iP, tSec=txtime)
            
            self.assertEqual(dyns[0].errorDiode['i1'], 0.0, msg="for sender, no i1 error occurs")
            self.assertEqual(dyns[0].errorDiode['q1'], 0.0, msg="for sender, no q1 error occurs")

            self.assertGreater(dyns[1].errorDiode['i1'], 0.0, msg="for receiver, some i1 error must occur")
            self.assertGreater(dyns[1].errorDiode['q1'], 0.0, msg="for receiver, some q1 error must occur")
 
            self.assertAlmostEqual(dyns[1].errorDiode['i1'], 0.0, places=3, msg="i1 error stays small")
            self.assertAlmostEqual(dyns[1].errorDiode['q1'], 0.0, places=6, msg="q1 error stays small")

 
#     @unittest.skip("test skip")
    def test_constpwm_sending(self):
        dyns = []; stepdyns=[]
#         dp = DynBoxParams(Qfull=0.01*3600)
        ap = ArchParam(Coss=1e-5, ton=1e-4, toff=2e-4)
        
        for c in range(2):
            dyns.append(DynConstPWM(diodeCurrCorrection=True, archParam=ap))
            stepdyns.append(DynConstPWMStep(archParam=ap))
        
        txtimes = [0.15]
        #seed is necessary b/c certain V/I/R combinations won't work 
        #(V < i*R => no transfer in RL; here math domain error)
        
        np.random.seed(253)
        peakCurrents = 0.1 + 2.0 * np.random.rand(4)
        startSocs = 0.1 + 0.8 * np.random.rand(4)
        resistances = 0.001 + 2.0 * np.random.rand(4)
        
        socSet = list(zip(startSocs, reversed(startSocs)))
        currResSet = list(zip(peakCurrents, resistances))
#         num_tests = len(socSet) * len(currResSet) * len(txtimes)
#         print("test_constpwm_sending: scheduled %d tests" % (num_tests))
        
        for simulvl in itertools.product([False, True], [False, True]):       
            print(simulvl)         
            for soc in socSet: 
                for iPeak, Rcirc in currResSet:
                    for txtime in txtimes:
                        for c in range(2):
                            dyns[c].setSoc(soc=soc[c])
                            stepdyns[c].setSoc(soc=soc[c])
                            dyns[c].setLossInclusion(simu_transfer=simulvl[0], simu_switch=simulvl[1])
                            stepdyns[c].setLossInclusion(simu_transfer=simulvl[0], simu_switch=simulvl[1])
                        
                        res = TransferResistance(Rtx=Rcirc*0.9, Rrx=Rcirc*1.1)
                #         Tcycle = 6.546759805265658e-05;
                #         t = TransferSpec(send=0, recv=1, amount=10000*Tcycle, measure='T')
    #                     t = TransferSpec(send=0, recv=1, iPeakAmps=iPeak, amount=txtime, measure='T')
                        
                        with Timer() as tim:
                            dyns[0].sendBalancing(toCell=dyns[1], resistances=res, iPeakAmps=iPeak, tSec=txtime)
                        with Timer() as timstep:
                            stepdyns[0].sendBalancing(toCell=stepdyns[1], resistances=res, iPeakAmps=iPeak, tSec=txtime)
                        
    #                     print("tim/timstep %.2f/%.2f" % (tim.interval, timstep.interval))
                        self.assertLess(tim.interval *10.0, timstep.interval, "direct calc must be faster than step-wise calc")
                                
                        for c in range(2):
                            soccf = dyns[c].getSoc()
                            socstep = stepdyns[c].getSoc()
                            self.assertAlmostEqual(soccf, socstep, places=6, 
                                msg="const PWM closed-form and step-wise calculation must have same result.(T=%f, I=%f, c=%d)"
                                 %(txtime, iPeak, c))
                            
                            self.assertNotEqual(soc[c], soccf, 
                                msg="soc must have moved after pwm closed-form calculation (T=%f, I=%f, c=%d)"
                                 %(txtime, iPeak, c))
    

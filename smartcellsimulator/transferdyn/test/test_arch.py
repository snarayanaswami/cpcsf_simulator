'''
Created on 27 Mar 2015

@author: matthias.kauer
'''
import unittest
import numpy as np
from ..transferspec import TransferSpec
from ..archkutkut import ArchKutkut
from ..dyninterface import DynInterface
from ..archparam import ArchParam
from ..dynbox import DynBox
from ..dynconstpwm import DynConstPWM
from ..archbuilder import ArchBuilder
import random


class Test(unittest.TestCase):

    def setUp(self):
        self.dyns = DynInterface.getImplementations()

    def tearDown(self):
        pass
    
    def test_archbuilder_has_consistent_seed(self):
        num_seed = 3
        
        archbuilder = ArchBuilder()
        archbuilder.socInitializer = lambda num_cell : np.random.uniform(low=0.3, high=0.4, size=num_cell)

#         seeds = range(-1, num_seed+1) #-1 causes random seed => not supposed to work
        seeds = range(num_seed)

        socs1 = []
        for sd in seeds:
            archbuilder.rng_seed = sd
            arch = archbuilder.build()
            socs1.append(arch.collectSocs())
            
            
        for i, sd in enumerate(seeds):
            archbuilder.rng_seed = sd
            arch = archbuilder.build()
            self.assertEqual(socs1[i], arch.collectSocs(), msg="initial socs must be same after seeded init")
#             socs1.append(arch.collectSocs)

    def test_arch_can_be_instantiated_with_param(self):
        # arch = ArchKutkut()
        def_param = ArchKutkut.getDefaultParam()
        #note that my param structs are mostly immutable
        ap = def_param._replace(numCell=2)

        Rcell = 1.5

        dynClass =DynInterface.getImplementations()[0] 
        dp = dynClass.getDefaultParam()
        arch = ArchKutkut(archParam=ap, dynClass=dynClass, dynParam=[dp._replace(Rcell=Rcell), dp._replace(Rcell=3.1, Qfull=4000)])
        self.assertEqual(arch.cells[0].param.Rcell, Rcell, "Rcell must be in arch.cell after assignment")
        self.assertEqual(arch.cells[1].param.Rcell, 3.1, "2nd Rcell must be in arch.cell after assignment")
        self.assertNotEqual(arch.cells[1].param.voltSlope, dp.voltSlope, "voltSlope must update with Qfull assignment")

        socs1 = arch.collectSocs()
        socs2 = arch.collectSocs()
        self.assertEqual(socs1, socs2, "socs must be identical w/o action")

        t = TransferSpec(send=1, recv=0, measure='T')
        tRet = arch.execTransfer(t)

        self.assertGreater(tRet, 0, 'transfer must have real time')

        socs3 = arch.collectSocs()
        self.assertNotEqual(socs1, socs3, "socs must change after action")

    def test_arch_works_with_default(self):
        t = TransferSpec(send=1, recv=2, measure='T')
        arch = ArchKutkut()
        arch.execTransfer(t)

        
    def test_transferEnergyConsistent(self):
        dEvals = [10, 60, 100, 250, 500, 1500];       

        for dynClass in self.dyns:
            if issubclass(dynClass, DynBox):
                continue #this consistency is not given in DynBox (which is just too simple
#                 ap = ArchParam(dynamics = dynClass)
            ap = ArchParam(numCell=2)
            arch = ArchKutkut(archParam=ap, dynClass=dynClass)
            arch.setLossInclusion(simu_transfer=False, simu_switch=False)

            for dE in dEvals:
                if(dE > 150 and issubclass(dynClass, DynConstPWM)):
                    continue
                Ebegin = arch.collectEnergies()
            
                ts = TransferSpec(send=0, recv=1, iPeakAmps=1.0, measure='E', amount=dE)
                
                arch.execTransfer(ts)
                Eend = arch.collectEnergies()
                
                self.assertAlmostEqual(Ebegin[0]-dE, Eend[0], 3,
                     'ending energy must be almost equal to desired ending energy for sender')

                #for receiver, this only holds for very few models
#                 self.assertAlmostEqual(Ebegin[1]+dE, Eend[1], 3,
#                      'ending energy must be almost equal to desired ending energy for receiver')

    def test_archBuilderBasic(self):
        archbuilder = ArchBuilder()
        archbuilder.setScenario(scenario_str='default')            
        archbuilder.numCell = 50
        archbuilder.socInitializer = lambda num_cell : [0.7] * num_cell
        arch = archbuilder.build()
        
        socs = arch.collectSocs()
        np.testing.assert_almost_equal(socs, [0.7]*archbuilder.numCell) #, msg="all socs must be according to socInitializer")

        Ebegin = arch.collectEnergies()
        Ebegin2 = arch.collectEnergies()
        self.assertEqual(Ebegin, Ebegin2)
        ts = TransferSpec(send=10, recv=11, iPeakAmps=1.0, measure='T', amount=10.0)
        arch.execTransfer(ts)
        Eend = arch.collectEnergies()

        self.assertNotEqual(Ebegin, Eend)
            


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

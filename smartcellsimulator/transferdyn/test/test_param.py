'''
Created on 27 Mar 2015

@author: matthias.kauer
'''
import unittest
from ..dynboxparam import DynBoxParam
from ..archparam import ArchParam
import copy


class Test(unittest.TestCase):

    def setUp(self):
        self.testParams = [ArchParam, DynBoxParam]

    def tearDown(self):
        pass

    def test_paramsAreIterable(self):
        
        # Write cell parameters
        for paramClass in self.testParams:
            test_dict = {}
            dp = paramClass()
            for key in dp._fields:
                test_dict[key] = getattr(dp, key)
       
    def test_paramsReturnDict(self):
        for paramClass in self.testParams:
            test_dict = {}
            dp = paramClass()
            dpd = dp.__dict__
            for key, val in dpd.items():
                test_dict[key] = val
            
            for key in dp._fields:
                self.assertEqual(test_dict[key], getattr(dp, key), msg=
                    "constructed dict must have same values as original param struct")
        
    def test_paramCopy(self):
        for paramClass in self.testParams:
            dp = paramClass
            ret = copy.copy(dp)
            
            dpd = dp.__dict__
            retd = ret.__dict__
            for k in dpd.keys():
                self.assertEqual(dpd[k], retd[k])
                
    def test_replace(self):

        dp = DynBoxParam()
        ret = dp._replace(voltBound=(1.0, 2.0))
        
        dpd = dp.__dict__
        retd = ret.__dict__
        for k in dpd.keys():
            if k not in ['voltBound']:
                self.assertEqual(dpd[k], retd[k])
 

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()

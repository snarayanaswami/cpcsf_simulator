class DynLosses(object):
    def __init__(self):
        self.swL = 0.0
        self.tfL = 0.0
        self.tfE = 0.0
        self.simu_switch = False
        self.simu_transfer= False
        
    @property
    def total(self):
        return self.swL + self.tfL
        
    def __add__(self, other):
        #https://docs.python.org/2/reference/datamodel.html#object.__add__
        assert self.simu_switch == other.simu_switch
        assert self.simu_transfer == other.simu_transfer
        
        ret = self.__class__()
        ret.simu_switch = self.simu_switch
        ret.simu_transfer = self.simu_transfer
        
        ret.swL = self.swL + other.swL
        ret.tfL = self.tfL + other.tfL
        ret.tfE = self.tfE + other.tfE
         
        return ret

    def __str__(self):
        return "BattDynLosses: " + str(self.__dict__)


Guidelines
===============
I'm following the Google style as discussed by Khan Academy https://github.com/Khan/style-guides/blob/master/style/python.md

```
sphinx-apidoc -e -T -o docs/apidoc . test
```
regenerates the automatic documentation. 'test' folder is excluded.

```
make html
```
build with sphinx.

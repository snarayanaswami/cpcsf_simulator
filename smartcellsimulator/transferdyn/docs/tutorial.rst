========
Tutorial
========

.. ipython:: python

   from transferdyn import ArchBuilder

   ab = ArchBuilder()
   ab.build()

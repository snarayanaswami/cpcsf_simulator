*******************
Public API
*******************
The classes defined in __init__.py are considered public and represent
the preferred way to interact with this library.

.. literalinclude:: ../__init__.py
    :language: python

For example you can use this:

>>> from transferdyn import ArchBuilder

to achieve the same as this longer version:

>>> from transferdyn.archbuilder import ArchBuilder

## transferdyn - A library for charge transfers in active cell balancing simulations

This currently to be used as a sub-repository (or sub-folder if you dislike the version coupling) in larger projects like [FastCellSimulator](https://bitbucket.org/matthias_kauer/fastcellsimulator) or [SmartCellSimulator](https://bitbucket.org/steinhorst/smartcellsimulator).

### Overview
This library currently contains implementations of the following models.

* Box
* Constant PWM

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* No pushes in [transferdyn](https://bitbucket.org/matthias_kauer/transferdyn). Only (somewhat) stable versions are pushed to this repository. 
* Development takes place in [transferdyn-dev](https://bitbucket.org/matthias_kauer/transferdyn-dev) for now. Ask us for access if you want to collaborate! Pushing to the default branch is restricted there as well, so I can cleanly push to [transferdyn](https://bitbucket.org/matthias_kauer/transferdyn) itself.

### Contributors ###

* [Matthias Kauer](http://www.matthiaskauer.com/about/)
* Arne Meeuw
* Swaminathan Narayanaswamy
* Sebastian Steinhorst
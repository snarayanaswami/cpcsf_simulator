'''
Created on 27 Jan, 2015

@author: matthias
'''

#http://dbader.org/blog/abstract-base-classes-in-python
from abc import ABCMeta, abstractmethod
from collections import namedtuple, Iterable
import scipy as s
from .transferspec import TransferSpec
from .dyninterface import DynInterface
from .archparam import ArchParam
import warnings

TransferResistance = namedtuple('TransferResistance', ['Rtx', 'Rrx', 'numMtx', 'numMrx']);

class ArchInterface(metaclass=ABCMeta):
#     http://stackoverflow.com/questions/7064925/python-metaclass-and-modgrammar
#only Python 3 has class .. (metaclass=ABCMeta) syntax
    #__metaclass__=ABCMeta #would be for python2
    #suggestion: don't change var names in implementations; it screws up eclipse code complete
 
    @staticmethod
    def getImplementations():
        impl_list = []
        try:
            from .archkutkut import ArchKutkut
            impl_list.append(ArchKutkut)
        except:
            warnings.warn("Arch implementation ArchKutkut not found")

        try:
            from .archdac2013 import ArchDAC2013
            impl_list.append(ArchDAC2013)
        except:
            warnings.warn("Arch implementation ArchDAC2013 not found")

        try:
            from .archflyback import ArchFlyback
            impl_list.append(ArchFlyback)
        except:
            warnings.warn("Arch implementation ArchFlyback not found")


        try:
            from .archinductornn import ArchInductorNN
            impl_list.append(ArchInductorNN)
        except:
            warnings.warn("Arch implementation ArchInductorNN not found")
           
        return impl_list
 
    @abstractmethod
    def getResistance(self, idxSend, idxRecv):
        pass
    
    @abstractmethod
    def verifyTransfers(self, transferspecList):
        pass

    def __init__(self, archParam, dynClass, dynParam):
        if archParam is None:
            archParam = ArchParam()
        if dynClass is None:
            defaultDynClass = DynInterface.getImplementations()[0]
            dynClass = defaultDynClass
        if dynParam is None:
            dynParam = dynClass.getDefaultParam()

        #supposed to make child constructors easier
        assert isinstance(archParam, ArchParam)
        self.param = archParam;
        
        assert issubclass(dynClass, DynInterface)
#             self.cells = [dynClass()] * self.param.numCell #don't do this -> all are same instance?
        self.cells = []
        for c in range(self.param.numCell):
            self.cells.append(dynClass(archParam=self.param))

        if not isinstance(dynParam, list):
            dynParam = [dynParam] * len(self.cells)
        if len(dynParam) == len(self.cells):
            for idx, c in enumerate(self.cells):
                c.param = dynParam[idx]
        else:
            raise Exception("sth wrong with dynParam")

        self.setSocs()
          
    
    @staticmethod
    def getDefaultParam():
        return ArchParam()

    def setSocs(self, socInitializer=lambda num_cell: [0.5]*num_cell): 
        if(callable(socInitializer)):
            soc_array = socInitializer(self.param.numCell)
        else:
            soc_array = socInitializer

        assert len(soc_array) == self.param.numCell
        for idx, c in enumerate(self.cells):
            assert 0 < soc_array[idx] < 1.0, soc_array[idx]
            c.setSoc(soc_array[idx])        
            
    def setLossInclusion(self, simu_switch, simu_transfer):
        for c in self.cells:
            c.setLossInclusion(simu_switch=simu_switch, simu_transfer=simu_transfer)
            
    def execTransfer(self, transferspec):
        t = transferspec
        assert isinstance(t, TransferSpec), t
        
        res = self.getResistance(t.send, t.recv)
        cellTx = self.cells[t.send]; cellRx = self.cells[t.recv]
        if(t.measure=='T'):
            tRet = cellTx.sendBalancing(toCell=cellRx,resistances=res, iPeakAmps=t.iPeakAmps, tSec=t.amount)
        elif(t.measure=='E'):
            tRet = cellTx.sendBalancing(toCell=cellRx,resistances=res, iPeakAmps=t.iPeakAmps, dE=t.amount)
        else:
            raise Exception('You have specified a transfer spec measure that I did not understand: %s' %(t.measure))

        return tRet
    
#     def execTransfers(self, transferspecList):
#         for t in transferspecList:
#             assert isinstance(t, TransferSpec), t
#            
#         for t in transferspecList:
#             self.execTransfer(transferspec=t)
 
    def getSocDeviation(self):
        SocArr = s.array(self.collectSocs())
        maxdev = abs(SocArr - SocArr.mean()).max()
        return maxdev

    def getLossSummary(self):
        cells = self.cells
        ret = cells[0].getLosses()
        
        for k in range(1, len(cells)):
            ret = ret + cells[k].getLosses()
        return ret
    
    def collectLosses(self):
        v = []
        for c in self.cells:
            v.append(c.getLosses())
        return v
    
    def collectVoltages(self):
        v = []
        for c in self.cells:
            v.append(c.getVoltage())
        return v
    
    def collectSocs(self):
        socs = []
        for c in self.cells:
            socs.append(c.getSoc())
        return socs
    
    def collectCharges(self):
        Q = []
        for c in self.cells:
            Q.append(c.getCharge())
        return Q
    
    def collectEnergies(self):
        nrg = []
        for c in self.cells:
            nrg.append(c.getEnergy())
        return nrg

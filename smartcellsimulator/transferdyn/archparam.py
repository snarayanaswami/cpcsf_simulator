from collections import namedtuple, OrderedDict

class ArchParam(namedtuple('ArchParamNamedTuple', ['RMos', 'RInd', 'LInd', 
        'toff', 'ton', 'Coss', 'iMax', 'numCell', 'sendRange', 'Vd', 'k'])):
    """
    A parameter class relying on namedtuple (constructor args = member vars).

    Notes: 
        ArchParam relies on the following assumptions:

        * transfers are happening over identical inductors and transistors.
    
    Args:
        RMos (float):       transistor resistance
        RInd (float):       inductor resistance
        LInd (float):       inductance 
        toff (float):       turn-off delay time + fall time of transistor (for switching loss calculation)
        ton (float):        turn-on delay time + rise time of transistor (for switching loss calculation)
        Coss (float):       transistor output capacitance (for switching loss calculation)
        iMax (float):       maximum current the architecture supports (operating peak current may be checked against this)
        numCell (int):      number of cells the architecture has
        sendRange (int):    how far cells can send charge (e.g. 1: only next neighbor)
        Vd (float):         (transistor) diode forward voltage (certain architectures receive over diode during receiving phase. Others take freewheeling phases into account)

    Examples:
    
        .. ipython:: python
            
            from transferdyn.archparam import ArchParam
            ap = ArchParam()
            ap  #Note the default parameters!

            ap2 = ArchParam(RMos=1.23, RInd=0.75)
            ap2 #two parameters are changed

            ap3 = ap2._replace(RMos=2.54)
            ap3 #further change from ap2
                
    See Also:
        * `namedtuple implementation (Stackoverflow) <http://stackoverflow.com/questions/17916853/how-named-tuples-are-implemented-internally-in-python>`_
        * `namedtuple as part of Python 3.4 collections docs <https://docs.python.org/3.4/library/collections.html>`_
        * :any:`ArchInterface` : interface parameterized by ArchParam
        * `DAC2013 paper for active balancing explanation <https://lukasiewycz.github.io/pdf/2013-DAC-Modular%20System-Level%20Architecture%20for%20Concurrent%20Cell%20Balancing.pdf>`_
    """

    def __new__(cls, RMos=0.01, RInd=0.05, LInd=1e-4, 
        toff=1.68e-7, ton=4.4e-8, Coss=1.25e-10, iMax=3.0, numCell=10, sendRange=1,Vd=0.0,k=1):
        #MOSFET values from NXP 6 (see DATE paper parameter list)
        return super(ArchParam, cls).__new__(cls, RMos=RMos, RInd=RInd, LInd=LInd,
             toff=toff, ton=ton, Coss=Coss, iMax=iMax, numCell=numCell, sendRange=sendRange, Vd=Vd, k=k)

    
    #this is necessary b/c __ wasn't found in superclass (I suppose)
    @property
    def __dict__(self):
        'A new OrderedDict mapping field names to their values'
        return OrderedDict(zip(self._fields, self))        

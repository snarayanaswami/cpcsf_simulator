'''
Created on 30 Mar, 2015

@author: matthias
'''

import math
from .dynconstpwm import DynConstPWM, RecvPack
from collections import namedtuple

StepRecvPack = namedtuple('StepRecvPack', ['Vt0', 'Rt', 'numCycle', 'VtEnd']) 


class DynConstPWMStep(DynConstPWM):
    '''
    classdocs
    '''


    def sendBalancing(self, toCell, resistances, iPeakAmps, tSec):
#         DynConstPWM.sendBalancing(self, toCell, resistances, transferspec)
        res = resistances
#         assert t.measure == 'T', 'currently E transfer not implemented'
        
        Ton, Toff, Tbreak = self.getTiming(toCell, resistances, iPeakAmps)
        Tcycle = Ton + Toff + Tbreak
        numCycle = self._getCycles(Tcycle, tSec=tSec)
        
        for k in range(numCycle):
            rPack = self._applyBalSender(Rcirc=res.Rtx, Ton=Ton)
            toCell._applyBalReceiver(Rcircuit=res.Rrx, Ton=Ton,
                Toff=Toff, Tbreak=Tbreak, recvPack=rPack)
            
    def _applyBalSender(self, Rcirc, Ton):
#         DynConstPWM.__applyBalSender(self, Rcirc, iPeakAmps, Ton, numCycle)
        ap = self.archParam
        R = Rcirc + self.param.Rcell
        m = self.param.voltSlope
        a = Ton/R + ap.LInd / R**2 * (math.exp((-R/ap.LInd)*Ton) - 1)
        
        
        Vbeg = self.V
        qtx = a * Vbeg
        Vstep = Vbeg - m * qtx;
        
        if(self.losses.simu_switch):
            i0 = self._iHelper(V=Vbeg, i0=0.0, R=R, T=Ton)
            qswtx = 0.5 *(ap.Coss *Vbeg + i0 * ap.toff)
            Vstep -= m*qswtx
        
        self.V = Vstep
        
        recvPack = StepRecvPack(Vt0 = Vbeg, Rt=R, VtEnd=Vstep, numCycle=1)
        return recvPack
        
    def _applyBalReceiver(self, Rcircuit, Ton, Toff, Tbreak, recvPack):
#         DynConstPWM.__applyBalReceiver(self, Rcircuit, Ton, Toff, Tbreak, recvPack)
        ap = self.archParam; p = self.param
        Rr = Rcircuit + self.param.Rcell
        L = ap.LInd
        Rtx = recvPack.Rt
        Vt0 = recvPack.Vt0
        numCycle = recvPack.numCycle
        
        Vbeg = self.V
        
        c = -L / Rr * (math.exp(-Rr/L*Toff) -1)
        b = -Toff / Rr + c/Rr
        m = p.voltSlope
        d = 1.0 / Rtx * (1-math.exp(-Rtx/L * Ton))
        
        i0 = d * recvPack.Vt0
        qrx1 = -Vbeg/Rr*Toff + L*(-Vbeg-i0*Rr)/Rr**2 * (math.exp(-Rr/L*Toff)-1);
        qrx12 = b * Vbeg + c * i0; #just to double-check in debugging
        
        Vd = ap.Vd
        
        Vtot = Vd + Vbeg
        i1 =  (-Vbeg)/Rr - (-Vbeg-i0*Rr)/Rr * math.exp(-Rr/L *Toff)
        T1 = i1 * L / Vtot
        qrx2 = 0.5 * T1 * i1
        
        Vstep = Vbeg + m*(qrx1 + qrx2)
        
        if(self.losses.simu_switch):
            i0 = self._iHelper(V=Vt0, i0=0.0, R=Rtx, T=Ton)
            qswrx = 0.5*(ap.Coss *Vbeg + i0 * ap.ton)
            Vstep -= m*qswrx
        
        self.V = Vstep

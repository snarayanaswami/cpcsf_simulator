'''
Created on 24 Nov, 2017

@author: martin
'''

from .archinterface import *
from .dynbox import DynBox
from .archparam import ArchParam
from .dynboxparam import DynBoxParam

class ArchInductorNNbest(ArchInterface):
    #this refers to the 2 MOSFET version as in smart cell journal paper

    def __init__(self, archParam=None, dynClass=None, dynParam=None):
        if archParam is None:
            archParam = ArchParam()
        if dynClass is None:
            dynClass = DynBox
        if dynParam is None:
            dynParam = DynBoxParam()
        super().__init__(archParam=archParam, dynClass=dynClass, dynParam=dynParam)


    def getResistance(self, idxSend, idxRecv):
        distance = abs(idxSend - idxRecv)

        archParam = self.param # Circuit resistances excluding internal cell resistance
        if idxSend>=idxRecv:
            numMtx = 3
            numMrx = 3 + 2 * distance
        else:
            numMtx = 3 + 2 * distance
            numMrx = 3
        Rtx= archParam.RInd + numMtx*archParam.RMos
        Rrx= archParam.RInd + numMrx*archParam.RMos
        
        return TransferResistance(Rtx=Rtx, Rrx=Rrx, numMtx=numMtx, numMrx=numMrx)

    def verifyTransfers(self, transferspecList):
        #TODO: also check for concurrency
        for transferspec in transferspecList:
            if abs(transferspec.send - transferspec.recv) != 1:
                return False
            if(abs(transferspec.iPeakAmps) > self.param.iMax):
                return False
            out_range = lambda x : (x<0) or (x>=self.param.numCell)
            if(out_range(transferspec.send) or out_range(transferspec.recv) ):
                return False
        return True                
    
    def __str__(self):
        return "ArchitectureInductorNNbest: " + str(self.param)
           

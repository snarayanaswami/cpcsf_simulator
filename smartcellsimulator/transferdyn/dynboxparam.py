import copy
from collections import OrderedDict

class DynBoxParam(object):
    #inspired from namedtuple
    #implementation details: http://stackoverflow.com/questions/17916853/how-named-tuples-are-implemented-internally-in-python
    _fields = ('voltBound', 'Qfull', 'Rcell')

    def __init__(self, voltBound=(3.00, 3.33), Qfull=1.1*3600, Rcell=0.05):
        #Soc 0-100% refers to usable voltage range; voltbound must correspond to this range; ideally voltage is linear between these points.
        #Qfull should correspond to the amount of charge available between these two soc points. 
        #data from measurement for DATE 2015 paper (A123 18650 cell)

        self.voltBound=voltBound
#         self._voltSlope=(voltBound[1]-voltBound[0])/Qfull
        self.Qfull=Qfull
        self.Rcell=Rcell
             
    @property
    def voltSlope(self):
        return (self.voltBound[1]-self.voltBound[0])/self.Qfull
    
#     @property
#     def __dict__(self):
#         outdict = OrderedDict()
#         for key in self._fields:
#             outdict[key] = getattr(self, key)
#               
#         return outdict

    def _asdict(self):
        return self.__dict__

#     @property
#     def voltSlope(self):
#         return self._voltSlope
#     
#     @property
#     def voltBound(self):
#         return self._voltBound
# 
#     @voltBound.setter
#     def voltBound(self, value):
#         self._voltBound = value
#         self._voltSlope = (value[1]-value[0])/self.Qfull 
#         
#     @property
#     def Qfull(self):
#         return self._Qfull
#     
#     @Qfull.setter
#     def Qfull(self, value):
#         self._Qfull = value
#         voltBound = self.voltBound
#         self._voltSlope=(voltBound[1]-voltBound[0])/value
#         
    def _replace(self, voltBound=None, Qfull=None, Rcell=None):
        ret = copy.copy(self)
        if voltBound is not None:
            ret.voltBound = voltBound
        if Qfull is not None:
            ret.Qfull = Qfull
        if Rcell is not None:
            ret.Rcell = Rcell
            
        return ret
    
    def __repr__(self):
        return self.__class__.__name__ + '%s' % self._asdict()


'''Helper class for constructing architectures'''

import random
import numpy as np
from .dynboxparam import DynBoxParam
from .archparam import ArchParam
from .dynconstpwm import DynConstPWM
from .dynconstpwmflyback import DynConstPWMFlyback
from .archkutkut import ArchKutkut
from .archflyback import ArchFlyback
from .archinductornn import ArchInductorNN
from .archinductornnbest import ArchInductorNNbest
from .dyninterface import DynInterface

class ArchBuilder(object):
    '''
    Helper class for constructing architectures.

    Architectures are collections of cells that handle most of the interactions
    a simulation may want to perform on a battery pack. In the builder you 
    modify default architecture parameters and construct new instances.
    
    Arguments:
        rng_seed (Optional[int]) : see :attr:`rng_seed`, default: -1

    Attributes:
        rng_seed (int) : 

            * >= 0 : set the specified seed for both random.seed and numpy.random.seed before 
              building (important if :attr:`socInitializer` creates random values)
            * < 0 : set random seed before calling build (printed for possible re-creation later

        socInitializer(function int -> array of len(num_cell) or array of len(num_cell)) : 
            initializes cell SoC during building

        dynClass(:any:`transferdyn.dyninterface.DynInterface`) :
            Cell dynamics the built architecture will operate on.
            Building calls this classes' constructor.

        dynParam(:any:`transferdyn.dynboxparam.DynBoxParam` or array of ~):
            Parameters for the cells that will be set **after** construction.
            If an array with len = numCell is specified, each cell gets 
            individual parameters.
            
        archParam(:any:`transferdyn.archparam.ArchParam`) : specify architecture 
            component parameters

        archClass(:any:`ArchInterface`) : select the 
            architecture class you want to build
            
    Examples:
        Obtain a default architecture by simply building.


        .. ipython:: python
            :suppress:

            from transferdyn import ArchBuilder

        .. ipython:: python

            ab = ArchBuilder()
            arch = ab.build()

        Set dynClass before building

        .. ipython:: python

            from transferdyn.dynbox import DynBox   #dumb default cell dynamics
            ab = ArchBuilder()
            ab.dynClass = DynBox  #use class here, don't construct!
            arch = ab.build()

    See Also:
        * :any:`archinterface` : specifies architectures do
    '''

    def __init__(self, rng_seed=-1):
        self.setScenario(scenario_str='default')
        self.rng_seed = rng_seed

    def build(self):
        """
        build a new architecture specified by the builder's current attributes

        See Also:
            * :attr:`rng_seed` for details on random number initialization
            * :attr:`setScenario`
        """
        rng_seed = self.rng_seed
        if(rng_seed < -1):
            seedrange = 2**31-1 #guaranteed maxint in Python on all platforms
#             seedrange = sys.maxsize
            rng_seed = random.randint(1, seedrange)
        if rng_seed >= 0: #only rng_seed = -1 should escape here!
            random.seed(rng_seed)
            np.random.seed(rng_seed)
            print("archbuilder: I initialized the rng rng_seed to %d" % rng_seed)

        dp = self.dynParam
        ap = self.archParam
        arch = self.archClass(archParam=ap, dynParam=dp, dynClass=self.dynClass)
        arch.setSocs(socInitializer=self.socInitializer)
        return arch
    
    @property
    def numCell(self):
        """number of cells (property proving direct access to self.archParam.numCell)"""
        return self.archParam.numCell
        
    @numCell.setter
    def numCell(self, value):
        self.archParam = self.archParam._replace(numCell=value)

    def setScenario(self, scenario_str):
        """
        configure builder attributes according to pre-defined scenario

        Args:
            scenario_str (str) : Select from the following built-in scenarios
                {*default*: uses default constructors and sets all cells to 50% SoC, 
                *24green*: uses parameters as in smart cell journal (submitted),
                *24green_nrg*: uses parameters as in optimal routing journal (to submit)
                may not work because cell dynamics class still close-sourced for now}
        """

        if(scenario_str=='default'):
            self.dynClass = DynInterface.getImplementations()[0]
            self.dynParam = self.dynClass.getDefaultParam()
            self.archParam = ArchParam()
            self.archClass = ArchKutkut
            self.socInitializer = lambda num_cell: [0.5] * num_cell #set all to 0.5
            """the socinit explanation"""

        elif(scenario_str=='24green'):
            """24 cells in parallel => 24x2.5Ah = 60Ah; in series 96 x 60Ah x 4V = 23kWh 
            Nissan leaf has comparable 24kWh, see: http://en.wikipedia.org/wiki/Nissan_Leaf

            Rcell and capa from datasheet, voltBound from measurement
            Qfull from meas: 28568 sec till 3.4V linearly @0.25A: 28568sec => ~1.98 Ah;
            till full discharge (2.5 V cutoff) ~ 32966sec => 2.28 Ah
            
            Archparam, we're assuming
            inductor:  Bourns 1140-120K-RC
            MOSFET: Infineon OptiMOS BSC010NE2LS 
            """
            self.socInitializer = lambda num_cell : random.uniform(a=0.40, b=0.94) + \
                np.random.uniform(low=0.00, high=0.06, size=num_cell)
            self.dynParam = DynBoxParam(Rcell=0.02215/24.0, voltBound=(3.4,4.16), Qfull=24*(28568*0.25))
            self.archParam = ArchParam(RInd=0.005, LInd=120e-6, ton=6.7e-9, toff=34e-9, RMos=0.0011, iMax=30.0, Coss=1700e-12, numCell=36, sendRange=1, Vd=0.8, k=1.00)
            self.dynClass = DynConstPWM
            self.archClass = ArchKutkut
        elif(scenario_str=='24green_nrg'):
            try:
                from .dynconstvoltcf import DynConstVoltCf
            except:
                raise
            self.setScenario('24green')

            self.dynClass = DynConstVoltCf
            self.socInitializer = lambda num_cell : random.uniform(a=0.3, b=0.6) + \
                 np.random.uniform(low=0.00, high=0.03, size=num_cell)
        elif(scenario_str=='flyback'):
            """24 cells in parallel => 24x2.5Ah = 60Ah; in series 96 x 60Ah x 4V = 23kWh 
            Nissan leaf has comparable 24kWh, see: http://en.wikipedia.org/wiki/Nissan_Leaf

            Rcell, Qfull, voltbound from scenario_str='24_green'

            Archparam for Flyback architecture
            inductor:  HM78D 121 470MLF
            MOSFET: NEXPERIA BUK9Y30 75B (for fast and low switching Mosfets) """

            self.socInitializer = lambda num_cell: random.uniform(a=0.4, b=0.94) + \
                np.random.uniform(low=0.00, high=0.03, size=num_cell)
            self.dynParam = DynBoxParam(Rcell=0.02215 / 24.0, voltBound=(3.4, 4.16), Qfull=24 * (28568 * 0.25))
            self.archParam = ArchParam(RInd=0.065, LInd=47e-6, ton=16e-9, toff=51e-9, RMos=0.0013, iMax=100.0, Coss=1082e-12, numCell=36, sendRange=1, Vd=0.85, k=0.97) #k=coupling coefficient
            self.dynClass = DynConstPWMFlyback
            self.archClass = ArchFlyback
        elif (scenario_str == 'inductornn'):
            """24 cells in parallel => 24x2.5Ah = 60Ah; in series 96 x 60Ah x 4V = 23kWh 
            Nissan leaf has comparable 24kWh, see: http://en.wikipedia.org/wiki/Nissan_Leaf

            Rcell and capa from datasheet, voltBound from measurement
            Qfull from meas: 28568 sec till 3.4V linearly @0.25A: 28568sec => ~1.98 Ah;
            till full discharge (2.5 V cutoff) ~ 32966sec => 2.28 Ah

            Archparam, we're assuming
            inductor:  Bourns 1140-120K-RC
            MOSFET: Infineon OptiMOS BSC010NE2LS 
            """
            self.socInitializer = lambda num_cell: random.uniform(a=0.4, b=0.94) + \
                                                   np.random.uniform(low=0.00, high=0.03, size=num_cell)
            self.dynParam = DynBoxParam(Rcell=0.02215 / 24.0, voltBound=(3.4, 4.16), Qfull=24 * (28568 * 0.25))
            self.archParam = ArchParam(RInd=0.005, LInd=120e-6, ton=6.7e-9, toff=34e-9, RMos=0.0011, iMax=30.0, Coss=1700e-12, numCell=36, sendRange=1, Vd=0.8, k=1.00)
            self.dynClass = DynConstPWM
            self.archClass = ArchInductorNN
        elif (scenario_str == 'inductornnbest'):
            """24 cells in parallel => 24x2.5Ah = 60Ah; in series 96 x 60Ah x 4V = 23kWh 
            Nissan leaf has comparable 24kWh, see: http://en.wikipedia.org/wiki/Nissan_Leaf

            Rcell and capa from datasheet, voltBound from measurement
            Qfull from meas: 28568 sec till 3.4V linearly @0.25A: 28568sec => ~1.98 Ah;
            till full discharge (2.5 V cutoff) ~ 32966sec => 2.28 Ah

            Archparam, we're assuming
            inductor:  Bourns 1140-120K-RC
            MOSFET: Infineon OptiMOS BSC010NE2LS 
            """
            self.socInitializer = lambda num_cell: random.uniform(a=0.40, b=0.94) + \
                                                   np.random.uniform(low=0.00, high=0.03, size=num_cell)
            self.dynParam = DynBoxParam(Rcell=0.02215 / 24.0, voltBound=(3.4, 4.16), Qfull=24 * (28568 * 0.25))
            self.archParam = ArchParam(RInd=0.005, LInd=120e-6, ton=6.7e-9, toff=34e-9, RMos=0.0011, iMax=30.0, Coss=1700e-12, numCell=36, sendRange=1, Vd=0.8, k=1.00)
            self.dynClass = DynConstPWM
            self.archClass = ArchInductorNNbest
        else:
            raise Exception("sorry, I don't know that scenario string")
        

#http://dbader.org/blog/abstract-base-classes-in-python
from abc import ABCMeta, abstractmethod
import warnings
from .archparam import ArchParam
from .dynlosses import DynLosses
    

class DynInterface(metaclass=ABCMeta):
#     http://stackoverflow.com/questions/7064925/python-metaclass-and-modgrammar
#only Python 3 has class .. (metaclass=ABCMeta) syntax
    #__metaclass__=ABCMeta #would be for python2
    #suggestion: don't change var names in implementations; it screws up eclipse code complete

    
    @staticmethod
    @abstractmethod
    def getDefaultParam():
        pass
    
    @abstractmethod
    def sendBalancing(self, toCell, resistances, iPeakAmps, tSec=None, dE=None):
        pass
    
    @abstractmethod
    def setSoc(self, soc):
        pass
    
    @abstractmethod
    def applyLoadCurrent(self, iAmps, tSec):
        assert False , "execution should never get here"
        timeTillBoundaryReachedOrFinished = 0.0; 
        return timeTillBoundaryReachedOrFinished
 
    @abstractmethod
    def getBalancingTimeEstTillTarget(self, iPeakAmps, soc=None, E=None):
        #refers to target in sender only!
        assert False , "execution should never get here"
        iPeakAmpsWithCorrectSign = iPeakAmps
        timeEst = 0.0
        return timeEst, iPeakAmpsWithCorrectSign
    
#     @abstractmethod
#     def getBalancingLossBounds(self, toCell, resistances, iPeakAmps, tSec=None, dE=None):
#         pass


    @abstractmethod
    def getVoltage(self):
        pass

    @abstractmethod
    def getSoc(self):
        pass

    @abstractmethod
    def getCharge(self):
        pass
    
    @abstractmethod
    def getEnergy(self):
        pass
    
    @abstractmethod
    def getLoadTimeEstTillTarget(self, iAmps, soc=None, E=None):
        pass
    
    @abstractmethod
    def getName(self):
        pass
   
    def __str__(self):
        s = self.getName() + ', param: ' + str(self.getParam());
        s += '\nStatus: V=' + str(self.getVoltage()) + ", Soc=" + str(self.getSoc())
        s += ', Q=' + str(self.getCharge())
        s += '\nLosses: ' + str(self.getLosses())
        return s;

#http://stackoverflow.com/questions/6275127/why-would-someone-use-property-if-no-setter-or-deleter-are-defined
    @property
    def losses(self):
        """I'm the losses property, no setter or deleter, so you don't get to change me from outside"""
        return self._losses

    def __init__(self, archParam):
        #supposed to make child constructors easier
        if archParam is None:
            archParam = ArchParam()
        assert isinstance(archParam, ArchParam)
        self.archParam = archParam

        self._losses = DynLosses()
        self.setLossInclusion(simu_transfer=True, simu_switch=True)

    def setLossInclusion(self, simu_transfer, simu_switch):
        self._losses.simu_switch = simu_switch;
        self._losses.simu_transfer = simu_transfer;


    @staticmethod
    def getImplementations():
        impl_list = []
        #TODO: reactivate dynbox
        if False:
            try:
                from .dynbox import DynBox
                impl_list.append(DynBox)
            except:
                warnings.warn("Implementation Dynbox not found")

        try:
            from .dynconstpwm import DynConstPWM
            impl_list.append(DynConstPWM)
        except:
            warnings.warn("Implementation DynConstPWM not found")
           
        return impl_list


'''
Created on 26 Jan, 2015

@author: matthias
'''
import warnings
import numpy as np
import math
from .dynboxparam import DynBoxParam
from .dyninterface import *
# from .archparam import ArchParam


        
            
class DynBox(DynInterface):
    def __init__(self, archParam=None, dynParam=None, soc=0.5):
        super().__init__(archParam=archParam)

        if not dynParam:
            dynParam = DynBoxParam()
            
        assert isinstance(dynParam, DynBoxParam)
        self.archParam = archParam; 
        
        # self._losses = DynLosses(); 
        self.param = dynParam
        self.Q = soc * self.param.Qfull  #init at 50% SoC
        self.Efull = self.getEnergy(Q=self.param.Qfull)

                
    @staticmethod
    def getDefaultParam():
        return DynBoxParam()

    # def getParam(self):
    #     return self.param
    
    def setLossInclusion(self, simu_transfer, simu_switch):
        self.losses.simu_switch = simu_switch;
        self.losses.simu_transfer = simu_transfer

    def applyLoadCurrent(self, iAmps, tSec):
        assert tSec > 0
        Qold = self.getCharge()
        dQ = (iAmps * tSec)
        
        tRet = tSec
        if Qold + dQ > self.param.Qfull:
            dQ = self.param.Qfull - Qold
            tRet = dQ / iAmps
        elif Qold + dQ < 0:
            dQ = 0 - Qold
            tRet = -dQ / iAmps
            
        tfL = self.param.Rcell * iAmps**2 * tRet
        self.Q += dQ
        self.subtractTransferLoss(tfL)
        
        return tRet
    
    def sendBalancing(self, toCell, resistances, iPeakAmps, tSec=None, dE=None):
        res = resistances;
                
        if(tSec is not None):
            ttx = tSec; 
        elif(dE is not None):
            targE = self.getEnergy() - dE
            ttx = self.getBalancingTimeEstTillTarget(toCell=toCell, resistances=resistances,
                    iPeakAmps=iPeakAmps, E=targE)
        
        rettx = self.applyBalancingCurrent(Rcircuit=res.Rtx,  
            iPeakAmps=-abs(iPeakAmps), tSec=ttx)
        retrx = toCell.applyBalancingCurrent(Rcircuit=res.Rrx, 
                    iPeakAmps=abs(iPeakAmps), tSec=rettx)
        
            
#         assert rettx == retrx and rettx == ttx, \
#             "simulator hit cell boundary tx(%d) %.2f - %.2f rx(%d)" % \
#             (self.getSoc(), toCell.getSoc())

        return rettx

    def subtractTransferLoss(self, tfL):
        if(self.losses.simu_transfer):
            #update Q according to transfer losses
            #drops losses that would've exceed bounds
            #must come after Q is updated w/ standard dQ
            #tracking must be done separately
            Enow = self.getEnergy()
            Enew = min(self.getEnergy(Q=self.param.Qfull), max(0, Enow-tfL))
            Qnew = self.getCharge(E=Enew)
            self.Q = Qnew
            
    def applyBalancingCurrent(self, Rcircuit, iPeakAmps, tSec):
        Raggr = Rcircuit + self.param.Rcell
        Qold = self.getCharge()
        #triangle shape causes 1/2 transfer; 50% of time the partner transfers
        iMean = 0.25 * iPeakAmps
        
        tRet = tSec
        dQ =  iMean* tSec
        if Qold + dQ > self.param.Qfull:
            dQ = self.param.Qfull - Qold
            tRet = dQ / iMean
        elif Qold + dQ < 0:
            dQ = 0 - Qold
            tRet = -dQ / iMean
            
        tfL = 0.5 * (Raggr * iPeakAmps**2) * tRet * 0.5; #only 50% time we are active, other time: partner cell
        self.losses.tfL += tfL;
#         if(dQ > 0):
#             dQmu = self.param.muEff * dQ
#             tfL = self.getEnergy(Q=Qold+dQ) - self.getEnergy(Q=Qold+dQmu)
#             assert tfL > 0     
#             self.losses.tfL += tfL;
#             
#             if(self.losses.simu_transfer):
#                 dQ = dQmu
               
        tfE =abs(self.getEnergy(Q=Qold) - self.getEnergy(Q=Qold+dQ))
        self.losses.tfE += tfE
        self.Q += dQ
        
        self.subtractTransferLoss(tfL)            
        return tRet
            
    def setSoc(self, soc):
        assert 0.0 <= soc <= 1.0
        Q = self.getCharge(soc=soc)
        self.Q = Q 
    
    def getVoltage(self, Q=None, E=None, soc=None):
        if soc is not None:
            Q = self.getCharge(soc=soc)
        elif E is not None:
            Q = self.getCharge(E=E)            
        elif Q is None:
            Q = self.Q
        
        params = self.param
        V = params.voltBound[0] + Q * params.voltSlope
        return V

    def getSoc(self, Q=None, V=None):
        if V is not None:
            Q = self.getCharge(V=V)
            return Q / self.param.Qfull
        elif Q is None:
            Q = self.Q
        return Q / self.param.Qfull
        
    def getCharge(self, soc=None, E=None, V=None):
        Vl = self.param.voltBound[0]; slope = self.param.voltSlope;
        if V is not None:
            Q = (V - Vl) / slope
            return Q
        elif soc is not None:
            return self.param.Qfull * soc
        elif E is not None:
            #Mitternacht formula for E = Vl * Q + 0.5*l*Q^2 
            Q=1.0 / slope * (-Vl + math.sqrt(Vl**2 + 2*slope * E) )
            return Q        
        else:
            return self.Q    
    
    def getEnergy(self, Q=None, V=None):
        if V is not None:
            Q = self.getCharge(V=V)
        if Q is None:
            Q = self.Q
        params = self.param
        E1 = params.voltBound[0] * Q #energy value resulting from offset voltage
        E2 = 0.5 * params.voltSlope * Q**2 #energy value strictly from increase; cf capacitor 1/2 CV^2
        return E1 + E2
    
    def getBalancingTimeEstTillTarget(self, iPeakAmps, soc=None, E=None):
        Eold = self.getEnergy()
        Qold = self.getCharge()
        params = self.param
        
        if soc is not None:
            assert soc >= 0 and soc <= 1
            Qnew = self.getCharge(soc=soc)
        elif E is not None:
            assert E < self.getEnergy(Q=self.param.Qfull)
            assert E > 0
            Qnew = self.getCharge(E=E) 
        assert Qnew <= params.Qfull and Qnew >= 0
        
#         assert (Qnew - Qold) * iPeakAmps > 0 #current and dQ must have same sign!
        dT = (Qnew - Qold) / (0.25 * iPeakAmps) #negative current signifies taking out
        if dT < 0:
            return (-1)*dT, (-1)* iPeakAmps
        else:
            return dT, iPeakAmps
            
    def getLoadTimeEstTillTarget(self, iAmps, soc=None, E=None):
        return self.getBalancingTimeEstTillTarget(4.0 * iAmps, soc=soc, E=E);

    def getLosses(self):
        return self.losses
    
    def getName(self):
        return self.__module__
    
        

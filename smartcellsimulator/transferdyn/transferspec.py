import inspect
class TransferSpec(object):
    """Docstring for TransferSpec 
        E: send X VAs
        T: send for x seconds
        not implemented yet:
        V: send till voltage of sender decreases by V
        Q: send x As from sender (recv might differ)
    """

    def __init__(self, send=0, recv=1, iPeakAmps=1.0, amount=1.0, measure='E'):
        """@todo: to be defined1

        @param send @todo
        @param recv @todo
        @param iPeakAmps @todo
        @param amount @todo
        @param measure specifies the unit amount is measured in 

        """
        assert measure in ['E', 'T']
        self.send = send
        self.recv = recv
        self.iPeakAmps = iPeakAmps
        self.amount = amount
        self.measure = measure
        
    def __str__(self):
        members = inspect.getmembers(self, lambda attr: not callable(attr))
        attrs = vars(self)
#         return str(members)
        return "TransferSpec: " + ', '.join("%s: %s" % item for item in attrs.items())

    def __repr__(self):
        return "<" + self.__str__() + ">"
    
    def __eq__(self, other):
        return (isinstance(other, self.__class__) 
                and self.__dict__ == other.__dict__ )
        
    def __ne__(self, other):
        return not self.__eq__(other)
        
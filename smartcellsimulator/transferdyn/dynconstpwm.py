'''
Created on 27 Mar 2015

@author: matthias.kauer
'''
import math
import numpy as np
from collections import namedtuple
import scipy.optimize

from .dyninterface import DynInterface, DynLosses
from .dynboxparam import DynBoxParam

RecvPack = namedtuple('RecvPack', ['Vt0', 'Rt', 'numCycle', 'VtEnd', 'alph', 'd']) 

class DynConstPWM(DynInterface):
    '''
    classdocs
    '''

    def __init__(self, archParam=None, dynParam=None, soc=0.5, diodeCurrCorrection=True):
        super().__init__(archParam=archParam)

        if not dynParam:
            dynParam = DynBoxParam()
            
        assert isinstance(dynParam, DynBoxParam)
        self.param = dynParam
        
        self.diodeCurrCorrection=diodeCurrCorrection
        self.errorDiode = {'q1': 0.0, 'i1': 0.0}

        
        # self.losses = DynLosses(); self.losses.simu_switch = False; self.losses.simu_transfer = True;
#         voltMin = 3.0; Qfull = 1.1*3600; voltMax = 4.2;
#         self.param = DynBoxParam(voltMin=voltMin, Qfull=Qfull, 
#                             voltSlope=(voltMax - voltMin) / Qfull, voltMax=voltMax)
        
        self.V = 0;
        self.Wfull = self.getVoltage(Q=self.param.Qfull)**2
        self.Wmin = self.param.voltBound[0]**2
        self.setSoc(soc) #inits W  
    
    def setSoc(self, soc):
        self.V = soc * self.param.voltBound[1] + (1-soc) * self.param.voltBound[0]
        
    def getParam(self):
        return self.param
    
    def setParam(self, param):
        self.param = param
    
    def sendBalancing(self, toCell, resistances, iPeakAmps, tSec=None, dE=None):
        assert not (tSec is None and dE is None)
            
        if(tSec is not None):
            ttx = tSec;
        elif(dE is not None):
            targE = self.getEnergy() - dE
            ttx = self.getBalancingTimeEstTillTarget(toCell=toCell, 
                resistances=resistances, iPeakAmps=iPeakAmps, E=targE)

        res = resistances
        
        # assert t.measure == 'T', 'currently E transfer not implemented'
        #ttx = t.amount; iPeakAmps = t.iPeakAmps
        ap = self.archParam
        Ton, Toff, Tbreak = self.getTiming(toCell, resistances, iPeakAmps)
        Tcycle = Ton + Toff + Tbreak
        numCycle = self._getCycles(Tcycle, tSec=ttx)
        
        Vtx0 = self.V
        recvPack = self._applyBalSender(Rcirc=res.Rtx, numMtx=res.numMtx, iPeakAmps=iPeakAmps,
                     Ton=Ton, numCycle=numCycle)
        retNumCycle = toCell._applyBalReceiver(Rcircuit=res.Rrx, numMrx=res.numMrx, Ton=Ton, Toff=Toff,
                                Tbreak=Tbreak, recvPack=recvPack)

        if(retNumCycle<recvPack.numCycle):
            #receiver couldn't take everything; therefore sender can also send less in hindsight
            self.V = Vtx0
            recvPack = self._applyBalSender(Rcirc=res.Rtx, iPeakAmps=iPeakAmps,
                         Ton=Ton, numCycle=retNumCycle)
        
        return retNumCycle * Tcycle
        

        
    def getBalancingTimeEstTillTarget(self, toCell, resistances, iPeakAmps, soc=None, E=None, V=None):

        numCycle = self._getBalancingCycleEstTillTarget(RcircTx=resistances.Rtx,
                            iPeakAmps=iPeakAmps, soc=soc, E=E, V=V)

        Ton, Toff, Tbreak = self.getTiming(toCell, resistances, iPeakAmps)
        Tcycle = Ton + Toff + Tbreak

        
        return numCycle * Tcycle
        

    def _getBalancingCycleEstTillTarget(self, RcircTx, iPeakAmps, soc=None, E=None, V=None):
        Rtx = RcircTx + self.param.Rcell
#         Rrx = resistances.Rrx + toCell.param.Rcell
        print("1")
        Ton = self._getTon(Rtx=Rtx, iPeakAmps=iPeakAmps)
#         Ton, Toff, Tbreak = self.getTiming(toCell, resistances, iPeakAmps)
#         Tcycle = Ton + Toff + Tbreak

        if V is None:
            V = self.getVoltage(soc=soc, E=E)
        Vnew = V
        Vold = self.V
        
        assert Vnew <= Vold
        #assert iPeakAmps > 0

#         ap = self.archParam
        alpha, _ = self._getSenderParam(Rtx=Rtx, Ton=Ton)
        numCycle = math.log(V/self.V) / math.log(alpha)
        
        return math.floor(numCycle)


    def __Td(self, V, iDes, i0, R):
        ap = self.archParam
#         R = Rcirc + self.param.Rcell
        return -ap.LInd / R * math.log((V - iDes*R) / (V-i0*R))
        
    def _getTon(self, Rtx, iPeakAmps):
        V1 = self.getVoltage()
        Ton = self.__Td(V1, iDes=iPeakAmps, R=Rtx, i0=0.0)
        return Ton
        
    def getTiming(self, toCell, resistances, iPeakAmps):
        res = resistances
        ap = self.archParam
        
        V1 = self.V
        Ton = self._getTon(Rtx=res.Rtx + self.param.Rcell, iPeakAmps=iPeakAmps)
        V2 = toCell.getVoltage() 
        
        iReserve = iPeakAmps * 0.03
        Toff = toCell.__Td(-V2, iDes=iReserve, R=res.Rrx+toCell.param.Rcell, i0=iPeakAmps)

        
        #TODO: I'm currently not sure how to obtatin tSec in all places ( balancingTillTarget especially)
#         if(False): 
#             ToffEst = Toff
#             numCycleEst = self._getCycles(Ton+ToffEst, tSec)
#             
#             m = self.param.voltSlope
#             dQBluntEst = 0.25 * iPeakAmps * tSec
#             V1lin = V1 - m*dQBluntEst
#             #linear: Ton * V1lin / ap.LInd wouldn't work (was larger with smaller V1 than initial)
#             iPeakEnd =self._iHelper(V1lin, i0=0.0, R=res.Rtx+self.param.Rcell, T=Ton) 
#             m2 = toCell.param.voltSlope
#             V2lin = V2 + m2*dQBluntEst
#             
#             #now correct ToffEst based on end voltage estimates
#             Toff = toCell.__Td(-V2lin, iDes=0.01*iPeakAmps, R=res.Rrx+toCell.param.Rcell,
#                          i0=iPeakEnd)
        
        Tbreak = 0.1 * Toff
        return Ton, Toff, Tbreak

    @staticmethod
    def _getCycles(tCycle, tSec):
        numCycle = math.floor(tSec / tCycle)
        return numCycle
        
    @staticmethod
    def getDefaultParam():
        return DynBoxParam()
    
    def _applyBalSender(self, Rcirc, numMtx, iPeakAmps, Ton, numCycle):
        R = Rcirc + self.param.Rcell
        ap = self.archParam

        # m = self.param.voltSlope
        # a = Ton/R + ap.LInd / R**2 * (math.exp((-R/ap.LInd)*Ton) - 1)
        alpha, d = self._getSenderParam(Rtx=R, Ton=Ton)
        
        Vbeg = self.V
        Vend = alpha**numCycle * Vbeg
        
#         Vmax = self.param.voltBound[1]
        Vmin = self.param.voltBound[0]
#         if Vend > Vmax:
#             numCycle = self._getBalancingCycleEstTillTarget(RcircTx=Rcirc,
#                         iPeakAmps=iPeakAmps, V=Vmax)
#             Vend = Vmax
        if Vend < Vmin:
            numCycle = self._getBalancingCycleEstTillTarget(RcircTx=Rcirc,
                        iPeakAmps=iPeakAmps, V=Vmin)
            Vend = Vmin
            
        iBeg = iPeakAmps; iEnd = d*Vend; iAvg = 0.5*(iBeg+iEnd)
        tfl = self._getTfl(R=R, iPeak0=iBeg, iPeakN=iEnd, T=Ton, numCycle=numCycle)
#        tfl = Vbeg*(ap.LInd*Vbeg/R**2 * math.log10((Vbeg-iPeakAmps*R)/Vbeg) + ap.LInd * iPeakAmps/R)
        tfl2 = self._getTfl2(R=R, alph=alpha, d=d, Vt0=Vbeg, T=Ton, numCycle=numCycle)
        self.losses.tfL += tfl

        
        swl =  self._getSwl(V=Vbeg, iPeakAmp=iPeakAmps, numCycle=numCycle, numM=numMtx, typ='send')
        self.losses.swL += swl

#         if(not self.losses.simu_transfer):
#             Eend = self.getEnergy(V=Vend) + tfl
#             Vend = self.getVoltage(E=Eend)
            
        self.V = Vend
        
        recvPack = RecvPack(Vt0 = Vbeg, Rt=R, VtEnd=Vend, numCycle=numCycle,
                        alph=alpha, d=d)
        return recvPack

    def _getSwl(self, V, iPeakAmp, numCycle, numM, typ='send'):
        ap = self.archParam
        tsw = ap.toff 
        if(typ == 'recv'):
            tsw = ap.ton
        #send: 1 pwm switch, recv: 1 pwm switch and numM-1 slow switches (don t change position during one transfer process
        swl = (numCycle + (numM - 1)) * (0.5*tsw* abs(iPeakAmp) * V + 0.5 * ap.Coss * V**2)
        return swl

#    @staticmethod
    def _getTfl(self, R, iPeak0, iPeakN,T, numCycle):
        ap = self.archParam
        iAvg = 0.5*(iPeak0+iPeakN)
        tfl = R/3.0 * T * iAvg**2 * numCycle
        return tfl

    @staticmethod
    def _getTfl2(R, alph, d, Vt0, T, numCycle):
        sum_alph_k = (math.exp(numCycle*math.log(alph)) - 1.0) / math.log(alph)
        sum_Isq = d**2 * Vt0**2 * sum_alph_k
        tfl = R/3.0 * T * sum_Isq
        return tfl
#         return R/3.0 *T * (iPeakN**3 - iPeak0**3)/3.0

    def _getSenderParam(self, Rtx, Ton):
        ap = self.archParam
        m = self.param.voltSlope
        L = ap.LInd
        
        a = Ton/Rtx + L / Rtx**2 * (math.exp((-Rtx/L)*Ton) - 1)
        d = 1.0 / Rtx * (1-math.exp(-Rtx/L * Ton))
        
        alph = (1-a*m)
        
        if(self.losses.simu_switch):
            alph -= m/2.0 * (ap.Coss + d*ap.toff)
            
        return alph, d
    
    def _getReceiverParam(self, Rr,Toff, recvPack):
        ap = self.archParam
        m = self.param.voltSlope
        L = ap.LInd

#         alph,d = self._getSenderParam(Rtx=Rt, Ton=Ton)
        alph = recvPack.alph
        d = recvPack.d
        
        c = -L / Rr * (math.exp(-Rr/L*Toff) -1)
        b = -Toff / Rr + c/Rr
        
#         a = Ton/Rt + L / Rt**2 * (math.exp((-Rt/L)*Ton) - 1)
        
#         alph = (1-a*m)
        thet = c*d*m
        beta = 1 + b*m
        gamm = 0

        if(self.losses.simu_switch):
            beta -= m/2.0 * ap.Coss
            thet -= m/2.0 * d * ap.ton
            
        if(self.diodeCurrCorrection):
            Vr0 = self.V
            k = recvPack.numCycle
            VtEnd = recvPack.VtEnd
            i1Wc = self._i1Helper(V=Vr0, i0=d*VtEnd, Rr=Rr, Toff=Toff)
            
            qrx_diode = i1Wc**2 *L / (2*(ap.Vd+Vr0))
            gamm += m * qrx_diode
#             -(((-d)*Rr*Vend - Vr[0])/(E^((Rr*Toff)/L)*Rr)) - 
#       Vr[0]/Rr)^2)/(2*(Vd + Vr[0]))

        return alph, beta, gamm, thet, d
 

    def getBalancingLossBounds(self, toCell, resistances, iPeakAmps, tSec=None, dE=None):
        if(tSec is not None):
            ttx = tSec;  
        elif(dE is not None):
            targE = self.getEnergy() - dE
            ttx = self.getBalancingTimeEstTillTarget(toCell=toCell, 
                resistances=resistances, iPeakAmps=iPeakAmps, E=targE)

        Ton, Toff, Tbreak = self.getTiming(toCell, resistances, iPeakAmps)
        Tcycle = Ton + Toff + Tbreak
        numCycle = self._getCycles(Tcycle, tSec=ttx) 

    def _applyBalReceiver(self, Rcircuit, numMrx, Ton, Toff, Tbreak, recvPack):
        ap = self.archParam; p = self.param;
        
        Rr = Rcircuit + self.param.Rcell
        Rt = recvPack.Rt
        Vtx0 = recvPack.Vt0
        numCycle = recvPack.numCycle
        Vd = ap.Vd
        
#         alph = self.__getSenderParam(Rt=Rt, Ton=Ton, L=ap.LInd, m=self.param.voltSlope)
        
        alph, beta, gamm, thet, d =self._getReceiverParam(Rr=Rr, Toff=Toff, recvPack=recvPack)

        Vbeg = self.V
        Vend = self._VxHelper(Vrx0=Vbeg, Vtx0=Vtx0, numCycle=numCycle,
                 alph=alph, thet=thet, beta=beta, gamm=gamm)

        Vmax = self.param.voltBound[1]
        if Vend > Vmax:
#             numCycle = self._getBalancingCycleEstTillTarget(Rt=recvPack.Rt,
#                         iPeakAmps=recvPack.iPeakAmps, V=Vmax)
            numCycle = self._getBalancingCycleEstTillTargetReceiver(Vtarg=Vmax, Vtx0=recvPack.Vt0,
                            alph=alph, thet=thet, beta=beta, gamm=gamm)
            Vend = Vmax
        self.V = Vend
        
        #make sure that i=0 happens after Toff, but before Tbreak ends
        iBeg = d * recvPack.Vt0
        iEnd = d*recvPack.VtEnd;
        i1Beg = self._i1Helper(V=Vbeg, i0=iBeg, Rr=Rr, Toff=Toff)
        i2Beg = self._i1Helper(V=Vbeg+Vd, i0=i1Beg, Rr=Rr, Toff=Tbreak)
        i1End = self._i1Helper(V=Vend, i0=iEnd, Rr=Rr, Toff=Toff)
        i2End = self._i1Helper(V=Vend+Vd, i0=i1End, Rr=Rr, Toff=Tbreak)
        #assert i1Beg > 0
        #assert i2Beg < 0
        #assert i1End > 0
        #assert i2End < 0
        
        #log switching and transfer losses
#         tfl = Rr/3.0 * iAvg**2 *Toff* numCycle
        tfl = self._getTfl(R=Rr, iPeak0=iBeg, iPeakN=iEnd, T=Toff, numCycle=numCycle)
#        tfl = Vbeg*(ap.LInd*Vbeg/Rr**2 * math.log10((Vbeg)/(Vbeg+iBeg*Rr) + ap.LInd * iBeg/Rr))
        tfl2 = self._getTfl2(R=Rr, alph=alph, d=d, Vt0=Vtx0, T=Toff, numCycle=numCycle)
        
        self.losses.tfL += tfl
        
#         swl = numCycle * (0.5*ap.ton * abs(iBeg) * Vbeg + 0.5 * ap.Coss * Vbeg**2) 
        swl =  self._getSwl(V=Vbeg, iPeakAmp=-iBeg, numCycle=numCycle, numM=numMrx, typ='recv')
        self.losses.swL += swl

#         if(not self.losses.simu_transfer):
#             Eend = self.getEnergy(V=Vend) + tfl
#             Vend = self.getVoltage(E=Eend)
 
        #evaluate our blunt i1 estimate
        if(self.diodeCurrCorrection):
            i1Wc = self._i1Helper(V=Vbeg, i0=iEnd, Rr=Rr, Toff=Toff)
            max_curr_err = max(abs(i1Wc-i1Beg), abs(i1Wc-i1End))
            
            qdiodeWc = i1Wc**2 *ap.LInd / (2*(ap.Vd+Vbeg)) #worst case charge
            qBeg = i1Beg**2 *ap.LInd / (2*(ap.Vd+Vbeg)) #charge at beginning
            qEnd = i1End**2 *ap.LInd / (2*(ap.Vd+Vend)) #charge at end
            max_charge_err = max(abs(qdiodeWc-qBeg), abs(qdiodeWc-qEnd))
            
#             print("max_err curr/charge: ", max_curr_err, max_charge_err)
            self.errorDiode['q1'] = max(self.errorDiode['q1'],max_charge_err)
            self.errorDiode['i1'] = max(self.errorDiode['i1'],max_curr_err)
                    
        return numCycle
        
#         print("i1", i1Beg, i2Beg, i1End, i2End)
    
    def _getBalancingCycleEstTillTargetReceiver(self, Vtarg, Vtx0, alph, thet, beta, gamm):
        solveFunc = lambda k : Vtarg - self._VxHelper(Vrx0=self.V, Vtx0=Vtx0, numCycle=k,
                     alph=alph, thet=thet, beta=beta, gamm=gamm)
        if(solveFunc(0) < 1e-10):
            return 0
        numCycle = scipy.optimize.broyden1(solveFunc, [0], f_tol = 1e-8)
        return numCycle

    def _iHelper(self, V, i0, R, T):
        ap = self.archParam
        return V/R - (V-i0*R)/R * math.exp(-R/ap.LInd *T)
    
    def _i1Helper(self, V, i0, Rr, Toff):
        ap = self.archParam
        return (-V)/Rr - (-V-i0*Rr)/Rr * math.exp(-Rr/ap.LInd *Toff)
    
    @staticmethod
    def _VxHelper(Vrx0, Vtx0, numCycle, alph, thet, beta, gamm):
        k = numCycle
        assert k>=0
#         if(k==0):
#             return Vrx0
        Vx0 = Vrx0 + thet*Vtx0/(beta-alph)
        VxEnd = beta**k*Vx0  + (-gamm + beta**k*gamm)/(beta-1)
        Vend = VxEnd - alph**k * thet * Vtx0 / (beta-alph)
        
        return Vend
    
#     def _applyBalReceiver0(self, Rcircuit, Ton, Toff, Tbreak, recvPack):
#         ap = self.archParam
#         Rr = Rcircuit + self.param.Rcell
#         L = ap.LInd
#         Rtx = recvPack.Rtx
#         Vtx0 = recvPack.Vtx0
#         numCycle = recvPack.numCycle
# 
#         
#         Vbeg = self.V
# 
#         p = self.param
#         m = p.voltSlope
#         #idea: linearize 1/V = invSlope * V + invSlopeOffset
#         Vmin=p.voltBound[0]; Vmax = p.voltBound[1]
#         invSlope = (1.0/Vmax - 1.0/Vmin) / (Vmax-Vmin)
# #         invSlopeOffset = -p.voltMin * invSlope 
# 
#         d = 1.0 / Rtx * (1-math.exp(-Rtx/L * Ton))
#         iEst = d * 0.5*(recvPack.Vtx0 + recvPack.VtxEnd) 
#                 
#         c = -L / Rr * (math.exp(-Rr/L*Toff) -1)
#         b = -Toff / Rr +L/Rr *c
#         a = Ton/Rtx + L / Rtx**2 * (math.exp((-Rtx/L)*Ton) - 1)
#         
#         iLastSq = iEst**2 
#         
#         Vd = ap.Vd
#         
#         thet = c*d*m - d * math.exp(-2*Rr*Toff/L)*L*m/Rr - d * math.exp(-Rr*Toff/L)*L*m/Rr
#         
#         beta = 1 + b*m + (L*m)/(2*Rr**2)
#         beta += (L*m)*(math.exp((-2*Rr*Toff)/L)/(2*Rr**2)); 
#         beta += (L*m)*(math.exp(-(Rr*Toff)/L)/Rr**2);
#         beta += iLastSq*L*m* invSlope *math.exp((2*Rr*Toff)/L)/2.0 
#         
#         gamm = (iLastSq*L*m)* math.exp((2*Rr*Toff)/L)/(2*Vmin)
#         gamm -=(iLastSq*L*m*invSlope*Vmin)*math.exp((2*Rr*Toff)/L)/2.0 
#         
#         gamm2 = (L*m*Vd)/(2*Rr**2) + (L*m*Vd)*math.exp((2*Rr*Toff)/L)/(2*Rr**2)
#         gamm2 += (L*m*Vd)*math.exp((Rr*Toff)/L)/Rr**2
#         
#         gamm += gamm2
#      
#         alph = (1-a*m)
#         
#         assert beta > 1, "receiver voltage should increase"
#         assert alph < 1, "sender voltage should decrease"
#         
#         
#         Vx0 = Vbeg + thet*Vtx0/(beta-alph) 
#         k = numCycle
#         VxEnd = -gamm + beta**k*gamm -beta**k*Vx0 + beta**(k+1)*Vx0
#         VxEnd /= (beta-1)
#         Vend = VxEnd + alph**k * thet * Vtx0 / (beta-alph)
#         
#         self.V = Vend

    def applyLoadCurrent(self, iAmps, tSec):
        assert tSec > 0
        dQ = iAmps * tSec;
        Qold = self.getCharge()
        
        tRet = tSec
        if Qold + dQ > self.param.Qfull:
            dQ = self.param.Qfull - Qold
        elif Qold + dQ < 0:
            dQ = 0 - Qold
        tRet = dQ / iAmps
        
        tfL = self.param.Rcell * iAmps**2 * tRet
   
        Qnew = Qold + dQ
        Enew = self.getEnergy(Q=Qnew)
        Vnew = self.getVoltage(E=Enew - tfL)
        self.V = Vnew
        
        return tRet

    def getLoadTimeEstTillTarget(self, iAmps, soc=None, E=None):
#         Wold = self.W
        Qold = self.getCharge()
        
        if soc is not None:
            assert soc >= 0 and soc <= 1
            Qnew = self.getCharge(soc=soc)
#             Wnew = self.getVoltage(Q=Qnew)**2
        elif E is not None:
            assert E < self.getEnergy(Q=self.param.Qfull)
            assert E > 0
#             Wnew = self.getW(E=E)
            self.getCharge(E=E)
        
#         assert Wnew <= self.Wfull and Wnew >= self.Wmin
        assert Qnew <= self.param.Qfull and Qnew >= 0

        assert np.sign(iAmps) == np.sign(Qold-Qnew)

        dT = (Qnew - Qold) /  iAmps #negative current signifies taking out
#         if dT < 0:
#             return (-1)*dT, (-1)* iAmps
#         else:
#             return dT, iAmps
        
        
    def getCharge(self, soc=None, E=None):
        if E is not None:
            V = self.getVoltage(E=E)
            soc = self.getSoc(V=V)
        elif soc is None:
            soc = self.getSoc();
        Q = self.param.Qfull * soc;
        return Q
    
    def getSoc(self, Q=None, V=None):
        if Q is not None:
            V = self.getVoltage(Q=Q);
        elif V is None:
            V = self.getVoltage()
        
        p = self.param;
        return (V - p.voltBound[0]) / (p.voltBound[1] - p.voltBound[0])

    def getEnergy(self, Q=None, V=None):
        p= self.param
        if Q is not None:
            E1 = p.voltBound[0] * Q #energy value resulting from offset voltage
            E2 = 0.5 * p.voltSlope * Q**2 #energy value strictly from increase; cf capacitor 1/2 CV^2
            return E1 + E2
        else:
            if V is None:
                V = self.V
            W = V**2
            E = (W - p.voltBound[0]**2) / (2*p.voltSlope)
            return E
    
    def getVoltage(self,Q=None, E=None, soc=None):
        p = self.param
        if soc is not None:
            Q = self.getCharge(soc=soc)
            return self.getVoltage(Q=Q)
        elif Q is not None:
            V = p.voltBound[0] +  Q * p.voltSlope;
        elif E is not None:
            W = (2.0 * self.param.voltSlope) * E + p.voltBound[0]**2
            V = math.sqrt(W)
        else:
            V = self.V
        return V
        
    def getLosses(self):
        return self.losses
    
    def getName(self):
        return self.__module__
        

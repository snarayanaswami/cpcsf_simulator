"""
=========================================
Contains the data of options and settings
=========================================

**file** :GlobalData.py

**date** : 3-July-2015
"""

import simpy
from .CANBus import CANBus


class GlobalData(object):
    def __init__(self):
        self.env = None
        self.canBus = None
        self.plots = []

        self.tBalTotal = 0

        self.OptionRandomSeed = 1000
        self.OptionConcurrencyDeg = 1
        self.OptionConcurrent = False
        self.OptionTimebasedBC = False
        self.OptionMinBC = False
        self.OptionMaxBC = False
        self.OptionLimitTimebasedBC = False
        self.OptionLimitBound = 0.0001
        self.OptionBCperiod = 5
        self.OptionBCAfterTransaction = True
        self.OptionStrategy = []
        self.OptionAdaptTransferTime = False
        self.OptionStartDiff = 0.01
        self.OptionStopDiff = 0.005
        self.OptionMaxTest = False

        self.OptionFollowCurve = False
        self.followCurveDataFile = None
        self.followCurveCurrentDivider = 12
        self.followCurveCycleRepetitions = 1

        self.OptionEvaluateDemonstrator = False
        self.evaluationSettings = None

        self.OptionDiffPlot = False
        self.OptionVoltagePlot = False

        self.startPackSOC = None
        self.finalPackSOC = None
        self.startPackCharge = None
        self.finalPackCharge = None

        self.plotWindow = None
        self.plotWindow2 = None
        #self.charger = None
        #self.discharger = None
        self.simpy = simpy

        # Battery pack settings
        self.numberOfCells = None
        self.cellCapacity = None
        self.initialSoc = None
        self.initialSocDist = None

        self.transferTime = 10

        # Amps, note that this is now peak current, resulting in 1/4 average current over whole transfer time
        self.transferRate = 1.2

        self.socListInit = []
        self.socListEnd = None

        self.pack = None
        self.flowControl = None
        self.minTimeStep = None
        #self.allPlots = None
        self.simulationStop = None
        self.stopSimulation = False
        self.starttime = None
        self.barColors = None
        self.monitor = None
        self.livePlot = False
        self.barChartLive = False
        self.CANchart = False
        self.window = None
        env = simpy.Environment()

        self.env = env

        self.canBus = CANBus(self)

        '''for range batch test'''
        self.rangeValue = 95
        self.rangeStart = 95
        self.rangeEnd = 95

    def guiPrint(self, printString):
        self.window.listWidget.addItem(printString)
        # numberOfLines=self.window.listWidget.count()
        # lineItem=self.window.listWidget.item(numberOfLines)
        self.window.listWidget.scrollToBottom()

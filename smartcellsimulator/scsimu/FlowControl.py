"""
=================================================
Control of charging, balancing and load-profiling
=================================================

**file** : FlowControl.py

**date** : 3-July-2015
"""

from random import random
import time
import csv
from .CANmessage import CANmessage

class FlowControl(object):
    """
    Class containing methods for controlling the flow of events for charging, discharging and load-profiling of the battery-pack.
    
    """
    def __init__(self,globalData):
        self.globalData = globalData
        
        if self.globalData.OptionFollowCurve:
            self.globalData.env.process(self.followCurve())
        elif self.globalData.OptionEvaluateDemonstrator:
            self.globalData.env.process(self.evaluateDemonstrator())
        else:
            self.globalData.env.process(self.directBalance())
            
        self.globalData.simulationStop = self.globalData.env.process(self.simulationControl())
        self.globalData.canBus.addMessageSubscriber(self.messageHandler)
        self.stopSimulation = False
    
    def sendMessage(self,msg):
        self.globalData.env.process(self.globalData.canBus.CANsend(msg)) 
     
    def sendAt(self,time,msg):
        if self.globalData.env.now == time:
            self.sendMessage(msg)
        
    def chargeDischargeBalance(self):
        """
        Charges the pack for a given time, discharges it to diverge the pack and its individual SoC values and then starts active balancing process.
        """
        while True:
            # Charge the battery pack with the given 'rate':
            self.sendAt(time=1, msg=CANmessage(self.globalData.canBus.CAN_SUPPLY_LOAD_MODE, {'SupplyLoadMode':'applyLoad', 'rate':2, 'duration':self.globalData.minTimeStep}))
            # Discharge the battery pack with the given 'rate':
            self.sendAt(time=2000, msg=CANmessage(self.globalData.canBus.CAN_SUPPLY_LOAD_MODE, {'SupplyLoadMode':'applyLoad', 'rate':-2, 'duration':self.globalData.minTimeStep}))
            # Disable the load/supply module:
            self.sendAt(time=3000, msg=CANmessage(self.globalData.canBus.CAN_SUPPLY_LOAD_MODE, {'SupplyLoadMode':'idle'}))
            # Enable active balancing:
            self.sendAt(time=3100, msg=CANmessage(self.globalData.canBus.CAN_BALANCE_CONTROL, {'activeBalancingControl':True}))
        
            yield self.globalData.env.timeout(self.globalData.minTimeStep)

    def followCurve(self):
        """
        Loads discharge profile values from '*.csv-file' to a list of values and applies the values to the battery pack.        
        """
        for smartcell in self.globalData.pack.smartCells:
                smartcell.cell.setSoc(self.globalData.initialSoc+self.globalData.initialSocDist*random())
                
        self.globalData.startPackSOC = self.globalData.pack.getSoc()
        self.globalData.guiPrint("Pack SoC is "+str(self.globalData.pack.getSoc()*100)+"%")
        
        self.globalData.startPackCharge = self.globalData.pack.getCoulomb()
        self.globalData.guiPrint("Pack stored Charge is "+str(self.globalData.pack.getCoulomb()/3600)+" Ah")
        
        self.globalData.startPackEnergy = self.globalData.pack.getEnergy()
        self.globalData.guiPrint("Pack stored Energy is "+str(self.globalData.startPackEnergy/3600)+" Wh")
        
        CurveData = []
        with open(self.globalData.followCurveDataFile, newline='') as infile:
            CSVReadIn = csv.reader(infile, delimiter=',', quotechar='|')
            for row in CSVReadIn:
                CurveData.append(row)
        
        length = len(CurveData)
        for repetitions in range(self.globalData.followCurveCycleRepetitions):
            for item in range(length):
                CurveData.append(CurveData[item])
  
        print("Curve goes from {} to {}".format(int(CurveData[0][0]), int(CurveData[-1][0])))
        
        while True:
            
            if self.globalData.env.now < len(CurveData):
                #Fast implementation of curve-following -> only for second based load-curves
                duration = 1
                data = CurveData[int(self.globalData.env.now)]
                
                # Load is given in kWh
                # Multiply by 1000 for Wh
                # Divide by pack-voltage for 
                rate = float(data[1]) * 1000 / self.globalData.pack.getVoltage() / 96 * self.globalData.numberOfCells / self.globalData.followCurveCurrentDivider
                self.sendAt(time=self.globalData.env.now, msg=CANmessage(self.globalData.canBus.CAN_SUPPLY_LOAD_MODE, {'SupplyLoadMode':'applyLoad', 'rate':rate, 'duration': duration}))
                
            # Charge and discharge battery pack according to values in *.csv file
            self.sendAt(time=len(CurveData)+1, msg=CANmessage(self.globalData.canBus.CAN_SUPPLY_LOAD_MODE, {'SupplyLoadMode':'idle'}))
            yield self.globalData.env.timeout(self.globalData.minTimeStep)

    def directBalance(self):
        """
        Start active cell balancing right away.        
        """
#         for smartcell in self.globalData.pack.smartCells:
#                 smartcell.cell.setSoc(self.globalData.initialSoc+self.globalData.initialSocDist*random())
        
        self.globalData.startPackSOC = self.globalData.pack.getSoc()
        self.globalData.guiPrint("Pack SoC is "+str(self.globalData.startPackSOC*100)+"%")
        
        self.globalData.startPackCharge = self.globalData.pack.getCoulomb()
        self.globalData.guiPrint("Pack stored Charge is "+str(self.globalData.startPackCharge/3600)+" Ah")
        
        self.globalData.startPackEnergy = self.globalData.pack.getEnergy()
        self.globalData.guiPrint("Pack stored Energy is "+str(self.globalData.startPackEnergy/3600)+" Wh")
        
        #self.globalData.socListInit.append(item.cmu.cellSOC())
        while True:#             
            self.sendAt(time=0, msg=CANmessage(self.globalData.canBus.CAN_BALANCE_CONTROL, {'activeBalancingControl':True}))                        
            yield self.globalData.env.timeout(self.globalData.minTimeStep)
            
    def evaluateDemonstrator(self):
        """
        Simulation with five cells and specified initial cell SoC states.
        """
        for index, smartCell in enumerate(self.globalData.pack.smartCells):
            dictKey = 'socCell{}'.format(index)
            smartCell.cell.setSoc(self.globalData.evaluationSettings[dictKey])
        
        self.globalData.startPackSOC = self.globalData.pack.getSoc()
        self.globalData.guiPrint("Pack SoC is "+str(self.globalData.startPackSOC*100)+"%")
        
        self.globalData.startPackCharge = self.globalData.pack.getCoulomb()
        self.globalData.guiPrint("Pack stored Charge is "+str(self.globalData.startPackCharge/3600)+" Ah")
        
        self.globalData.startPackEnergy = self.globalData.pack.getEnergy()
        self.globalData.guiPrint("Pack stored Energy is "+str(self.globalData.startPackEnergy/3600)+" Wh")

        while True:
            self.sendAt(time=0, msg=CANmessage(self.globalData.canBus.CAN_BALANCE_CONTROL, {'activeBalancingControl':True}))
            yield self.globalData.env.timeout(self.globalData.minTimeStep)
            
    
    def simulationControl(self):
        """
        Stop simulation when active balancing procedure is done.
        """
        while (not self.stopSimulation) and (self.globalData.env.now < self.globalData.simDuration):
            yield self.globalData.env.timeout(1)
        self.globalData.stopSimulation = True
        self.globalData.guiPrint("############################################################")
        self.globalData.guiPrint("Simulation ended after {} simulation seconds".format(self.globalData.env.now))
        self.globalData.guiPrint("############################################################")
        endtime=time.time()
        self.globalData.guiPrint("Simulation runtime is {:.2f} seconds".format(endtime-self.globalData.starttime))
        self.globalData.monitor.refreshViewForce()
        
        
    def messageHandler(self,msg):
        if msg.identifier == self.globalData.canBus.CAN_BALANCE_CONTROL and msg.data["activeBalancingControl"] is False:            
            self.stopSimulation = True
            self.globalData.accumulatedLosses = self.globalData.pack.getLosses()
            self.globalData.guiPrint("Balancing successful after {} seconds".format(self.globalData.env.now)) # = real balancing time
            self.globalData.guiPrint("Total CAN Messages sent: {}".format(self.globalData.canBus.messageCount))
            self.globalData.guiPrint("Total Data sent: {} Byte".format(self.globalData.canBus.dataCount/8))
            self.globalData.finalPackSOC = self.globalData.pack.getSoc()
            self.globalData.guiPrint("Pack SoC is {} %".format(self.globalData.pack.getSoc()*100))
            self.globalData.finalPackCharge = self.globalData.pack.getCoulomb()
            self.globalData.guiPrint("Pack stored Charge is {} Ah".format(self.globalData.pack.getCoulomb()/3600))
            self.globalData.finalPackEnergy = self.globalData.pack.getEnergy()
            self.globalData.guiPrint("Pack stored Energy is {} Wh".format(self.globalData.finalPackEnergy/3600))
            self.globalData.guiPrint("Accumulated Losses: {} Wh".format((self.globalData.accumulatedLosses['swL']+self.globalData.accumulatedLosses['tfL'])/3600))
            self.globalData.guiPrint("Switching Losses: {} Wh".format((self.globalData.accumulatedLosses['swL'])/3600)+" "+"{} J".format((self.globalData.accumulatedLosses['swL'])))
            self.globalData.guiPrint("Calculated Losses: {} Wh".format((self.globalData.startPackEnergy/3600-self.globalData.finalPackEnergy/3600))) #for comparison purposes only
            #self.globalData.guiPrint("Total Balancing Time: {}".format(self.globalData.tBalTotal)) #value does not offer meaningful comparison (by Master student Arne Meeuw)
        yield self.globalData.env.exit()    
        
    
            
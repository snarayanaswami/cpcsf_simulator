"""
==================================================
Module holding the class representing a Smart Cell
==================================================

**file** : SmartCell.py

**date** : 3-July-2015
"""

from .Cell import Cell
from .CMU import CMU

class SmartCell(object):
    def __init__(self,globalData,objectId,arch):
        self.env=globalData.env
        self.objectId=objectId
        self.cell=Cell(globalData,objectId,arch)
        self.cmu=CMU(globalData,objectId,self.cell)
        self.globalData=globalData
        
    def printStats(self):
        print(("Cell ID %d has a SOC of %f%% with a charge of %d As of overall capacity %d As and a voltage of %f V")%(self.objectId,self.cmu.cellSOC()*100,self.cell.charge_as.level,self.cell.charge_as.capacity,self.cell.getVoltage()))
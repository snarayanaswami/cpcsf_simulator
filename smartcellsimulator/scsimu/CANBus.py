"""
===========================================
CAN bus system for inter-cell communication
===========================================

**file** : CANBus.py

**date** : 3-July-2015
"""

import simpy
from _collections import deque
from numpy import mean

CAN_BUFFER = 100
class CANBus(object):
    """
    A Broadcast pipe that allows one process to send messages to many.

    This construct is useful when message consumers are running at
    different rates than message generators and provides an event
    buffering to the consuming processes.

    The parameters are used to create a new
    
    Args:
        :class:`~simpy.resources.store.Store` instance each time
        :meth:`get_output_conn()` is called.

    """

    #CAN-Message defines:
    
    CAN_SOC_BROADCAST =       [0x0001, 32]   # ('TARGET':'BROADCAST', 'ORIGIN':, 'soc' :)
    CAN_VOLTAGE_BROADCAST =   [0x0002, 32]   # ('TARGET':'BROADCAST', 'ORIGIN':, 'voltage' :)
    CAN_BLOCK_REQUEST =       [0x0010, 16]   # ('TARGET':'BROADCAST', 'ORIGIN':, 'SENDER_ID':, 'RECEIVER_ID':)
    CAN_UNBLOCK_REQUEST =     [0x0011, 16]   # ('TARGET':'BROADCAST', 'ORIGIN':, 'SENDER_ID':, 'RECEIVER_ID':)
                               
    CAN_SEND_REQUEST =        [0x0012, 0]    # ('TARGET':, 'ORIGIN':)
    CAN_SEND_ACKNOWLEDGE =    [0x0013, 64]   # ('TARGET':, 'ORIGIN':, 'transferTime':, 'transferRate')    
    
    CAN_STATUS_RESPONSE =     [0x0020, 24]   # ('TARGET':, 'ORIGIN':, 'STATUS', 'BLOCKERID1', 'BLOCKERID2')
    
    
    CAN_BALANCE_CONTROL =     [0x00A0, 0]
    CAN_SUPPLY_LOAD_MODE =    [0x00A1, -66]

    CAN_RANGE_BROADCAST =     [0x0003, 32]


    def __init__(self, globalData, capacity=1):
        self.globalData = globalData
        self.env = globalData.env
        self.capacity = capacity
        self.pipes = []
        self.messageSubscribers = []
        self.canBusConnection = self.connect()
        self.averageQueueLength = 0
        self.maxQueueLength = 0
        self.averageTransmissiontime = 0
        self.maxTransmissiontime = 0
        self.averageQueueLengthSamples = 0
        self.averageTransmissiontimeSamples = 0
        self.canBus = self.globalData.canBus
        
        self.prevT = -1
        
        self.messageCount = 0
        self.freq = 0
        self.prevMsgCount = 0
        self.sumFreq = 0
        self.avgFreq = 0
        
        self.dataCount = 0
        self.speed = 0
        self.prevDataCount = 0
        self.sumSpeed = deque(maxlen=100)
        self.avgSpeed = 0
        
        self.env.process(self.CANreceive())

    def put(self, msg):
        '@type msg: CANmessage'
        """
        Broadcast a message to all receivers.
        
        Args:
            msg : CANmessage to send out
        """
        
        yield self.env.timeout(0.001)
        if not self.pipes:
            raise RuntimeError('There are no output pipes.')
        #events = [store.put(msg) for store in self.pipes]
        for store in self.pipes:
            store.put(msg) 
        
        if len(self.pipes[0].put_queue) > self.maxQueueLength:
            self.maxQueueLength = len(self.pipes[0].put_queue)
            
        self.averageQueueLength += len(self.pipes[0].put_queue)
        self.averageQueueLengthSamples += 1.0
        
        if len(self.pipes[0].put_queue) > CAN_BUFFER:
            raise RuntimeError('CAN Buffer overflow!!! '+ str(len(self.pipes[0].put_queue)) + " messages in queue!")
        #return self.env.all_of(events)  # Condition event for all "events"
        self.messageCount += 1
        self.dataCount += 1 + 29 + 1 + 1 + 6 + msg.identifier[1] + 16 + 2 + 7 + 3 # [Start Bit][extended ID][SRR Bit][IDE Bit][CTRL Bits][Data Bits][CRC Bits][ACK Bits][EOF Bits][IFS Bits]
        
        
        dt = self.env.now - self.prevT
        if dt >= self.globalData.minTimeStep:
            dMsgCount = self.messageCount - self.prevMsgCount
            dDataCount = self.dataCount - self.prevDataCount
            self.freq = dMsgCount/dt
            self.speed = dDataCount/dt
            self.sumFreq += self.freq
            self.sumSpeed.append(self.speed)
            self.avgFreq = self.sumFreq/(self.env.now+1)
            self.avgSpeed = mean(self.sumSpeed)
            self.prevT = self.env.now
            self.prevMsgCount = self.messageCount
            self.prevDataCount = self.dataCount
        
    def connect(self):
        """
        Get a new output connection for this broadcast pipe.

        Returns:
            pipe (Store) : :class:`~simpy.resources.store.Store`.

        """
        pipe = simpy.Store(self.env, capacity=self.capacity)
        self.pipes.append(pipe)
        return pipe
    
    def CANreceive(self):
        '@type msg: CANmessage'
        
        #print("Starting CAN message receiver")
        while True:
            msg = yield self.canBusConnection.get()
            
            yield self.env.timeout(0.001)
            msg.receiveTime = self.env.now
            #print('at time %f: %i received message objectId: %s.' %(self.env.now, self.objectId, msg.objectIdentifier))
            #print msg.data
        
            if (msg.receiveTime-msg.queueTime) > self.maxTransmissiontime:
                self.maxTransmissiontime = msg.receiveTime - msg.queueTime
            self.averageTransmissiontime += msg.receiveTime - msg.queueTime
            self.averageTransmissiontimeSamples += 1.0
            for sub in self.messageSubscribers:
                self.env.process(sub(msg))
                #print("Calling "+ sub.__name__)
            
                
    def CANsend(self, msg):
        '@type msg: CANmessage'
        msg.queueTime = self.globalData.env.now
        
        yield self.env.process(self.put(msg))
        #print("CANSend"+str(msg.data))
        #self.env.process(self.put(msg))
        #yield self.put(msg)
    
    def addMessageSubscriber(self,subObject):
        #print("Added MessageSubscriber " + subObject.__name__)
        self.messageSubscribers.append(subObject)
        
    def getAverageQueueLength(self):
        if self.averageQueueLengthSamples > 0:
            retval = self.averageQueueLength / self.averageQueueLengthSamples
        else:
            retval = 0
        self.averageQueueLength = 0
        self.averageQueueLengthSamples = 0
        return retval
        
    def getAverageTransmissiontime(self):
        if self.averageTransmissiontimeSamples > 0:
            retval = self.averageTransmissiontime / self.averageTransmissiontimeSamples
        else:
            retval = 0
        self.averageTransmissiontime = 0
        self.averageTransmissiontimeSamples = 0
        return retval
    
    def getMaxQueueLength(self):
        retval = self.maxQueueLength
        self.maxQueueLength = 0
        return retval
        
    def getMaxTransmissiontime(self):
        retval = self.maxTransmissiontime
        self.maxTransmissiontime = 0
        return retval
"""
==========================================
Logic for the container with battery cells
========================================== 

**file** : BatteryPack.py

**date** : 3-July-2015
"""

from .SmartCell import SmartCell
from .SupplyLoad import SupplyLoad
import random
from smartcellsimulator.transferdyn.archkutkut import ArchKutkut
from smartcellsimulator.transferdyn.dynconstpwm import DynConstPWM
#from smartcellsimulator.transferdyn.dynconstpwmstep import DynConstPWMStep
from smartcellsimulator.transferdyn.dynbox import DynBoxParam
from smartcellsimulator.transferdyn.archbuilder import ArchBuilder

class BatteryPack(object):
    """
    Class representing a container for the set of smart cells in the battery pack that is simulated.
    """
    def __init__(self, globalData):
        self.globalData=globalData
        self.env=globalData.env
        self.numberOfCells=globalData.numberOfCells
        self.smartCells=[]
        
        archbuilder = ArchBuilder(self.globalData.OptionRandomSeed)
        archbuilder.setScenario(scenario_str='inductornnbest')
        '''scenarios: default, 24green, 24green_nrg, flyback, inductornn, inductornnbest'''
        '''for nonneighbour, only flyback and inductornn are suitable'''
        archbuilder.numCell = self.numberOfCells

        # This method of soc initialization is deprecated
        # TODO update soc initialization
        
        self.arch = archbuilder.build()

        for i in range(self.numberOfCells):
            sc = SmartCell(globalData, i, self.arch)
            self.smartCells.append(sc)
            #print("Cell " + str(i)+ " capacity is "+str(self.smartCells[i-1].cell.charge_as.capacity))
            
        self.socList = [0 for _ in range(self.numberOfCells)]
        self.supplyLoad = SupplyLoad(globalData)
             
    def getSoc(self):
        minSoC = 1
        for sc in self.smartCells:
            actSoC = sc.cell.getSoc()
            if actSoC < minSoC:
                minSoC = actSoC
        return minSoC
    
    def getCoulomb(self):
        coulombSum = 0
        for sc in self.smartCells:
            coulombSum += sc.cell.getCoulomb()
        return coulombSum
    
    def getEnergy(self):            
        energySum = sum(self.arch.collectEnergies())
        return energySum
    
    def getVoltage(self):
        voltageSum = 0
        for sc in self.smartCells:
            voltageSum += sc.cell.getVoltage()
            
        voltageSum = sum(self.arch.collectVoltages())
        return voltageSum
            
    def printStats(self):
        for item in self.smartCells:
            item.printStats()
            
    def getLosses(self):
        lossesSum = {'swL':0,'tfL':0}
        for sc in self.smartCells:
            lossesSum['swL'] += sc.cell.getLosses().swL
            lossesSum['tfL'] += sc.cell.getLosses().tfL
        return lossesSum

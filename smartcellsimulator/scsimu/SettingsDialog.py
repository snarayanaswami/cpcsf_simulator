"""
=====================================================
Collection of GUI dialogs for settings and parameters
=====================================================

**file** : SettingsDialog.py

**date** : 3-July-2015
"""

from PyQt5 import QtGui

class CurveFollowSettingsDialog(QtGui.QDialog):
    """
    QDialog for changing settings for curve following.
    
    Returns:
        curveFollowSettings (dictionary): Settings for following a load-curve and boolean value whether dialog was confirmed or cancelled
    
    Notes:
        Return-dictionary elements:
        
        - *currentDivider* : Used for scaling the values in the '.csv' file to the capacity of the pack
        
        - *cycleRepetitions* : How often is the cycle repeated
    """
    def __init__(self, parent=None, curveFollowSettings=None):
        """
        Args:
            parent : Parent-window
            curveFollowSettings : Settings to initialize the dialog's UI elements with.
        """
        super(CurveFollowSettingsDialog, self).__init__(parent)
        
        if curveFollowSettings is not None:
            self.curveFollowSettings = curveFollowSettings
        else:
            self.curveFollowSettings = {"currentDivider":12, "cycleRepetitions":1}
        
        self.setWindowTitle("Curve follow settings")
        layout = QtGui.QVBoxLayout(self)

        self.lblCurrendDivider = QtGui.QLabel("Current divider:")
        layout.addWidget(self.lblCurrendDivider)
        self.spbCurrentDivider = QtGui.QDoubleSpinBox()
        self.spbCurrentDivider.setMaximum(1000)
        self.spbCurrentDivider.setValue(self.curveFollowSettings["currentDivider"])
        self.spbCurrentDivider.setMinimum(0.01)
        self.spbCurrentDivider.setSingleStep(1)
        layout.addWidget(self.spbCurrentDivider)

        self.lblCycleRepetitions = QtGui.QLabel("Cycle repetitions:")
        layout.addWidget(self.lblCycleRepetitions)
        self.spbCycleRepetitions = QtGui.QSpinBox()
        self.spbCycleRepetitions.setMaximum(1000)
        self.spbCycleRepetitions.setValue(self.curveFollowSettings["cycleRepetitions"])
        self.spbCycleRepetitions.setMinimum(1)
        self.spbCycleRepetitions.setSingleStep(1)
        layout.addWidget(self.spbCycleRepetitions)     
        
        # OK and Cancel buttons
        buttons = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel, 0x1, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout.addWidget(buttons)

    @staticmethod
    def getCurveFollowSettings(parent=None, curveFollowSettings=None):
        dialog = CurveFollowSettingsDialog(parent, curveFollowSettings)
        result = dialog.exec_()
        return {"currentDivider":dialog.spbCurrentDivider.value(), "cycleRepetitions":dialog.spbCycleRepetitions.value(), "Accepted":result==QtGui.QDialog.Accepted}

class EvaluationSettingsDialog(QtGui.QDialog):
    """
    QDialog for changing settings for evaluating the hardware development platform.
    
    Returns:
        evaluationSettings (dictionary): Settings for evaluating the platform and boolean value whether dialog was confirmed or cancelled
    """
    def __init__(self, parent=None, evaluationSettings=None):
        """
        Args:
            parent : Parent-window
            evaluationSettings : Settings to initialize the dialog's UI elements with.
        """
        super(EvaluationSettingsDialog, self).__init__(parent)

        if evaluationSettings is not None:
            self.evaluationSettings = evaluationSettings
        else:
            self.evaluationSettings = {"NumberOfCells":5, "CellCapacity":1.1, "socCell0":0.90, "socCell1":0.10, "socCell2":0.90, "socCell3":0.10, "socCell4":0.90, "OCVlatency":5.0}

        self.setWindowTitle("Evaluation settings")
        layout = QtGui.QVBoxLayout(self)

        self.lblNumberOfCells = QtGui.QLabel("Number of cells:")
        layout.addWidget(self.lblNumberOfCells)
        self.spbNumberOfCells = QtGui.QSpinBox()
        self.spbNumberOfCells.setMaximum(1080)
        self.spbNumberOfCells.setValue(self.evaluationSettings["NumberOfCells"])
        self.spbNumberOfCells.setMinimum(1)
        self.spbNumberOfCells.setSingleStep(1)
        layout.addWidget(self.spbNumberOfCells)

        self.lblCellCapacity = QtGui.QLabel("Cell capacity:")
        layout.addWidget(self.lblCellCapacity)
        self.spbCellCapacity = QtGui.QDoubleSpinBox()
        self.spbCellCapacity.setMaximum(1000)
        self.spbCellCapacity.setValue(self.evaluationSettings["CellCapacity"])
        self.spbCellCapacity.setMinimum(0.01)
        self.spbCellCapacity.setSuffix(" Ah")
        layout.addWidget(self.spbCellCapacity)
        
        self.cellSpbs = []
        self.cellLbls = []
        for index in range(5):
            self.cellSpbs.append(QtGui.QDoubleSpinBox())
            self.cellLbls.append(QtGui.QLabel("SOC Cell[{}]:".format(index)))
            
        for index,cellSpb in enumerate(self.cellSpbs):

            cellSpb.setSingleStep(1)
            dictKey = 'socCell{}'.format(index)
            cellSpb.setMaximum(100)
            cellSpb.setValue(self.evaluationSettings[dictKey]*100)
            cellSpb.setMinimum(0)
            cellSpb.setSuffix(" %")
            layout.addWidget(self.cellLbls[index])
            layout.addWidget(cellSpb)
            
        self.lblOCVlatency = QtGui.QLabel("OCV waiting time:")
        layout.addWidget(self.lblOCVlatency)
        self.spbOCVlatency = QtGui.QDoubleSpinBox()
        self.spbOCVlatency.setMaximum(100)
        self.spbOCVlatency.setValue(self.evaluationSettings["OCVlatency"])
        self.spbOCVlatency.setMinimum(0)
        self.spbOCVlatency.setSuffix(" s")
        layout.addWidget(self.spbOCVlatency)

        # OK and Cancel buttons
        buttons = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel, 0x1, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout.addWidget(buttons)

    @staticmethod
    def getEvaluationSettings(parent=None, evaluationSettings=None):
        dialog = EvaluationSettingsDialog(parent, evaluationSettings)
        result = dialog.exec_()
        evaluationSettings = {"NumberOfCells":dialog.spbNumberOfCells.value(), "CellCapacity":dialog.spbCellCapacity.value()}
        for index,cellSpb in enumerate(dialog.cellSpbs):
            dictKey = 'socCell{}'.format(index)
            evaluationSettings[dictKey] = cellSpb.value() / 100
        evaluationSettings["OCVlatency"] = dialog.spbOCVlatency.value()
        evaluationSettings[ "Accepted"] = (result == QtGui.QDialog.Accepted)
        
        return evaluationSettings
    

class CondenseMetadataSettingsDialog(QtGui.QDialog):
    """
    QDialog for changing settings for condensing metadata from multiple simulations for comparison.
    
    Returns:
        condenseMetadataSettings (dictionary): Settings condensing metadata and boolean value whether dialog was confirmed or cancelled
    
    Notes:
        Return-dictionary elements:
        
        - *seedStart* : Randomseed to start the batch-processing
        
        - *seedEnd* : Randomseed at which the batch-simulation is stopped
        
        - *cellsStart* : Amount of cells in the pack at start batch-processing
        
        - *cellsEnd* : Amount of cells at which batch-processing is stopped
        
        - *strategies* : Strategies to simulate in batch-processing
    """
    def __init__(self, parent=None, condenseMetadataSettings=None):
        """
        Args:
            parent : Parent-window
            condenseMetadataSettings : Settings to initialize the dialog's UI elements with.
        """
        super(CondenseMetadataSettingsDialog, self).__init__(parent)
        
        strategies = ['Minimum', 'Maximum', 'MinMax', 'BelowAverage', 'Passive']
        
        if condenseMetadataSettings is not None:
            self.condenseMetadataSettings = condenseMetadataSettings
        else:
            self.folder = QtGui.QFileDialog.getExistingDirectory(None, 'Open a folder', '~/', QtGui.QFileDialog.ShowDirsOnly)
            self.condenseMetadataSettings = {"folder":self.folder, "seedStart":1000, "seedEnd":1019, "cellsStart":96, "cellsEnd":96, 'strategies':strategies}
        
        self.setWindowTitle("Condense Metadata")
        layout = QtGui.QVBoxLayout(self)

        self.lblFolder = QtGui.QLabel("Folder:")
        layout.addWidget(self.lblFolder)
        self.edtFolder = QtGui.QLineEdit()
        self.edtFolder.setText(self.condenseMetadataSettings['folder'])
        layout.addWidget(self.edtFolder)
        
        self.lblStrategies = QtGui.QLabel("Strategies:")
        layout.addWidget(self.lblStrategies)
        self.stratChbs = []
        for strategy in strategies:
            chb = QtGui.QCheckBox()
            chb.setText(strategy)
            self.stratChbs.append(chb)
            layout.addWidget(chb)    

        self.lblSeedStart = QtGui.QLabel("Seed start:")
        layout.addWidget(self.lblSeedStart)
        self.spbSeedStart = QtGui.QSpinBox()
        self.spbSeedStart.setMaximum(100000)
        self.spbSeedStart.setValue(self.condenseMetadataSettings["seedStart"])
        self.spbSeedStart.setMinimum(1)
        self.spbSeedStart.setSingleStep(1)
        layout.addWidget(self.spbSeedStart)

        self.lblSeedEnd = QtGui.QLabel("Seed end:")
        layout.addWidget(self.lblSeedEnd)
        self.spbSeedEnd = QtGui.QSpinBox()
        self.spbSeedEnd.setMaximum(100000)
        self.spbSeedEnd.setValue(self.condenseMetadataSettings["seedEnd"])
        self.spbSeedEnd.setMinimum(1)
        self.spbSeedEnd.setSingleStep(1)
        layout.addWidget(self.spbSeedEnd)

        self.lblCellsStart = QtGui.QLabel("Cells start:")
        layout.addWidget(self.lblCellsStart)
        self.spbCellsStart = QtGui.QSpinBox()
        self.spbCellsStart.setMaximum(1080)
        self.spbCellsStart.setValue(self.condenseMetadataSettings["cellsStart"])
        self.spbCellsStart.setMinimum(1)
        self.spbCellsStart.setSingleStep(12)
        layout.addWidget(self.spbCellsStart)

        self.lblCellsEnd = QtGui.QLabel("Cells end:")
        layout.addWidget(self.lblCellsEnd)
        self.spbCellsEnd = QtGui.QSpinBox()
        self.spbCellsEnd.setMaximum(1080)
        self.spbCellsEnd.setValue(self.condenseMetadataSettings["cellsEnd"])
        self.spbCellsEnd.setMinimum(1)
        self.spbCellsEnd.setSingleStep(12)
        layout.addWidget(self.spbCellsEnd)

        self.chbSaveDiodeData = QtGui.QCheckBox()
        self.chbSaveDiodeData.setText('Save diode data file')
        layout.addWidget(self.chbSaveDiodeData)
        
        # OK and Cancel buttons
        buttons = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel, 0x1, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout.addWidget(buttons)

    @staticmethod
    def getCondenseMetadataSettings(parent=None, curveFollowSettings=None):
        dialog = CondenseMetadataSettingsDialog(parent, curveFollowSettings)
        result = dialog.exec_()
        
        strategies = []
        for stratChb in dialog.stratChbs:
            if stratChb.isChecked():
                strategies.append(stratChb.text())
        
        condenseMetadataSettings = {"folder":dialog.edtFolder.text(), "seedStart":dialog.spbSeedStart.value(), "seedEnd":dialog.spbSeedEnd.value(), "cellsStart":dialog.spbCellsStart.value(), "cellsEnd":dialog.spbCellsEnd.value(), "saveDiodeData":dialog.chbSaveDiodeData.isChecked(), 'strategies':strategies}
        condenseMetadataSettings['Accepted'] = (result == QtGui.QDialog.Accepted)
        return condenseMetadataSettings
    
    
class SaveScatterplotSettingsDialog(QtGui.QDialog):
    """
    QDialog for changing parameter-settings for creating a scatterplot (x-axis, y-axis).
    
    Returns:
        curveFollowSettings (dictionary): Settings for creating a scatterplot and boolean value whether dialog was confirmed or cancelled
    
    Notes:
        Return-dictionary elements:
                
        - *xAxis* : Category for x-axis
        - *yAxis* : Category for y-axis
    """
    def __init__(self, parent=None, saveScatterplotSettings=None):
        """
        Args:
            parent : Parent-window
            saveScatterplotSettings : Settings to initialize the dialog's UI elements with.
        """
        super(SaveScatterplotSettingsDialog, self).__init__(parent)
        
        columns = ['Strategy','Seed','Cells','Time','CAN Bits sent','Average datarate','Start SOC','Final SOC','Start Charge','Final Charge','Calculated loss','Charge loss', 'Transfertime']
        
        if saveScatterplotSettings is not None:
            self.saveScatterplotSettings = saveScatterplotSettings
        else:
            self.folder = QtGui.QFileDialog.getExistingDirectory(None, 'Open a folder', '~/', QtGui.QFileDialog.ShowDirsOnly)
            self.saveScatterplotSettings = {"folder":self.folder, "xAxis":3, "yAxis":10}
        
        self.setWindowTitle("Condense Metadata")
        layout = QtGui.QVBoxLayout(self)

        self.lblFolder = QtGui.QLabel("Folder:")
        layout.addWidget(self.lblFolder)
        self.edtFolder = QtGui.QLineEdit()
        self.edtFolder.setText(self.saveScatterplotSettings['folder'])
        layout.addWidget(self.edtFolder)        

        self.lblXAxis = QtGui.QLabel("X-Axis:")
        layout.addWidget(self.lblXAxis)
        self.cmbXAxis = QtGui.QComboBox()
        self.cmbXAxis.addItems(columns)
        self.cmbXAxis.setCurrentIndex(self.saveScatterplotSettings['xAxis'])
        layout.addWidget(self.cmbXAxis)

        self.lblYAxis = QtGui.QLabel("Y-Axis:")
        layout.addWidget(self.lblYAxis)
        self.cmbYAxis = QtGui.QComboBox()
        self.cmbYAxis.addItems(columns)
        self.cmbYAxis.setCurrentIndex(self.saveScatterplotSettings['yAxis'])
        layout.addWidget(self.cmbYAxis)
        
        # OK and Cancel buttons
        buttons = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel, 0x1, self)
        buttons.accepted.connect(self.accept)
        buttons.rejected.connect(self.reject)
        layout.addWidget(buttons)

    @staticmethod
    def getSaveScatterplotSettings(parent=None, saveScatterplotSettings=None):
        dialog = SaveScatterplotSettingsDialog(parent, saveScatterplotSettings)
        result = dialog.exec_()
                
        saveScatterplotSettings = {"folder":dialog.edtFolder.text(), "xAxis":dialog.cmbXAxis.currentIndex(), "yAxis":dialog.cmbYAxis.currentIndex()}
        saveScatterplotSettings['Accepted'] = (result == QtGui.QDialog.Accepted)
        return saveScatterplotSettings
    
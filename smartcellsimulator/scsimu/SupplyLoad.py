"""
===================================================
Logic for charging and discharging the battery pack
===================================================

**file** : SupplyLoad.py

**date** : 3-July-2015
"""

class SupplyLoad(object):
    """
    
    """
    def __init__(self,globalData):
        """
        Args:
            globalData: Collective data to work on 
        """
        self.globalData = globalData
        self.env = globalData.env
        self.numberOfCells = globalData.numberOfCells
        self.canBus = globalData.canBus
        self.canBus.addMessageSubscriber(self.CanMessageHandler)
        self.mode = "idle"
        self.rate = 0
        self.duration = 0
        self.env.process(self.applyLoad())
        
       
    def CanMessageHandler(self,msg):
        """
        Args:
            msg: Incoming CAN-message
        """
        if msg.identifier == self.canBus.CAN_SUPPLY_LOAD_MODE:
            self.mode = msg.data["SupplyLoadMode"]
            if "rate" in msg.data:
                self.rate = msg.data["rate"]
            if "duration" in msg.data:
                self.duration = msg.data["duration"]
                
        yield self.env.exit()
   
                
    def applyLoad(self):
        """
        Apply a current to the pack and charge or discharge all cells simultaneously.
        """
        charging_duration = self.globalData.minTimeStep
        while True:
            cellCharge = []
            if self.mode == "applyLoad":
                for cell in self.globalData.pack.smartCells:
                    cellCharge.append(self.globalData.env.process(cell.cmu.charge(self.duration, self.rate)))
                    
            yield self.globalData.env.timeout(charging_duration)
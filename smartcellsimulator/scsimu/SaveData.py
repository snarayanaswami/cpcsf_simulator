"""
=====================================================================
Contains methods for exporting recorded simulation data to .csv-files
=====================================================================

**file** : SaveData.py

**date** : 3-July-2015
"""

import csv
from math import ceil
import os.path

class SaveData():
    """
    
    """
    def __init__(self):
        pass
       
    def saveData(self, file, globalData, CANdata=False, reduceData=False, reduceAmount=15000):
        """
        Split filename, check for saving options and delegate to separate saving methods.
        
        Args:
            file (str) : Path to filename to save data under
            globalData: globalData containing data from the simulation
            CANdata (bool) : Option whether to save detailed data of CAN-bus
            reduceData (bool) : Option whether to save reduced data files
            reduceAmount (int) : Amount of maximum values allowed in single reduced data file
        """
        if (file):
            file = os.path.splitext(file)
            if(globalData.rangeStart!=globalData.rangeEnd):
                self.saveSOCData(file[0] + '_SOC_'+str(globalData.rangeValue) + file[1], reduceData, reduceAmount, globalData)
                if CANdata:
                    self.saveCANData(file[0] + '_CAN_'+str(globalData.rangeValue) + file[1], reduceData, reduceAmount, globalData)
                self.saveMetaData(file[0] + '_META_'+str(globalData.rangeValue) + file[1], globalData)
                self.saveArchitectureData(file[0] + '_ARCHDATA_'+str(globalData.rangeValue) + file[1], globalData)
            else:
                self.saveSOCData(file[0] + '_SOC' + file[1], reduceData, reduceAmount, globalData)
                if CANdata:
                    self.saveCANData(file[0] + '_CAN' + file[1], reduceData, reduceAmount, globalData)
                self.saveMetaData(file[0] + '_META' + file[1], globalData)
                self.saveArchitectureData(file[0] + '_ARCHDATA' + file[1], globalData)
            
            
    def saveCANData(self, file, reduceData, reduceAmount, globalData):
        """
        Save detailed information about CAN-bus (Bits sent over time, bus speed, average speed).
        
        Args:
            file (str) : Path to filename to save data under
            reduceData (bool) : Option whether to save reduced data files
            reduceAmount (int) : Amount of maximum values allowed in single reduced data file
            globalData: globalData containing data from the simulation
        """
        timeAxis = globalData.monitor.canDataPlot.datax 
        CANMsg = globalData.monitor.canDataPlot.datay
        CANFreq = globalData.monitor.canSpeedPlot.datay
        
        with open(file, 'w', newline='') as csvfile:
            CSVWriteOut = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            firstRow = ['Time','Data send [kByte]','Speed [kBit/s]','Average speed [kBit/s]']
            # Write first row to csv
            CSVWriteOut.writerow(firstRow)
            
            # For every row in time Axis
            for i,t in enumerate(timeAxis):
                # Start with i'th time value
                currentRow = [t]
                # Append Messages amount
                currentRow.append(CANMsg[0][i])
                # Append Messages frequency
                currentRow.append(CANFreq[0][i])
                # Append Messages average frequency
                currentRow.append(CANFreq[1][i])
                
                # Write out row to csv
                CSVWriteOut.writerow(currentRow)
                
        if (reduceData):
            self.downSampleCSV(file, reduceAmount, len(timeAxis)+1, len(firstRow))
            
            
    def saveSOCData(self, file, reduceData, reduceAmount, globalData):
        """
        Save SoC-history data of every cell.
        
        Args:
            file (str) : Path to filename to save data under
            reduceData (bool) : Option whether to save reduced data files
            reduceAmount (int) : Amount of maximum values allowed in single reduced data file
            globalData: globalData containing data from the simulation
        """
        timeAxis = globalData.monitor.socPlot.datax
        SOCdata = globalData.monitor.socPlot.datay
        
        with open(file, 'w', newline='') as csvfile:
            CSVWriteOut = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            firstRow = ['Time']
            # Append titles for each cell in SOCdata
            for i in range(len(SOCdata)):
                firstRow.append('Cell{}'.format(i))
            # Append title for Pack SOC
            firstRow.append('Pack')
            # Write first row to csv
            CSVWriteOut.writerow(firstRow)
            
            # For every row in time Axis
            for i,t in enumerate(timeAxis):
                # Start with i'th time value
                currentRow = [t]
                # For every cell in SOCdata append i'th value
                for cellSOC in SOCdata:
                    currentRow.append(cellSOC[i])
                # Append i'th Pack SOC value 
                currentRow.append(min(currentRow[1:]))
                
                # Write out row to csv
                CSVWriteOut.writerow(currentRow)
                
        if (reduceData):
            self.downSampleCSV(file, reduceAmount, len(timeAxis)+1, len(firstRow))
            
      
    def downSampleCSV(self, file, maxsamples, rows, columns):
        """
        Reduce the amount of data points in a *.csv file and save as '*_ds.csv' file.
        
        Args:
            file (str) : Path to filename to save data under
            maxsamples (int) : Amount of maximum values allowed in single reduced data file
            rows (int) : Number of rows in the file
            columns (int) : Number of columns in the file 
        """
        divider = ceil(rows * columns / maxsamples) 
        with open(file, newline='') as infile:
            file = os.path.splitext(file)
            with open(file[0] + '_ds' + file[1], 'w', newline='') as outfile:
                CSVReadIn = csv.reader(infile, delimiter=',', quotechar='|')
                CSVWriteOut = csv.writer(outfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for i,row in enumerate(CSVReadIn):
                    if (i%divider==0) or (i==1):
                        CSVWriteOut.writerow(row)
                        
                        
    def saveMetaData(self, file, globalData):
        """
        Save meta data of a simulation.
        
        Args:
            file (str) : Path to filename to save data under
            globalData: globalData containing data from the simulation
        """
        with open(file, "w", newline='') as metadata:
            CSVMetaData = csv.writer(metadata, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            CSVMetaData.writerow(["Key", "Value"])
            CSVMetaData.writerow(["Balancing Strategy", globalData.OptionStrategy['Name']])
            CSVMetaData.writerow(["Number of cells", globalData.numberOfCells])
            CSVMetaData.writerow(["Random Seed", globalData.OptionRandomSeed])
            CSVMetaData.writerow(["Timebased SOC broadcast", globalData.OptionTimebasedBC])
            CSVMetaData.writerow(["Timebased Min broadcast", globalData.OptionMinBC])
            CSVMetaData.writerow(["Timebased Max broadcast", globalData.OptionMaxBC])
            CSVMetaData.writerow(["Broadcast period", globalData.OptionBCperiod])
            CSVMetaData.writerow(["Limit timebased SOC broadcast", globalData.OptionLimitTimebasedBC])
            CSVMetaData.writerow(["Limit bound", globalData.OptionLimitBound])
            CSVMetaData.writerow(["Concurrent balancing", globalData.OptionConcurrent])
            CSVMetaData.writerow(["Concurrency degree", globalData.OptionConcurrencyDeg])
            CSVMetaData.writerow(["Broadcast after transaction", globalData.OptionBCAfterTransaction])
            CSVMetaData.writerow(["Simulation time", globalData.env.now])
            CSVMetaData.writerow(["Total Bits sent", globalData.canBus.dataCount])
            CSVMetaData.writerow(["Average datarate", globalData.canBus.dataCount/globalData.env.now])
            CSVMetaData.writerow(["Start Pack SoC", globalData.startPackSOC*100])
            CSVMetaData.writerow(["Final Pack SoC", globalData.finalPackSOC*100])
            CSVMetaData.writerow(["Start Pack stored Energy", globalData.startPackEnergy/3600])
            CSVMetaData.writerow(["Final Pack stored Energy", globalData.finalPackEnergy/3600])
            CSVMetaData.writerow(["Calculated Losses", (globalData.startPackEnergy/3600-globalData.finalPackEnergy/3600)])
            CSVMetaData.writerow(["Accumulated Losses", (globalData.accumulatedLosses['swL']+globalData.accumulatedLosses['tfL'])/3600])
            CSVMetaData.writerow(["Transfertime", globalData.tBalTotal])
    
    
    def saveArchitectureData(self, file, globalData):
        """
        Save architecture data of a simulation.
        
        Args:
            file (str) : Path to filename to save data under
            globalData: globalData containing data from the simulation
        """
        with open(file, "w", newline='') as metadata:
            ArchitectureData = csv.writer(metadata, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            
            # Write title row
            row = ['key']
            for i in range(len(globalData.pack.smartCells)):
                row.append('Cell{}'.format(i))
            ArchitectureData.writerow(row)
            
            # Write architecture parameters
            archParam = globalData.pack.smartCells[0].cell.cell.archParam
            archParamDict = archParam.__dict__
            for key, val in archParamDict.items():
                row = [key]
                for smartCell in globalData.pack.smartCells:
                    paramDict = smartCell.cell.cell.archParam.__dict__
                    row.append(paramDict[key])
                
                ArchitectureData.writerow(row)
                    
            # Write diode error values
            row = ['q1']
            for smartCell in globalData.pack.smartCells:
                if hasattr(smartCell.cell.cell, 'errorDiode'):
                    if 'i1' in smartCell.cell.cell.errorDiode:
                        row.append(smartCell.cell.cell.errorDiode['q1'])
                else:
                    row.append('-')
            ArchitectureData.writerow(row)
            
            row = ['l1']
            for smartCell in globalData.pack.smartCells:
                if hasattr(smartCell.cell.cell, 'errorDiode'):
                    if 'i1' in smartCell.cell.cell.errorDiode:
                        row.append(smartCell.cell.cell.errorDiode['i1'])
                else:
                    row.append('-')
            ArchitectureData.writerow(row)
            
            # Write cell parameters
            dynParam = globalData.pack.smartCells[0].cell.cell.param
            dynParamDict = dynParam.__dict__
            for key, val in dynParamDict.items():
                row = [key]
                for smartCell in globalData.pack.smartCells:
                    paramDict = smartCell.cell.cell.param.__dict__
                    row.append(paramDict[key])
            
                ArchitectureData.writerow(row)
                
    #         for key in globalData.pack.smartCells[0].cell.arch.param._fields:
    #             row = [key]
    #             for cell in globalData.pack.smartCells:
    #                 row.append(getattr(cell.cell.arch.param,key))
    #             ArchitectureData.writerow(row)
    
    
    def condenseMetaData(self, seedStart, seedEnd, cellsStart, cellsEnd, folder, strategies=None, saveDiodeData=False):
        """
        Takes meta data files of simulations and rearranges the values to compare the simulations between each other.
        
        Args:
            seedStart : Randomseed the batch-processing was started with
            seedEnd : Randomseed at which the batch-simulation was stopped
            cellsStart : Amount of cells in the pack at of start batch-processing
            cellsEnd : Amount of cells at which batch-processing was stopped
            folder : Foldername containing the simulation data
            strategies : Strategies used in batch-processing
            saveDiodeData : Option whether to save data about energy lost over diode
        """
        if strategies is None:
            strategies = ['Minimum', 'Maximum', 'MinMax', 'BelowAverage', 'Passive']
        for currentCells in range(ceil(cellsStart/12), ceil(cellsEnd/12)+1):
            subfolder = folder + '/{}_CELLS'.format(currentCells*12)        
            AllData = []
            for CurrentSeed in range(seedStart, seedEnd+1):
                SeedData = []    
                for currentStrategy in strategies:
                    file = subfolder + '/{}_S{}_META.csv'.format(currentStrategy, CurrentSeed)
                    column = []
                    with open(file, newline='') as infile:
                        CSVReadIn = csv.reader(infile, delimiter=',', quotechar='|')
                        for row in CSVReadIn:
                            column.append(row[1])
                    columnCondensed = ([column[1], column[3], column[2], column[13], column[14], column[15], column[16], column[17], column[18], column[19], column[20], column[21]])
                    SeedData.append(columnCondensed)
                AllData.append(SeedData)
                
            with open(subfolder + '/_condensed_META.csv', 'w', newline='') as outfile:
                CSVWriteOut = csv.writer(outfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for strategyIndex in range(len(strategies)):
                    for Row in range(0,12):
                        writeRows = []
                        for CurrentSeed in range(0, seedEnd-seedStart+1):
                                writeRows.append(AllData[CurrentSeed][strategyIndex][Row])
                        CSVWriteOut.writerow(writeRows)
                    CSVWriteOut.writerow([])
                    CSVWriteOut.writerow([])
                    
            if saveDiodeData:
                print('Saving diodeData!')
                self.saveDiodeData(seedStart, seedEnd, cellsStart, cellsEnd, folder, strategies)
               
                    
    def savePackSocFileDS(self, seedStart, seedEnd, cellsStart, cellsEnd, folder):
        """
        Scans through a folder of given simulation criteria and creates a downsampled file for comparing the pack SoC value of different balancing scenarios.
        
        Args:
            seedStart : Randomseed the batch-processing was started with
            seedEnd : Randomseed at which the batch-simulation was stopped
            cellsStart : Amount of cells in the pack at of start batch-processing
            cellsEnd : Amount of cells at which batch-processing was stopped
            folder : Foldername containing the simulation data
        """
        Strategies = ['Minimum', 'Maximum', 'MinMax', 'BelowAverage', 'Passive']
        for currentCells in range(ceil(cellsStart/12), ceil(cellsEnd/12)+1):
            subfolder = folder + '/{}_CELLS'.format(currentCells*12)        
            AllData = []
            for CurrentSeed in range(seedStart, seedEnd+1):
                SeedData = []    
                for CurrentStrategy in range (0, 4):
                    file = subfolder + '/{}_S{}_SOC_ds.csv'.format(Strategies[CurrentStrategy], CurrentSeed)
                    packSOC = []
                    with open(file, newline='') as infile:
                        CSVReadIn = csv.reader(infile, delimiter=',', quotechar='|')
                        packSOC.append('S_{}'.format(CurrentSeed))
                        packSOC.append(Strategies[CurrentStrategy])
                        for row in CSVReadIn:
                            packSOC.append(row[currentCells*12+1])
                    SeedData.append(packSOC)
                AllData.append(SeedData)
                
            with open(subfolder + '/_packSOC_ds.csv', 'w', newline='') as outfile:
                CSVWriteOut = csv.writer(outfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for CurrentStrategy in range(0, 4):
                    
                    
                    
                    for Row in range(0, len(AllData[0][CurrentStrategy])):
                        writeRows = []
                        for CurrentSeed in range(0, seedEnd-seedStart+1):
                            try:
                                writeRows.append(AllData[CurrentSeed][CurrentStrategy][Row])
                            except:
                                writeRows.append(None)
                        CSVWriteOut.writerow(writeRows)
                    CSVWriteOut.writerow([])
                    
                    
    def savePackSocFile(self, seedStart, seedEnd, cellsStart, cellsEnd, folder):
        """
        
        
        Args:
            seedStart : Randomseed the batch-processing was started with
            seedEnd : Randomseed at which the batch-simulation was stopped
            cellsStart : Amount of cells in the pack at of start batch-processing
            cellsEnd : Amount of cells at which batch-processing was stopped
            folder : Foldername containing the simulation data
        """
        Strategies = ['Minimum', 'Maximum', 'MinMax', 'BelowAverage', 'Passive']
        for currentCells in range(ceil(cellsStart/12), ceil(cellsEnd/12)+1):
            subfolder = folder + '/{}_CELLS'.format(currentCells*12)        
            AllData = []
            for CurrentSeed in range(seedStart, seedEnd+1):
                SeedData = []    
                for CurrentStrategy in range (0, 4):
                    file = subfolder + '/{}_S{}_SOC.csv'.format(Strategies[CurrentStrategy], CurrentSeed)
                    packSOC = []
                    with open(file, newline='') as infile:
                        CSVReadIn = csv.reader(infile, delimiter=',', quotechar='|')
                        packSOC.append('S_{}'.format(CurrentSeed))
                        packSOC.append(Strategies[CurrentStrategy])
                        for row in CSVReadIn:
                            packSOC.append(row[currentCells*12+1])
                    SeedData.append(packSOC)
                AllData.append(SeedData)
                
            with open(subfolder + '/_packSOC.csv', 'w', newline='') as outfile:
                CSVWriteOut = csv.writer(outfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for CurrentStrategy in range(0, 4):
                    for Row in range(0,len(AllData[0][CurrentStrategy])):
                        writeRows = []
                        for CurrentSeed in range(0, seedEnd-seedStart+1):
                            try:
                                writeRows.append(AllData[CurrentSeed][CurrentStrategy][Row])
                            except:
                                writeRows.append(None)
                        CSVWriteOut.writerow(writeRows)
                    CSVWriteOut.writerow([])
    
    def saveScatterPlotFile(self, folder, xRow, yRow):
        """
        Savea a *.csv file containing data for the x- and y-axis of a scatterplot.
        
        Args:
            folder : Foldername containing the simulation data
            xRow : Number of the row in the condensed meta file to use for x-axis data
            yRow : Number of the row in the condensed meta file to use for y-axis data
        """
        columns = ['Strategy','Seed','Cells','Time','CAN Bits sent','Average datarate','Start SOC','Final SOC','Start Charge','Final Charge','Calculated loss','Charge loss']
        condensedMetadata = folder + '/_condensed_META.csv'
        rowsPerStrategy = len(columns)+2
        xColumn = []
        yColumn = []
        lColumn = []
        with open(condensedMetadata, newline='') as infile:
            CSVReadIn = csv.reader(infile, delimiter=',', quotechar='|')
            for i,row in enumerate(CSVReadIn):
                if (i%rowsPerStrategy) == 0:
                    for labelItem in row:
                        lColumn.append(labelItem)
                        pass
                if (i%rowsPerStrategy) == xRow:
                    for xItem in row:
                        xColumn.append(xItem)
                        pass
                if (i%rowsPerStrategy) == yRow:
                    for yItem in row:
                        yColumn.append(yItem)
                        pass
                    
        with open(folder + '/_scatterPlot({},{}).csv'.format(columns[xRow], columns[yRow]), 'w', newline='') as outfile:
            CSVWriteOut = csv.writer(outfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            CSVWriteOut.writerow(['x','y','label'])
            for index, label in enumerate(lColumn):
                CSVWriteOut.writerow([xColumn[index], yColumn[index], label])
                
    def saveDiodeData(self, seedStart, seedEnd, cellsStart, cellsEnd, folder, strategies=None):
        """
        Args:
            seedStart : Randomseed the batch-processing was started with
            seedEnd : Randomseed at which the batch-simulation was stopped
            cellsStart : Amount of cells in the pack at of start batch-processing
            cellsEnd : Amount of cells at which batch-processing was stopped
            folder : Foldername containing the simulation data
            strategies : List of strategies to scan for
        """
        if strategies is None:
            strategies = ['Minimum', 'Maximum', 'MinMax', 'BelowAverage', 'Passive']
        for currentCells in range(ceil(cellsStart/12), ceil(cellsEnd/12)+1):
            subfolder = folder + '/{}_CELLS'.format(currentCells*12)        
            AllData = []
            for CurrentSeed in range(seedStart, seedEnd+1):
                SeedData = []    
                for currentStrategy in strategies:
                    file = subfolder + '/{}_S{}_ARCHDATA.csv'.format(currentStrategy, CurrentSeed)
                    column = []
                    with open(file, newline='') as infile:
                        CSVReadIn = csv.reader(infile, delimiter=',', quotechar='|')
                        for row in CSVReadIn:
                            column.append(row[int(currentCells/2)])
                    columnCondensed = ([currentStrategy, column[11], column[12]])
                    SeedData.append(columnCondensed)
                AllData.append(SeedData)

            with open(subfolder + '/_diodeData.csv', 'w', newline='') as outfile:
                CSVWriteOut = csv.writer(outfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for strategyIndex in range(len(strategies)):
                    for Row in range(len(columnCondensed)):
                        writeRows = []
                        for CurrentSeed in range(0, seedEnd-seedStart+1):
                                writeRows.append(AllData[CurrentSeed][strategyIndex][Row])
                        CSVWriteOut.writerow(writeRows)
                    CSVWriteOut.writerow([])
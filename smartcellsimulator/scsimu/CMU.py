"""
=============================================================
Cell management unit observing and controlling the Smart Cell
=============================================================

**file** : CMU.py

**date** : 3-July-2015
"""

import random
from .CANmessage import CANmessage
from .ActiveBalancing import *
from simpy.util import start_delayed

class CMU(object):
    """
    Class representing the controlling logic layer of a smart cell.
    
    """
    def __init__(self, globalData, objectId, cell):
        self.globalData = globalData
        self.env = globalData.env
        self.objectId = objectId
        self.canBus = globalData.canBus
                
        self.cell = cell
        self.lastSoC = 10000
        
        self.lastTransaction = 0
        
        self.socList = [0 for _ in range(self.globalData.numberOfCells)]
        self.modeList = ["idle" for _ in range(self.globalData.numberOfCells)]
        self.adaptrange = 1
        
        """adding submodule activeBalancing, registering messageSubscriber"""
        self.activeBalancing = ActiveBalancing(self.globalData, self)
        self.canBus.addMessageSubscriber(self.activeBalancing.ACBmessageHandler)
        self.canBus.addMessageSubscriber(self.updateSocList)
#         self.canBus.addMessageSubscriber(self.transferMonitor)
        
        #self.env.process(self.broadCastMessage())
        start_delayed(self.env, self.broadCastMessage(), delay=random.random())
#         start_delayed(self.env, self.packMonitor(), delay=random.random())
                  
    def updateSocList(self, msg):
        """
        Update the array of state of charge values when a SoC-message arrives.
        
        Args:
            msg : Incoming CAN-message
        """
        if (msg.identifier == self.canBus.CAN_SOC_BROADCAST):
            self.socList[msg.data['ORIGIN']] = msg.data['soc']
            
            yield self.env.exit()
    
    def sendBroadcastCondition(self):
        """
        Check whether the set conditions are true for sending a SoC broadcast.
        
        Options can be:
            - Timebased broadcast
                - Send periodic broadcasts every given period
            - Minimum in pack broadcast
                - The lowest SoC cell in the pack broadcasts its timebased SoC
            - Maximum in pack broadcast
                - The largest SoC cell in the pack broadcasts its timebased SoC
            - Limit timebased broadcast
                - Only send out a broadcast if the change in SoC is larger than the given threshold
        """
        result = False
        bound = ( abs(self.lastSoC-self.cellSOC()) > self.globalData.OptionLimitBound )
        
        # timedBroadcast for all cells is enabled
        if (self.globalData.OptionTimebasedBC):
            # limit timebased broadcast enabled and cell soc change is larger than limitBound
            if (self.globalData.OptionLimitTimebasedBC):
                if ( bound ):
                    result = True
            else:
                result = True
        # timedBroadcast for minimum cell is enabled and cell is minimum in pack
        if (self.globalData.OptionMinBC) and ( (self.socList[self.objectId] <= sorted(self.socList)[0]) or ( (self.globalData.OptionConcurrent) and (self.socList[self.objectId] <= sorted(self.socList)[self.globalData.OptionConcurrencyDeg-1]) ) ):
            # limit timebased broadcast enabled and cell soc change is larger than limitBound
            if (self.globalData.OptionLimitTimebasedBC):
                if ( bound ):
                    result = True
            else:
                result = True
        # timedBroadcast for maximum cell is enabled and cell is maximum in pack
        if (self.globalData.OptionMaxBC) and ( (self.socList[self.objectId] >= sorted(self.socList)[-1]) or ( (self.globalData.OptionConcurrent) and (self.socList[self.objectId] >= sorted(self.socList)[-self.globalData.OptionConcurrencyDeg]) ) ):
            # limit timebased broadcast enabled and cell soc change is larger than limitBound
            if (self.globalData.OptionLimitTimebasedBC):
                if ( bound ):
                    result = True
            else:
                result = True
        
        if ( self.globalData.OptionEvaluateDemonstrator):
            if ( (self.globalData.env.now - self.lastTransaction) < self.globalData.evaluationSettings['OCVlatency'] ):
                result = False
            else:
                result = result
        
        return result
                      
    def broadCastMessage(self):
        """
        Send a broadcast message to the CAN bus, containing the cell's ID and state of charge. 
        """
        while True:
            if ( self.sendBroadcastCondition() ):
                #message = CANmessage(self.canBus.CAN_SOC_BROADCAST, {'TARGET': 'BROADCAST', 'ORIGIN': self.objectId, 'soc':self.cellSOC()})
                message = CANmessage(self.canBus.CAN_SOC_BROADCAST,{'TARGET': 'BROADCAST', 'ORIGIN': self.objectId, 'soc': self.cellSOC()})
                yield self.globalData.env.process(self.canBus.CANsend(message))   
             
            self.lastSoC = self.cellSOC()
            yield self.env.timeout(self.globalData.OptionBCperiod)
    
    def transferMonitor(self, msg):
        if (msg.identifier == self.canBus.CAN_SEND_ACKNOWLEDGE):
            
            duration = msg.data['transferTime']
            rate = msg.data['transferRate']
            
            RtxRrx = self.arch.getResistance(idxRecv=0, idxSend=1)
            if(rate > 0):
                Rcircuit = RtxRrx.Rrx
            else:
                Rcircuit = RtxRrx.Rtx

        yield self.env.exit()
        
    def cellSOC(self):
        return self.cell.getSoc()
    
    def charge(self,duration,rate):
        if self.cell.applyLoadCurrent( tSec=duration, iAmps=rate ) < duration:
            """notify the charger that charging shall be stopped via a CAN message"""  
            msg = CANmessage(self.canBus.CAN_SUPPLY_LOAD_MODE, {'TARGET': 'BROADCAST', 'ORIGIN': self.objectId, 'SupplyLoadMode':'idle'})
            msg = CANmessage(self.globalData.canBus.CAN_BALANCE_CONTROL, {'activeBalancingControl':False})
            yield self.globalData.env.process(self.canBus.CANsend(msg)) 
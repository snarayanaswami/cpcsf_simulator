"""
======================================
Interface to the physical battery cell
======================================

**file** : Cell.py

**date** : 3-July-2015

"""

import math
import random

class Cell(object):
    """
    This class represents a battery cell and its interfaces to physically interact with supply, load and active balancing actions.
    
    """
    
    def __init__(self, globalData,objectId,arch):
        self.env = globalData.env
        self.objectId=objectId
        self.lastTime=self.env.now 
        self.globalData=globalData
        self.arch = arch
        
        self.cell = arch.cells[objectId]
        
        self.getLoadTimeEstTillTarget = self.cell.getLoadTimeEstTillTarget
        self.applyLoadCurrent = self.cell.applyLoadCurrent
        
    def getVoltage(self):
        returnvoltage = self.cell.getVoltage()
        return (returnvoltage)
        
    def charge(self,duration,rate):
        #TODO: Need to get indices here!
        RtxRrx = self.arch.getResistance(idxRecv=0, idxSend=1)
        if(rate > 0):
            Rcircuit = RtxRrx.Rrx
        else:
            Rcircuit = RtxRrx.Rtx
            
        tReal = self.cell.applyBalancingCurrent(Rcircuit=Rcircuit, iPeakAmps=rate, tSec=duration)

        assert tReal == duration
        
    def discharge(self,duration,rate):
        self.charge(duration, -rate)
        
    def setSoc(self,soc):
        self.cell.setSoc(soc)  
      
    def getSoc(self):
        return self.cell.getSoc()
      
    def getCoulomb(self):
        return self.cell.getCharge()
    
    def getEnergy(self):
        return self.cell.getEnergy()

    def getLosses(self):
        return self.cell.getLosses()
"""
===========================================
Descriptions of active balancing strategies
===========================================

**file** : BalancingStrategies.py

**date** : 20-July-2015
"""

from statistics import mean, variance
import random
from .CANmessage import CANmessage

class BalancingStrategies():
    """
    Class implementing the active balancing strategy interface for determining the request and acknowledge behavior of smart cells.
    
    
    Notes:
        **Request strategy** *(Request charge condition)*
        - Determines whether a cell is in need for receiving charge and will send out a request
        
        **Acknowledge Strategy** *(Send charge condition)*
        - Determines whether  a cell is capable of giving charge away to other cells and thus accept a request
    """
    def __init__(self, activeBalancing, globalData, CMU):
            self.globalData = globalData
            self.CMU = CMU
            self.activeBalancing = activeBalancing
        
    
    # From here on comes the transfer strategy
    
    def ReqStr_BelowAverage(self):
        """
        The cell requests charge as soon as it is below the average SoC value of the pack.
           
        """
        
        while True:
            requestPause = 1 + random.random()
            yield self.CMU.env.timeout(requestPause)
            
            if ( self.globalData.OptionEvaluateDemonstrator ):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if self.activeBalancing.enabled:
                if (self.activeBalancing.balancingMode == "idle") and (self.CMU.cellSOC() < mean(self.CMU.socList)):
                                       
                    if self.activeBalancing.moreChargeOnLeft():
                        sender = self.CMU.objectId-1
                    else:
                        sender = self.CMU.objectId+1
                   
                    message = CANmessage(self.CMU.canBus.CAN_SEND_REQUEST, {'TARGET': sender, 'ORIGIN': self.CMU.objectId})
                    yield self.globalData.env.process(self.CMU.canBus.CANsend(message))
            
    
    
    #Strategy how requests are acknowledged
    # acknowledge to requester with lowest SoC          
    def AckStr_BelowAverage(self):
        """
        A cell acknowledges a request for charge if its SoC is greater than the pack average.
        """
        
        while True:
            repeatTime = 1+random.random()
            yield self.CMU.env.timeout(repeatTime)
            
            if ( self.globalData.OptionEvaluateDemonstrator ):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if (len(self.activeBalancing.chargeRequesterList) > 0) and (self.activeBalancing.balancingMode == "idle") and (self.CMU.cellSOC() > mean(self.CMU.socList)):
                #print("start sending at time " + str(self.globalData.env.now))  
                transferTime = self.globalData.transferTime
                transferRate = self.globalData.transferRate
                
                receiver = self.CMU.objectId + 1 - 2 * int(self.activeBalancing.lessChargeOnLeft()) #if less charge on left -> receiver id = self id - 1
                
                #find out who deserves the charge most from chargeRequesterList
                if receiver not in self.activeBalancing.chargeRequesterList:
                    receiver = None
                      
                #if we found someone
                if(receiver is not None):
                    retvalue = yield self.globalData.env.process(self.activeBalancing.blockNeighbors(self.CMU.objectId,receiver))
                    if retvalue:
                        #print("sending from "+str(self.CMU.objectId)+" to "+str(receiver)+" at time "+str(self.globalData.env.now))
                        message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE, {'TARGET': receiver, 'ORIGIN': self.CMU.objectId,'transferTime':transferTime,'transferRate':transferRate})
                        self.globalData.env.process(self.CMU.canBus.CANsend(message)) 
                        yield self.globalData.env.process(self.activeBalancing.sendCharge(transferTime,transferRate,self.CMU.objectId,receiver))
            self.activeBalancing.chargeRequesterList = []  

    def ReqStr_Minimum(self):
        """
        The cell requests charge if it is the n-th minimum of the pack.
        """
        
        while True:
            requestPause = 1+random.random()
            if (self.CMU.socList[self.CMU.objectId] is sorted(self.CMU.socList)[0]):
                requestPause = 0.1
            yield self.CMU.env.timeout(requestPause)
            
            if (self.globalData.OptionEvaluateDemonstrator):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if self.activeBalancing.enabled:
                #print ('[#{}]: {}, min = {}'.format(self.CMU.objectId, self.CMU.cellSOC(), min(self.CMU.socList)))
                if (self.activeBalancing.balancingMode == "idle"):                    
                    if ( (self.CMU.socList[self.CMU.objectId] <= sorted(self.CMU.socList)[0]) or 
                         ( (self.globalData.OptionConcurrent) and (self.CMU.socList[self.CMU.objectId] <= sorted(self.CMU.socList)[self.globalData.OptionConcurrencyDeg-1]) ) ):
                         
                        if self.activeBalancing.moreChargeOnLeft():
                            sender = self.CMU.objectId-1
                        else:
                            sender = self.CMU.objectId+1
                        
                        message = CANmessage(self.CMU.canBus.CAN_SEND_REQUEST, {'TARGET': sender, 'ORIGIN': self.CMU.objectId})
                        yield self.globalData.env.process(self.CMU.canBus.CANsend(message))         
              
            
    def AckStr_Minimum(self):
        """
        The cell sends charge whenever it is asked, however it refuses to acknowledge a request if the cell itself would be the minimum after the transaction.
        """
        
        while True:
            repeatTime = 1+random.random()
            yield self.CMU.env.timeout(repeatTime)
            
            if ( self.globalData.OptionEvaluateDemonstrator):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if (len(self.activeBalancing.chargeRequesterList)) > 0 and (self.activeBalancing.balancingMode == "idle"):
                #print("start sending at time " + str(self.globalData.env.now))  
                receiver = None
                transferTime = self.globalData.transferTime
                transferRate = self.globalData.transferRate
                compareValue = 1
                
                #find out who deserves the charge most from chargeRequesterList
                for requester in self.activeBalancing.chargeRequesterList:
                    if self.CMU.socList[requester] < compareValue:
                        receiver = requester
                        compareValue = self.CMU.socList[requester]
                
                #if we found someone
                transferEstimate = transferRate / 4 * transferTime / self.globalData.cellCapacity / 3600 * 1.2

                if ((sorted(self.CMU.socList)[0] + transferEstimate) <= (self.CMU.socList[self.CMU.objectId] - transferEstimate)) or (self.CMU.socList[receiver] <= sorted(self.CMU.socList)[0]):
                    retvalue = yield self.globalData.env.process(self.activeBalancing.blockNeighbors(self.CMU.objectId,receiver))
                    if retvalue:
                        #print("sending from "+str(self.CMU.objectId)+" to "+str(receiver)+" at time "+str(self.globalData.env.now))
                        message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE, {'TARGET': receiver, 'ORIGIN': self.CMU.objectId, 'transferTime':transferTime,'transferRate':transferRate})
                        self.globalData.env.process(self.CMU.canBus.CANsend(message))
                        yield self.globalData.env.process(self.activeBalancing.sendCharge(transferTime,transferRate,self.CMU.objectId,receiver))
            self.activeBalancing.chargeRequesterList = []      

    def ReqStr_Maximum(self):
        """
        The cell requests charge if it is the n-th maximum of the pack.
        """
        
        while True:
            requestPause = 1 + random.random()
            if (self.CMU.socList[self.CMU.objectId] is sorted(self.CMU.socList)[-1]):
                requestPause = 0.1
            yield self.CMU.env.timeout(requestPause)
            
            if (self.globalData.OptionEvaluateDemonstrator):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if self.activeBalancing.enabled:
                if (self.activeBalancing.balancingMode == "idle"):
                    if ( (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-1]) or 
                         ( (self.globalData.OptionConcurrent) and (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-self.globalData.OptionConcurrencyDeg]) ) ):
                        
                        if self.activeBalancing.lessChargeOnLeft():
                            receiver = self.CMU.objectId - 1
                        else:
                            receiver = self.CMU.objectId + 1
                            
                        self.activeBalancing.chargeRequesterList.append(receiver)       
              
            
    def AckStr_Maximum(self):
        """
        The cell sends charge whenever it is asked, however it refuses to acknowledge a request if the cell itself would be the maximum after the transaction.
        """
        
        while True:
            repeatTime = 1 + random.random()
            
            if (self.CMU.socList[self.CMU.objectId] is sorted(self.CMU.socList)[-1]):
                repeatTime = 0.1
            yield self.CMU.env.timeout(repeatTime)    
            if ( self.globalData.OptionEvaluateDemonstrator):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if len(self.activeBalancing.chargeRequesterList) > 0 and self.activeBalancing.balancingMode is "idle":
                    #print("start sending at time " + str(self.globalData.env.now))  
                    receiver = None
                    transferTime = self.globalData.transferTime
                    transferRate = self.globalData.transferRate
                    compareValue = 1
                    
                    #find out who deserves the charge most from chargeRequesterList
                    for requester in self.activeBalancing.chargeRequesterList:
                        if self.CMU.socList[requester]<compareValue:
                            receiver = requester
                            compareValue = self.CMU.socList[requester]
                     
                    #if we found someone
                    transferEstimate = transferRate / 4 * transferTime / self.globalData.cellCapacity / 3600 * 1.2
                    # Z_r + Z_est <= Z_max - Z_est
                    if ( (self.CMU.socList[receiver] + transferEstimate) <= (sorted(self.CMU.socList)[-1] - transferEstimate) ) or (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-1]):
                        retvalue = yield self.globalData.env.process(self.activeBalancing.blockNeighbors(self.CMU.objectId,receiver))
                        if retvalue:
                            #print("sending from "+str(self.CMU.objectId)+" to "+str(receiver)+" at time "+str(self.globalData.env.now))
                            message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE, {'TARGET': receiver, 'ORIGIN': self.CMU.objectId, 'transferTime':transferTime,'transferRate':transferRate})
                            self.globalData.env.process(self.CMU.canBus.CANsend(message))
                            yield self.globalData.env.process(self.activeBalancing.sendCharge(transferTime,transferRate,self.CMU.objectId,receiver))
            self.activeBalancing.chargeRequesterList = []   
            

    def ReqStr_MinMax(self):
        """
        Combination of minimum- and maximum-strategies.
        """
        
        while True:
            requestPause = 1 + random.random()
            if ((self.CMU.socList[self.CMU.objectId] is sorted(self.CMU.socList)[-1]) or (self.CMU.socList[self.CMU.objectId] is sorted(self.CMU.socList)[0])):
                requestPause = 0.1
            yield self.CMU.env.timeout(requestPause)

            Concurrency=self.globalData.numberOfCells//2

            if ( self.globalData.OptionEvaluateDemonstrator):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if self.activeBalancing.enabled:
                if (self.activeBalancing.balancingMode == "idle"):
                    if ( (self.CMU.socList[self.CMU.objectId] <= sorted(self.CMU.socList)[0]) or 
                         ( (self.globalData.OptionConcurrent) and (self.CMU.socList[self.CMU.objectId] <= sorted(self.CMU.socList)[Concurrency-1]) ) ):
                        
                        if self.activeBalancing.moreChargeOnLeft():
                            sender = self.CMU.objectId-1
                        else:
                            sender = self.CMU.objectId+1

                        message = CANmessage(self.CMU.canBus.CAN_SEND_REQUEST, {'TARGET': sender, 'ORIGIN': self.CMU.objectId})
                        yield self.globalData.env.process(self.CMU.canBus.CANsend(message))
                    
                    if ( (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-1]) or 
                         ( (self.globalData.OptionConcurrent) and (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-Concurrency]) ) ):
                        
                        if self.activeBalancing.lessChargeOnLeft():
                            receiver = self.CMU.objectId - 1
                        else:
                            receiver = self.CMU.objectId + 1
                            
                        self.activeBalancing.chargeRequesterList.append(receiver)      
              
            
    def AckStr_MinMax(self):
        """
        
        """
        
        while True:
            repeatTime = 1 + random.random()
            yield self.CMU.env.timeout(repeatTime)

            Concurrency = self.globalData.numberOfCells//2
            
            if ( self.globalData.OptionEvaluateDemonstrator):
                if ( (self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency']) ):
                    continue
                else:
                    pass
                
            if (len(self.activeBalancing.chargeRequesterList)>0) and (self.activeBalancing.balancingMode == "idle"):
                    receiver = None
                    transferTime = self.globalData.transferTime
                    transferRate = self.globalData.transferRate
                    compareValue = 1
                    
                    #find out who deserves the charge most from chargeRequesterList
                    for requester in self.activeBalancing.chargeRequesterList:
                        if self.CMU.socList[requester]<compareValue:
                            receiver = requester
                            compareValue = self.CMU.socList[requester]
                    
                    #if we found someone
                    transferEstimate = transferRate / 4 * transferTime / self.globalData.cellCapacity / 3600 * 1.2
                    
                    maxCondition = ( ( (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-1]) or 
                                     ( (self.globalData.OptionConcurrent) and (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-Concurrency]) ) ) and
                                     ( (self.CMU.socList[receiver] + transferEstimate) <= (sorted(self.CMU.socList)[-1] - transferEstimate)) or (self.CMU.socList[self.CMU.objectId] >= sorted(self.CMU.socList)[-1]) )
                    
                    minCondition = ( ( (self.CMU.socList[self.CMU.objectId] <= sorted(self.CMU.socList)[0]) or 
                                     ( (self.globalData.OptionConcurrent) and (self.CMU.socList[self.CMU.objectId] <= sorted(self.CMU.socList)[Concurrency-1]) ) ) and
                                     ( (sorted(self.CMU.socList)[0] + transferEstimate) <= (self.CMU.socList[self.CMU.objectId] - transferEstimate)) or (self.CMU.socList[receiver] <= sorted(self.CMU.socList)[0]) )
    
                    
                    
                    
                    if maxCondition or minCondition:
                                        
                        retvalue = yield self.globalData.env.process(self.activeBalancing.blockNeighbors(self.CMU.objectId,receiver))
                        if retvalue:
                            if (self.globalData.OptionAdaptTransferTime):
                                transferCharge = abs(self.CMU.cellSOC() - self.CMU.socList[receiver]) * self.CMU.cell.charge_as.level
                                transferTime = transferCharge / transferRate
                                if transferTime < 10:
                                    transferTime = 10
                                #print("Balancing: {} As, with {} A in {} s".format(transferCharge, transferRate, transferTime))
                            
                            #print("sending from "+str(self.CMU.objectId)+" to "+str(receiver)+" at time "+str(self.globalData.env.now))
                            message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE, {'TARGET': receiver, 'ORIGIN': self.CMU.objectId, 'transferTime':transferTime,'transferRate':transferRate})
                            self.globalData.env.process(self.CMU.canBus.CANsend(message))
                            yield self.globalData.env.process(self.activeBalancing.sendCharge(transferTime,transferRate,self.CMU.objectId,receiver))
                            
            self.activeBalancing.chargeRequesterList = []
               
 
    def ReqStr_Passive(self):
        """
        Passive balancing for comparison with active balancing.
        """
        
        requestPause = 1 + random.random()
        yield self.CMU.env.timeout(requestPause) 
        transferTime = 1
        transferRate = self.globalData.transferRate / 4
        packmin = min(self.CMU.socList)
        while True:
            if self.activeBalancing.enabled:           
                if (self.CMU.cellSOC() > packmin):
#                     yield self.globalData.env.process(self.activeBalancing.sendCharge(transferTime,transferRate,self.CMU.objectId,self.CMU.objectId))
#                     self.CMU.cell.applyLoadCurrent( tSec=transferTime, iAmps=-transferRate )
                    self.globalData.env.process(self.CMU.charge(transferTime,-transferRate))
                    yield self.globalData.env.timeout(transferTime)
            requestPause = 1 + random.random()
            yield self.CMU.env.timeout(requestPause) 
    
    def AckStr_Passive(self):
        """
        Just a time out sending cells do not exist with passive balancing.
        """
        
        while True:
            repeatTime = 1 + random.random()
            yield self.CMU.env.timeout(repeatTime)

    def ReqStr_Boundary(self):
        """
        The cell requests charge if it is a boundary cell.

        """
        while True:
            requestPause = 1 + random.random()
            yield self.CMU.env.timeout(requestPause)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime +
                                                     self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass

            if self.activeBalancing.enabled and (self.activeBalancing.balancingMode == "idle"):

                ''' Check if the given cell is a boundary cell '''

                if self.activeBalancing.is_boundary_cell():

                    ''' Cell SoC is less/more than the average and therefore takes/gives charge from/to neighbours '''
                    if self.CMU.cellSOC() < mean(self.CMU.socList):

                        transferDirection = self.activeBalancing.boundary_transfer_lower_soc()

                        if (transferDirection == "Send Left"):
                            sender = self.CMU.objectId + 1
                        elif (transferDirection == "Send Right"):
                            sender = self.CMU.objectId - 1
                        else:
                            sender = None

                        if sender is not None:
                            message = CANmessage(self.CMU.canBus.CAN_SEND_REQUEST, {'TARGET': sender,
                                                                                    'ORIGIN': self.CMU.objectId})
                            yield self.globalData.env.process(self.CMU.canBus.CANsend(message))

                    else:

                        transferDirection = self.activeBalancing.boundary_transfer_higher_soc()

                        if (transferDirection == "Send Left"):
                            receiver = self.CMU.objectId - 1
                        elif (transferDirection == "Send Right"):
                            receiver = self.CMU.objectId + 1
                        else:
                            receiver = None

                        if receiver is not None:
                            self.activeBalancing.chargeRequesterList.append(receiver)

    def AckStr_Boundary(self):
        """
        Acknowledge transfer request by the boundary cell.
        """
        while True:
            repeatTime = 1 + random.random()
            yield self.CMU.env.timeout(repeatTime)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (
                        self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass

            if (len(self.activeBalancing.chargeRequesterList) > 0) and (self.activeBalancing.balancingMode == "idle"):
                # print("start sending at time " + str(self.globalData.env.now))
                transferTime = self.globalData.transferTime
                transferRate = self.globalData.transferRate

                if ((self.CMU.objectId + 1) in self.activeBalancing.chargeRequesterList) and (
                    (self.CMU.objectId - 1) in self.activeBalancing.chargeRequesterList):
                    if (self.CMU.socList[self.CMU.objectId + 1] > self.CMU.socList[self.CMU.objectId - 1]):
                        receiver = self.CMU.objectId - 1
                    else:
                        receiver = self.CMU.objectId + 1
                elif ((self.CMU.objectId + 1) <= (self.globalData.numberOfCells - 1)) and (
                    (self.CMU.objectId + 1) in self.activeBalancing.chargeRequesterList):
                    receiver = self.CMU.objectId + 1
                elif ((self.CMU.objectId - 1) >= 0) and (
                    (self.CMU.objectId - 1) in self.activeBalancing.chargeRequesterList):
                    receiver = self.CMU.objectId - 1
                else:
                    receiver = None

                # if we found someone
                if receiver is not None:
                    retvalue = yield self.globalData.env.process(
                        self.activeBalancing.blockNeighbors(self.CMU.objectId, receiver))
                    if retvalue:
                        # print("sending from "+str(self.CMU.objectId)+" to "+str(receiver)+" at time "+str(self.globalData.env.now))
                        message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE,
                                             {'TARGET': receiver, 'ORIGIN': self.CMU.objectId,
                                              'transferTime': transferTime, 'transferRate': transferRate})
                        self.globalData.env.process(self.CMU.canBus.CANsend(message))
                        yield self.globalData.env.process(
                            self.activeBalancing.sendCharge(transferTime, transferRate, self.CMU.objectId, receiver))
            self.activeBalancing.chargeRequesterList = []

    def ReqStr_NNMaxMin(self):

        """
        The cell requests charge as soon as it is below the average SoC value of the pack.

        """
        while True:
            requestPause = 1 + random.random()
            yield self.CMU.env.timeout(requestPause)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass

            '''
            print("Object ID")
            print(str(self.CMU.objectId))
            print("......................")
            print("SOC List unsorted")
            print(str(self.CMU.socList))
            print("......................")
            print("Self SOC")
            print(str((self.CMU.socList[self.CMU.objectId])))
            print("......................")
            '''
            if self.activeBalancing.enabled:
                if (self.activeBalancing.balancingMode == "idle") and (self.CMU.cellSOC() < mean(self.CMU.socList)):

                    '''set range for non-neighbour balancing'''
                    range = 14#self.globalData.rangeValue

                    '''find lower and upper border for consideration of neighbour cells'''
                    lwbrd=0
                    upbrd=0
                    if self.CMU.objectId - range < 0:
                        lwbrd = abs(self.CMU.objectId - range)
                    if self.CMU.objectId + range > self.CMU.globalData.numberOfCells - 1:
                        upbrd = abs(self.CMU.objectId + range - self.CMU.globalData.numberOfCells + 1)

                    '''finding maximum of charge in range and define sender'''
                    if self.activeBalancing.moreChargeOnLeftNN(range, lwbrd, upbrd):
                        TargetSOC=sorted(self.CMU.socList[self.CMU.objectId-range+lwbrd:self.CMU.objectId])[::-1][0]
                        #print("Target SOC " + str(TargetSOC))
                        #self.CMU.objectId-range+lwbrd:self.CMU.objectId
                        #print("sender " + str(sender))
                    else:
                        TargetSOC=sorted(self.CMU.socList[self.CMU.objectId+1:self.CMU.objectId+range-upbrd+1])[::-1][0]
                        #print("Target SOC " + str(TargetSOC))
                        #self.CMU.objectId + 1:self.CMU.objectId+range-upbrd+1
                        #print(str(self.CMU.socList))
                        #print("sender " + str(sender))
                    sender = self.CMU.socList[:].index(TargetSOC)
                    #print("request: " + str(self.CMU.objectId) + " asks for " + str(sender)+" with "+str(round((self.CMU.cellSOC()-mean(self.CMU.socList))/mean(self.CMU.socList)*10000)))
                    message = CANmessage(self.CMU.canBus.CAN_SEND_REQUEST, {'TARGET': sender, 'ORIGIN': self.CMU.objectId})
                    yield self.globalData.env.process(self.CMU.canBus.CANsend(message))

    def AckStr_NNMaxMin(self):
        """
        A cell acknowledges a request for charge if its SoC is greater than the pack average.
        """
        while True:
            repeatTime = 1 + random.random()
            yield self.CMU.env.timeout(repeatTime)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime + self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass
            #print("a: "+str(self.CMU.objectId)+" reql: "+str(self.activeBalancing.chargeRequesterList)+" c: "+str(self.CMU.cellSOC() > mean(self.CMU.socList)))
            #print(str(self.CMU.objectId)+" "+str(len(self.activeBalancing.chargeRequesterList) > 0) + " "+str(self.activeBalancing.balancingMode == "idle") + " "+str(self.CMU.cellSOC() > mean(self.CMU.socList)))
            if ( len(self.activeBalancing.chargeRequesterList) > 0) and (self.activeBalancing.balancingMode == "idle") and (self.CMU.cellSOC() > mean(self.CMU.socList)):
                # print("start sending at time " + str(self.globalData.env.now))
                transferTime = self.globalData.transferTime
                transferRate = self.globalData.transferRate

                range = 14#self.globalData.rangeValue

                '''find lower and upper border for consideration of neighbour cells'''
                lwbrd = 0
                upbrd = 0
                if self.CMU.objectId - range < 0:
                    lwbrd = abs(self.CMU.objectId - range)
                if self.CMU.objectId + range > self.CMU.globalData.numberOfCells - 1:
                    upbrd = abs(self.CMU.objectId + range - self.CMU.globalData.numberOfCells + 1)

                '''finding minimum of charge in range and define receiver'''
                if self.activeBalancing.lessChargeOnLeftNN(range, lwbrd, upbrd):
                    TargetSOC = sorted(self.CMU.socList[self.CMU.objectId - range + lwbrd:self.CMU.objectId])[0]
                    #self.CMU.objectId - range + lwbrd:self.CMU.objectId
                else:
                    TargetSOC = sorted(self.CMU.socList[self.CMU.objectId + 1:self.CMU.objectId + range - upbrd + 1])[0]
                    # self.CMU.objectId + 1:self.CMU.objectId + range - upbrd
                receiver = self.CMU.socList[:].index(TargetSOC)
                #print("ID: "+str(self.CMU.objectId)+" recv: "+str(receiver))
                #print("reqlist: "+str(self.activeBalancing.chargeRequesterList))

                # find out who deserves the charge most from chargeRequesterList
                if receiver not in self.activeBalancing.chargeRequesterList:
                    #print("notinList list: "+str(self.activeBalancing.chargeRequesterList))
                    #print(str(self.CMU.socList))
                    #print("...............")
                    receiver = None

                # if we found someone
                if receiver is not None:
                    #print("ack: ID: " + str(self.CMU.objectId) + " Mode: " + self.activeBalancing.balancingMode)
                    retvalue = yield self.globalData.env.process(self.activeBalancing.blockNeighbors(self.CMU.objectId, receiver))
                    if retvalue:
                        message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE,{'TARGET': receiver, 'ORIGIN': self.CMU.objectId,'transferTime': transferTime, 'transferRate': transferRate})
                        self.globalData.env.process(self.CMU.canBus.CANsend(message))
                        #print("........................................................................................")
                        #print("ack: ID: " + str(self.CMU.objectId) + " Mode: " + self.activeBalancing.balancingMode)
                        yield self.globalData.env.process(
                            self.activeBalancing.sendCharge(transferTime, transferRate, self.CMU.objectId, receiver))
            self.activeBalancing.chargeRequesterList = []

    def ReqStr_NNModeBC(self):

        """
        The cell requests charge as soon as it is below the average SoC value of the pack.

        """
        while True:
            requestPause = 1 + random.random()
            yield self.CMU.env.timeout(requestPause)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime +
                                                     self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass

            if self.activeBalancing.enabled and (self.activeBalancing.balancingMode == "idle") and (
                self.CMU.cellSOC() < mean(self.CMU.socList)):
            # print("request: " + str(self.CMU.objectId) + " asks for " + str(sender)+" with "+str(round((self.CMU.cellSOC()-mean(self.CMU.socList))/mean(self.CMU.socList)*10000)))
                '''set range for non-neighbour balancing'''
                maxRange = self.globalData.rangeValue

                '''find lower and upper border for consideration of neighbour cells'''
                lwbrd = 0
                upbrd = 0
                if self.CMU.objectId - maxRange < 0:
                    lwbrd = abs(self.CMU.objectId - maxRange)
                if self.CMU.objectId + maxRange > self.CMU.globalData.numberOfCells - 1:
                    upbrd = abs(self.CMU.objectId + maxRange - self.CMU.globalData.numberOfCells + 1)

                '''finding unblocked list of cells around cellID -> achievable cells'''
                startID = self.CMU.objectId
                endID = self.CMU.objectId

                for i in range(1, maxRange-lwbrd+1):
                    if(self.CMU.modeList[self.CMU.objectId-i] =="idle"):
                        startID = self.CMU.objectId-i
                    else:
                        break
                for i in range(1, maxRange-upbrd+1):
                    if(self.CMU.modeList[self.CMU.objectId+i] =="idle"):
                        endID = self.CMU.objectId + i
                    else:
                        break

                '''look if I am weakest cell in achievable idle cells'''
                if(self.CMU.cellSOC()==min(self.CMU.socList[startID:endID+1])):
                    '''max of achievable cells should be sender'''
                    TargetSOC = max(self.CMU.socList[startID:endID+1])
                    sender = self.CMU.socList[:].index(TargetSOC)

                    message = CANmessage(self.CMU.canBus.CAN_SEND_REQUEST,{'TARGET': sender, 'ORIGIN': self.CMU.objectId})
                    yield self.globalData.env.process(self.CMU.canBus.CANsend(message))

    def AckStr_NNModeBC(self):
        """
        A cell acknowledges a request for charge if its SoC is greater than the pack average.
        """
        while True:
            repeatTime = 1 + random.random()
            yield self.CMU.env.timeout(repeatTime)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime +
                                                     self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass
            # print("a: "+str(self.CMU.objectId)+" reql: "+str(self.activeBalancing.chargeRequesterList)+" c: "+str(self.CMU.cellSOC() > mean(self.CMU.socList)))
            # print(str(self.CMU.objectId)+" "+str(len(self.activeBalancing.chargeRequesterList) > 0) + " "+str(self.activeBalancing.balancingMode == "idle") + " "+str(self.CMU.cellSOC() > mean(self.CMU.socList)))

            if (len(self.activeBalancing.chargeRequesterList) > 0) and (
                self.activeBalancing.balancingMode == "idle") and (self.CMU.cellSOC() > mean(self.CMU.socList)):
                # print("start sending at time " + str(self.globalData.env.now))
                transferTime = self.globalData.transferTime
                transferRate = self.globalData.transferRate

                maxRange = self.globalData.rangeValue

                '''find lower and upper border for consideration of neighbour cells'''
                lwbrd = 0
                upbrd = 0
                if self.CMU.objectId - maxRange < 0:
                    lwbrd = abs(self.CMU.objectId - maxRange)
                if self.CMU.objectId + maxRange > self.CMU.globalData.numberOfCells - 1:
                    upbrd = abs(self.CMU.objectId + maxRange - self.CMU.globalData.numberOfCells + 1)

                '''finding unblocked list of cells around cellID -> achievable cells'''
                startID = self.CMU.objectId
                endID = self.CMU.objectId

                for i in range(1, maxRange - lwbrd+1):
                    if (self.CMU.modeList[self.CMU.objectId - i] == "idle"):
                        startID = self.CMU.objectId - i
                    else:
                        break
                for i in range(1, maxRange - upbrd+1):
                    if (self.CMU.modeList[self.CMU.objectId + i] == "idle"):
                        endID = self.CMU.objectId + i
                    else:
                        break

                '''look if I am strongest cell of achievable idle cells'''
                receiver = None
                if (self.CMU.cellSOC() == max(self.CMU.socList[startID:endID + 1])):
                    '''min of achievable cells should be receiver'''
                    TargetSOC = min(self.CMU.socList[startID:endID + 1])
                    receiver = self.CMU.socList[:].index(TargetSOC)

                # find out who deserves the charge most from chargeRequesterList
                if receiver not in self.activeBalancing.chargeRequesterList:
                    receiver = None

                # if we found someone
                if receiver is not None:
                    # print("ack: ID: " + str(self.CMU.objectId) + " Mode: " + self.activeBalancing.balancingMode)
                    retvalue = yield self.globalData.env.process(
                        self.activeBalancing.blockNeighbors(self.CMU.objectId, receiver))
                    if retvalue:
                        message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE,
                                             {'TARGET': receiver, 'ORIGIN': self.CMU.objectId,
                                              'transferTime': transferTime, 'transferRate': transferRate})
                        self.globalData.env.process(self.CMU.canBus.CANsend(message))
                        # print("........................................................................................")
                        # print("ack: ID: " + str(self.CMU.objectId) + " Mode: " + self.activeBalancing.balancingMode)
                        yield self.globalData.env.process(
                            self.activeBalancing.sendCharge(transferTime, transferRate, self.CMU.objectId,
                                                            receiver))
            self.activeBalancing.chargeRequesterList = []

    def ReqStr_NNAdapt(self):

        """
        The cell requests charge as soon as it is below the average SoC value of the pack.

        """
        while True:
            requestPause = 1 + random.random()
            yield self.CMU.env.timeout(requestPause)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime +
                                                     self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass

            if self.activeBalancing.enabled and (self.activeBalancing.balancingMode == "idle") and (
                self.CMU.cellSOC() < mean(self.CMU.socList)):
            # print("request: " + str(self.CMU.objectId) + " asks for " + str(sender)+" with "+str(round((self.CMU.cellSOC()-mean(self.CMU.socList))/mean(self.CMU.socList)*10000)))

                '''set range for non-neighbour balancing'''
                maxRange = 95#self.globalData.rangeValue
                adaptRange = self.CMU.adaptrange
                if adaptRange <= 1:
                    adaptRange = maxRange

                '''find borders to not break list index while considering non-neighbour transfers'''
                lwbrd = 0
                upbrd = 0
                if self.CMU.objectId - maxRange < 0:
                    lwbrd = abs(self.CMU.objectId - maxRange)
                if self.CMU.objectId + maxRange > self.CMU.globalData.numberOfCells - 1:
                    upbrd = abs(self.CMU.objectId + maxRange - self.CMU.globalData.numberOfCells + 1)

                '''finding unblocked list of cells around cellID -> achievable cells'''
                startID = self.CMU.objectId
                endID = self.CMU.objectId

                for i in range(1, maxRange-lwbrd+1):
                    if(self.CMU.modeList[self.CMU.objectId-i] =="idle"):
                        startID = self.CMU.objectId-i
                    else:
                        break
                for i in range(1, maxRange-upbrd+1):
                    if(self.CMU.modeList[self.CMU.objectId+i] =="idle"):
                        endID = self.CMU.objectId + i
                    else:
                        break

                '''find best transfer in subset of idle cells which does not exceed the set transfer range'''
                best_transfer = self.activeBalancing.findPartners(SOCList=self.CMU.socList, startID=startID, endID=endID, maxRange=adaptRange)

                #best_transfer = senderID, receiverID, range, SOC difference

                '''look if I am the receiver of this best transfer'''
                if best_transfer!=0:
                    if(self.CMU.objectId==best_transfer[1]):
                        '''max of achievable cells should be sender'''
                        sender = best_transfer[0]

                        message = CANmessage(self.CMU.canBus.CAN_SEND_REQUEST,{'TARGET': sender, 'ORIGIN': self.CMU.objectId})
                        yield self.globalData.env.process(self.CMU.canBus.CANsend(message))

    def AckStr_NNAdapt(self):
        """
        A cell acknowledges a request for charge if its SoC is greater than the pack average.
        """
        while True:
            repeatTime = 1 + random.random()
            yield self.CMU.env.timeout(repeatTime)

            if (self.globalData.OptionEvaluateDemonstrator):
                if ((self.globalData.env.now) < (self.CMU.lastTransaction + self.globalData.transferTime +
                                                     self.globalData.evaluationSettings['OCVlatency'])):
                    continue
                else:
                    pass
            # print("a: "+str(self.CMU.objectId)+" reql: "+str(self.activeBalancing.chargeRequesterList)+" c: "+str(self.CMU.cellSOC() > mean(self.CMU.socList)))
            # print(str(self.CMU.objectId)+" "+str(len(self.activeBalancing.chargeRequesterList) > 0) + " "+str(self.activeBalancing.balancingMode == "idle") + " "+str(self.CMU.cellSOC() > mean(self.CMU.socList)))

            if (len(self.activeBalancing.chargeRequesterList) > 0) and (
                self.activeBalancing.balancingMode == "idle") and (self.CMU.cellSOC() > mean(self.CMU.socList)):
                # print("start sending at time " + str(self.globalData.env.now))
                transferTime = self.globalData.transferTime
                transferRate = self.globalData.transferRate

                maxRange = 95#self.globalData.rangeValue
                adaptRange = self.CMU.adaptrange
                if adaptRange == 1:
                    adaptRange = maxRange

                '''find lower and upper border for consideration of neighbour cells'''
                lwbrd = 0
                upbrd = 0
                if self.CMU.objectId - maxRange < 0:
                    lwbrd = abs(self.CMU.objectId - maxRange)
                if self.CMU.objectId + maxRange > self.CMU.globalData.numberOfCells - 1:
                    upbrd = abs(self.CMU.objectId + maxRange - self.CMU.globalData.numberOfCells + 1)

                '''finding unblocked list of cells around cellID -> achievable cells'''
                startID = self.CMU.objectId
                endID = self.CMU.objectId

                for i in range(1, maxRange - lwbrd+1):
                    if (self.CMU.modeList[self.CMU.objectId - i] == "idle"):
                        startID = self.CMU.objectId - i
                    else:
                        break
                for i in range(1, maxRange - upbrd+1):
                    if (self.CMU.modeList[self.CMU.objectId + i] == "idle"):
                        endID = self.CMU.objectId + i
                    else:
                        break

                '''find best transfer in subset of idle cells which does not exceed the set transfer range'''
                receiver=None
                best_transfer = self.activeBalancing.findPartners(SOCList=self.CMU.socList, startID=startID, endID=endID, maxRange=adaptRange)
                #best_transfer = senderID, receiverID, range, SOC difference

                '''if I am sender of this best transfer'''
                if best_transfer !=0:
                    if (self.CMU.objectId==best_transfer[0]):
                        receiver = best_transfer[1]

                # find out who deserves the charge most from chargeRequesterList
                if receiver not in self.activeBalancing.chargeRequesterList:
                    receiver = None

                # if we found someone
                if receiver is not None:

                    retvalue = yield self.globalData.env.process(
                        self.activeBalancing.blockNeighbors(self.CMU.objectId, receiver))
                    if retvalue:
                        message = CANmessage(self.CMU.canBus.CAN_SEND_ACKNOWLEDGE,
                                             {'TARGET': receiver, 'ORIGIN': self.CMU.objectId,
                                              'transferTime': transferTime, 'transferRate': transferRate})
                        self.globalData.env.process(self.CMU.canBus.CANsend(message))

                        '''store own soc and soc of receiver'''
                        soctb = self.CMU.cellSOC()
                        socrb = self.CMU.socList[receiver]

                        # print("........................................................................................")
                        # print("ack: ID: " + str(self.CMU.objectId) + " Mode: " + self.activeBalancing.balancingMode)
                        yield self.globalData.env.process(
                            self.activeBalancing.sendCharge(transferTime, transferRate, self.CMU.objectId,
                                                            receiver))

                        '''calculate eta and broadcast new range'''
                        eta = self.activeBalancing.calc_efficiency(soctb=soctb, socrb=socrb, socte=self.CMU.cellSOC(), socre=self.CMU.socList[receiver])
                        if eta < 0.3 and eta > 0:
                            NewRange = adaptRange - 1
                            self.CMU.adaptrange = NewRange
                            message = CANmessage(self.CMU.canBus.CAN_RANGE_BROADCAST,{'TARGET' :'BROADCAST', 'ORIGIN':self.CMU.objectId, 'NewRange':NewRange})
                            self.globalData.env.process(self.CMU.canBus.CANsend(message))
            self.activeBalancing.chargeRequesterList = []

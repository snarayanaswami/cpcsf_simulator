"""
=================================
Control of active balancing logic
=================================

**file** : ActiveBalancing.py

**date** : 3-July-2015
"""

import random
import simpy
from numpy import mean
from .CANmessage import CANmessage
from smartcellsimulator.transferdyn.transferspec import TransferSpec
from .BalancingStrategies import BalancingStrategies

class ActiveBalancing(object):
    """
    Class implementing the logic behind active balancing actions.
    
    Attributes:
        CMU : Cell management unit of the smart cell
    """    
    def __init__(self, globalData, CMU):
        self.globalData = globalData
        self.CMU = CMU
        self.balancingMode = "idle"
        self.blockRequester = []
        self.chargeRequesterList = []
        self.enabled = False
        
        self.balancingStrategies = BalancingStrategies(self, globalData, CMU)
        
        # start Processes
        self.globalData.env.process(self.globalData.OptionStrategy['Ack'](self.balancingStrategies))
        self.globalData.env.process(self.globalData.OptionStrategy['Req'](self.balancingStrategies))

        self.globalData.env.process(self.CellMonitoring())
        self.canMessageResponseQueue = simpy.Store(self.globalData.env)
        #self.canProcessResponseQueue=simpy.Store(self.globalData.env)
        self.acknowledgeCount = 0

    def BroadCastSoC(self):
        """
        Construct a SoC broadcast message and put it into sending queue.
        """
        
        message = CANmessage(self.CMU.canBus.CAN_SOC_BROADCAST, {'TARGET' :'BROADCAST', 'ORIGIN':self.CMU.objectId, 'soc':self.CMU.cellSOC()})
        self.globalData.env.process(self.CMU.canBus.CANsend(message))
        
        
    def receiveCharge(self,duration,rate):
        """
        Start process of receiving charge (this is just a timeout, as the architecture is 'sender driven' and charge is put into the cell).
        
        Args:
            duration: Duration of the balancing transaction
            rate: Value of the peak current
        """
        
        # Safe time of last transaction (used evaluating demonstrator)
        self.CMU.lastTransaction = self.globalData.env.now
        # Set 'receiving-charge-color' of cell in bar-chart
        self.globalData.barColors[self.CMU.objectId]="008000"
        
        # Step charging process in minTimeSteps to allow recording every value
        while duration >= self.globalData.minTimeStep:
            yield self.globalData.env.timeout(self.globalData.minTimeStep)  
            duration = duration-self.globalData.minTimeStep
            
        if duration > 0:
            yield self.globalData.env.timeout(duration)
        
        # Set 'idle-color' of cell in bar-chart
        self.globalData.barColors[self.CMU.objectId]="#666666"
        
        # If enabled, send broadcast after transaction
        if (self.globalData.OptionBCAfterTransaction):
            self.BroadCastSoC()
       
    def sendCharge(self,duration,rate,senderID,receiverID):
        """
        Start process of sending charge (architecture puts takes care of the receiver-cell).
        
        Args:
            duration: Duration of the balancing transaction
            rate: Value of the peak current
            senderID: ID of the sending cell
            receiverID: The receiving cell's ID
        """
        
        # Safe time of last transaction (used evaluating demonstrator)
        self.CMU.lastTransaction = self.globalData.env.now
        # Set 'sending-charge-color' of cell in bar-chart
        self.globalData.barColors[self.CMU.objectId] = "800000"
        #print("starting discharge of cell "+str(self.CMU.objectId)+" at "+str(self.globalData.env.now))

#         t = TransferSpec(send=senderID, recv=receiverID, iPeakAmps=rate, amount=duration, measure='T')
#         self.CMU.cell.arch.execTransfer(t)
#         yield self.globalData.env.timeout(duration)
        
        tBal = 0
        # Step charging process in minTimeSteps to allow recording every value
        while duration>=self.globalData.minTimeStep:
            yield self.globalData.env.timeout(self.globalData.minTimeStep)
            t = TransferSpec(send=senderID, recv=receiverID, iPeakAmps=rate, amount=self.globalData.minTimeStep, measure='T')
            tTransfer = self.CMU.cell.arch.execTransfer(t)
            duration=duration-self.globalData.minTimeStep
            tBal = tBal + tTransfer
             
        if duration>0:
            yield self.globalData.env.timeout(duration)  
            t = TransferSpec(send=senderID, recv=receiverID, iPeakAmps=rate, amount=duration, measure='T')
            tTransfer = self.CMU.cell.arch.execTransfer(t)
            tBal = tBal + tTransfer
            
        
        self.globalData.tBalTotal = self.globalData.tBalTotal + tBal
        
        # Send unblock request to end transaction
        self.unBlockNeighbors(senderID, receiverID)
        
        # Set 'idle-color' of cell in bar-chart
        self.globalData.barColors[self.CMU.objectId]="#666666"
        
        # If enabled, send broadcast after transaction
        if (self.globalData.OptionBCAfterTransaction):
            self.BroadCastSoC()
        #print("stopped discharge of cell "+str(self.CMU.objectId)+" at "+str(self.globalData.env.now))
            
    def getInList(self, senderID, receiverID):
        """
        Construct a list of participants of a transaction (need for 
        
        Args:
            int senderID: ID of the sending cell
            receiverID: The receiving cell's ID
            List of IDs with the participating cells of the transaction
        """
        
        if senderID > receiverID:
            '''blocks neighbours while balancing'''
            #startID = receiverID-1
            #endID = senderID+2
            '''does not block neighbours while balancing'''
            startID = receiverID
            endID = senderID + 1
        else:
            '''blocks neighbours while balancing'''
            #startID = senderID-1
            #endID = receiverID+2
            '''does not block neighbours while balancing'''
            startID = senderID
            endID = receiverID + 1
            
        if startID < 0:
            startID = 0
        if endID > self.globalData.numberOfCells:
            endID = self.globalData.numberOfCells
            
        return range(startID,endID)

    def ACBmessageHandler(self,RxMsg):
        """
        Message handler for messages coming in from the CAN bus.
        
        Args:
            RxMsg: The received CAN-message
        """
        
        if (RxMsg.identifier == self.CMU.canBus.CAN_BLOCK_REQUEST):
            ownID = self.CMU.objectId
            senderID = RxMsg.data['SENDER_ID']
            receiverID = RxMsg.data['RECEIVER_ID']

            for CellID in self.getInList(senderID=senderID, receiverID=receiverID):
                self.CMU.modeList[CellID]="blocked"
            
            if (ownID in self.getInList(senderID, receiverID)):
                result = False
                if (self.balancingMode == "idle"):
                    if ( (ownID == senderID) or (ownID==receiverID) ):
                        self.balancingMode="blocked"
                        #self.globalData.barColors[self.CMU.objectId]="8A458A"
                        self.blockRequester.append(senderID)
                        result = True
                    else:
                        self.balancingMode = "blockedWeak"
                        self.globalData.barColors[self.CMU.objectId] = "D46A6A"
                        self.blockRequester.append(senderID)
                        result = True
                else:
                    if (self.balancingMode == "blockedWeak"):
                        if ( (ownID == senderID) or (ownID == receiverID) ):
                            result = False
                        else:
                            self.balancingMode = "blocked"
                            self.globalData.barColors[self.CMU.objectId] = "8A458A"
                            self.blockRequester.append(senderID)
                            result = True
                    
                message = CANmessage(self.CMU.canBus.CAN_STATUS_RESPONSE, {'TARGET' :RxMsg.data['ORIGIN'], 'ORIGIN':self.CMU.objectId, 'STATUS':result})    
                self.globalData.env.process(self.CMU.canBus.CANsend(message)) 
                
        if (RxMsg.identifier == self.CMU.canBus.CAN_UNBLOCK_REQUEST):
            ownID = self.CMU.objectId
            senderID = RxMsg.data['SENDER_ID']
            receiverID = RxMsg.data['RECEIVER_ID']

            for CellID in self.getInList(senderID=senderID, receiverID=receiverID):
                self.CMU.modeList[CellID]="idle"
            
            if (ownID in self.getInList(senderID, receiverID)):
                
                if (self.balancingMode == "blockedWeak") and (senderID in self.blockRequester):
                    self.blockRequester.remove(senderID)
                    self.balancingMode = "blockedWeak"
                    if len(self.blockRequester) is 0:
                        self.balancingMode = "idle"
                        self.globalData.barColors[self.CMU.objectId]="666666"
                        
                if (self.balancingMode == "blocked") and (senderID in self.blockRequester):
                    self.blockRequester.remove(senderID)
                    self.balancingMode = "blockedWeak"
                    self.globalData.barColors[self.CMU.objectId] = "D46A6A"
                    if len(self.blockRequester) is 0:
                        self.balancingMode = "idle"
                        self.globalData.barColors[self.CMU.objectId] = "666666"

        if ((RxMsg.identifier == self.CMU.canBus.CAN_SEND_REQUEST) and (RxMsg.data["TARGET"] == self.CMU.objectId)):
            self.activeCellBalancingSendRequestHandler(RxMsg.data["ORIGIN"],RxMsg.data)
        
        if ((RxMsg.identifier == self.CMU.canBus.CAN_SEND_ACKNOWLEDGE) and (RxMsg.data['TARGET'] == self.CMU.objectId)):
            yield self.globalData.env.process(self.receiveCharge(RxMsg.data["transferTime"],RxMsg.data["transferRate"]))
            
        if (RxMsg.identifier == self.CMU.canBus.CAN_BALANCE_CONTROL):
            self.enabled = RxMsg.data["activeBalancingControl"]
            
        if (RxMsg.identifier == self.CMU.canBus.CAN_STATUS_RESPONSE) and (RxMsg.data['TARGET'] == self.CMU.objectId): 
            #print("Answer from " +str(RxMsg.identifier)+" to " +str(self.CMU.objectId)+" answer is "+str(RxMsg.data["activeBalancingMsgResponseValue"])+" at time "+str(self.globalData.env.now))
            yield self.canMessageResponseQueue.put(RxMsg.data)
            #print("Queue length for cell "+str(self.CMU.objectId)+" is "+str(len(self.canMessageResponseQueue.items)))

        '''set new range when getting broadcast message of range'''
        if (RxMsg.identifier == self.CMU.canBus.CAN_RANGE_BROADCAST):
            self.CMU.adaptrange = RxMsg.data["NewRange"]

    def blockNeighbors(self,senderID,receiverID):
        """
        Block the neighboring cells for performing the transfer.
         
        Args:
            senderID: ID of the sending cell
            receiverID: The receiving cell's ID
            
        Returns:
            Boolean if blocking was successful
        """
        
        msg=CANmessage(self.CMU.canBus.CAN_BLOCK_REQUEST, {'TARGET': 'BROADCAST', 'ORIGIN': self.CMU.objectId, 'SENDER_ID':senderID, 'RECEIVER_ID':receiverID})
        self.globalData.env.process(self.CMU.canBus.CANsend(msg))
        
        listOk = []
        listReplied = []
        
        # Wait for responses from participating cells of the transaction
        while (len(listReplied) < len(self.getInList(senderID, receiverID))):
            response = yield self.canMessageResponseQueue.get()
            # if response is positive and origin is unique, add to response list
            if (response['ORIGIN'] in self.getInList(senderID, receiverID)) and (response['ORIGIN'] not in listReplied): 
                listOk.append(response['STATUS'])
                listReplied.append(response['ORIGIN'])

        if all(listOk):
            return True
        else:
            self.unBlockNeighbors(senderID,receiverID)
            return False
        
            
    def unBlockNeighbors(self,senderID,receiverID):
        """
        Constructs and sends an unblock-message informing the participants of a transaction about its end.
        
        Args:
            senderID: ID of the sending cell
            receiverID: The receiving cell's ID
        """
        
        msg = CANmessage(self.CMU.canBus.CAN_UNBLOCK_REQUEST, {'TARGET': 'BROADCAST', 'ORIGIN': self.CMU.objectId, 'SENDER_ID':senderID, 'RECEIVER_ID':receiverID})
        self.globalData.env.process(self.CMU.canBus.CANsend(msg))
            
            
    def activeCellBalancingSendRequestHandler(self,requesterID,data):
        """
        Adds the ID of a requesting cell into the queue of requesters
        
        Args:
            requesterID: ID of the cell requesting charge
        """
        
        if self.balancingMode is "idle":
                self.chargeRequesterList.append(requesterID)

    def is_boundary_cell(self):
        """
        Determine if a cell is boundary cell.

        Returns:
            Boolean True if boundary cell.
        """

        left_average = mean(self.CMU.socList[:self.CMU.objectId]) if (self.CMU.objectId != 0) else 0
        left_cells = len(self.CMU.socList[:self.CMU.objectId])
        right_average = mean(self.CMU.socList[self.CMU.objectId + 1:]) if (
        self.CMU.objectId != self.globalData.numberOfCells - 1) else 0
        right_cells = len(self.CMU.socList[self.CMU.objectId + 1:])
        desiredAverage = mean(self.CMU.socList)

        left_difference = desiredAverage - left_average
        right_difference = desiredAverage - right_average

        # Least amount of margin without any instability
        error_margin = (self.globalData.OptionStopDiff) / 2

        # find the left and right differences to find the ratio of transfer
        self.CMU.left_transfer = (left_difference * left_cells) if ((left_average != 0)) else 0
        self.CMU.right_transfer = (right_difference * right_cells) if ((right_average != 0)) else 0

        if ((self.CMU.objectId == 0) and (abs(self.CMU.right_transfer) > error_margin)) or (
            (self.CMU.objectId == self.globalData.numberOfCells - 1) and (
            abs(self.CMU.left_transfer) > (error_margin))):
            return True
        elif (
            (((left_average + error_margin) < desiredAverage) and ((right_average + error_margin) < desiredAverage)) or
            (((left_average - error_margin) > desiredAverage) and ((right_average - error_margin) > desiredAverage))) or \
                ((self.CMU.cellSOC() + error_margin) < desiredAverage) or (
            (self.CMU.cellSOC() - error_margin) > desiredAverage):
            return True
        # elif ((left_average < self.CMU.cellSOC()) and (right_average < self.CMU.cellSOC())) or \
        #         ((left_average > self.CMU.cellSOC()) and (right_average > self.CMU.cellSOC())):
        #     if ((self.CMU.cellSOC() + error_margin) < desiredAverage) or ((self.CMU.cellSOC() - error_margin) > desiredAverage) or \
        #         ((left_average + error_margin) < desiredAverage) or ((right_average + error_margin) < desiredAverage) or \
        #         ((left_average - error_margin) > desiredAverage) or ((right_average - error_margin) > desiredAverage):
        #         return True
        #     else:
        #         return False
        else:
            return False

    def boundary_transfer_higher_soc(self):
        """
        Decide whether the charge is to be transferred left or right.

        Returns:
            String specifying the Direction
        """
        if (self.CMU.objectId == 0):
            if (self.CMU.right_transfer > 0):
                return "Send Right"
            else:
                return -1
        elif (self.CMU.objectId == self.globalData.numberOfCells - 1):
            if (self.CMU.left_transfer > 0):
                return "Send Left"
            else:
                return -1

        if (self.CMU.left_transfer > self.CMU.right_transfer) and (self.CMU.left_transfer > 0):
            return "Send Left"
        elif (self.CMU.right_transfer > 0):
            return "Send Right"
        else:
            return -1

    def boundary_transfer_lower_soc(self):
        """
        Decide whether the charge is to be received from left or right.

        Returns:
            String specifying the Direction
        """
        if (self.CMU.objectId == 0):
            if (self.CMU.right_transfer < 0):
                return "Send Left"
            else:
                return -1
        elif (self.CMU.objectId == self.globalData.numberOfCells - 1):
            if (self.CMU.left_transfer < 0):
                return "Send Right"
            else:
                return -1

        if (self.CMU.left_transfer < self.CMU.right_transfer) and (self.CMU.left_transfer < 0):
            return "Send Right"
        elif (self.CMU.right_transfer < 0):
            return "Send Left"
        else:
            return -1

    def moreChargeOnLeft(self):
        """
        Test whether the amount of charge left of the cell is greater than on the right side.
        
        Returns:
            Boolean
        """
        
        if self.CMU.objectId is 0:
            return False
        if self.CMU.objectId is self.CMU.globalData.numberOfCells-1:
            return True
        meanValLeft=mean(self.CMU.socList[:self.CMU.objectId])
        meanValRight=mean(self.CMU.socList[self.CMU.objectId+1:])
        if meanValLeft>meanValRight:
            return True
        else: 
            return False

    def moreChargeOnLeftNN(self, range, lwbrd, upbrd):
        if self.CMU.objectId is 0:
            return False
        if self.CMU.objectId is self.CMU.globalData.numberOfCells-1:
            return True
        #find max soc within range on left and right of cell Id, upbrd and lwbrd avoid list errors
        maxValLeft = max(self.CMU.socList[self.CMU.objectId-range+lwbrd:self.CMU.objectId])
        maxValRight = max(self.CMU.socList[self.CMU.objectId + 1:self.CMU.objectId+range-upbrd+1])

        if maxValLeft>maxValRight:
            return True
        else:
            return False

    def lessChargeOnLeft(self):
        """
        Test whether the amount of charge left of the cell is less than on the right side.
        
        Returns:
            Boolean
        """
        
        if self.CMU.objectId is 0:
            return False
        if self.CMU.objectId is self.CMU.globalData.numberOfCells-1:
            return True
        meanValLeft=mean(self.CMU.socList[:self.CMU.objectId])
        meanValRight=mean(self.CMU.socList[self.CMU.objectId+1:])
        if meanValLeft<meanValRight:
            return True
        else: 
            return False

    def lessChargeOnLeftNN(self, range, lwbrd, upbrd):
        if self.CMU.objectId is 0:
            return False
        if self.CMU.objectId is self.CMU.globalData.numberOfCells - 1:
            return True
        # find min soc within range on left and right of cell Id, upbrd and lwbrd avoid list errors
        minValLeft = min(self.CMU.socList[self.CMU.objectId - range + lwbrd:self.CMU.objectId])
        minValRight = min(self.CMU.socList[self.CMU.objectId + 1:self.CMU.objectId + range - upbrd + 1])

        if minValLeft < minValRight:
            return True
        else:
            return False
        
    # From here on comes the monitoring strategy
    
    def CellMonitoring(self):
        """
        Monitor the cell values to start and stop balancing.
        
        """
        monitoringPause = 5
        yield self.CMU.env.timeout(random.random())
        while True:
            # Monitoring of pack is only possible when information about every cell is listed
            if all(self.CMU.socList):
                # Enable active balancing when SoC difference is greater than set threshold
                if ( (self.enabled == False) and (abs(min(self.CMU.socList) - max(self.CMU.socList)) > self.globalData.OptionStartDiff) ):
                    message = CANmessage(self.CMU.canBus.CAN_BALANCE_CONTROL, {'TARGET': 'BROADCAST', 'ORIGIN': self.CMU.objectId, 'activeBalancingControl':True})
                    self.globalData.env.process(self.CMU.canBus.CANsend(message)) 
#                     print("Enabled Balancing! at {}".format(self.globalData.env.now))
#                     print("MinCellSOC: {:.3f}".format(min(self.CMU.socList)))
#                     print("MaxCellSOC: {:.3f}".format(max(self.CMU.socList)))
#                     print("SOC diff: {:.4f}".format(abs(min(self.CMU.socList) - max(self.CMU.socList))))
            
                # Disable active balancing when SoC difference is less than set threshold
                if ( (self.enabled == True) and (abs(min(self.CMU.socList) - max(self.CMU.socList)) < self.globalData.OptionStopDiff) ):
                    message = CANmessage(self.CMU.canBus.CAN_BALANCE_CONTROL, {'TARGET': 'BROADCAST', 'ORIGIN': self.CMU.objectId, 'activeBalancingControl':False})
                    self.globalData.env.process(self.CMU.canBus.CANsend(message))
#                     print("Disabled Balancing! at {}".format(self.globalData.env.now))
#                     print("MinCellSOC: {:.3f}".format(min(self.CMU.socList)))
#                     print("MaxCellSOC: {:.3f}".format(max(self.CMU.socList)))
#                     print("SOC diff: {:.4f}".format(abs(min(self.CMU.socList) - max(self.CMU.socList))))
                
                if ( self.globalData.OptionMaxTest and self.enabled and (self.globalData.env.now >= 1800) ):
                    message = CANmessage(self.CMU.canBus.CAN_BALANCE_CONTROL, {'TARGET': 'BROADCAST', 'ORIGIN': self.CMU.objectId, 'activeBalancingControl':False})
                    self.globalData.env.process(self.CMU.canBus.CANsend(message))
                    
            yield self.CMU.env.timeout(monitoringPause)

    #for non-neighbour strategy NNAdapt

    '''find possible transfer partners, returns transfer with highest urgency in subset of idle cells'''
    def findPartners(self, SOCList, startID, endID, maxRange):
        partnerlist = []
        nmbCells = len(SOCList)

        #average SOC of the whole battery pack
        SOCave = sum(SOCList)/len(SOCList)

        #list of only idle SOCs
        SOCidle = SOCList[startID:endID+1]

        for k in range(0, len(SOCidle)):
            if(SOCidle[k]>SOCave):
                direction = self.getdirection(SOCList=SOCList, ID=k+startID)
                recv = self.findReceivers(SOCList=SOCidle, SOCave=SOCave, ID=k, direction=direction, maxRange=maxRange)
                for item in range(0,len(recv)):
                    partner = (k+startID,recv[item]+startID, abs(recv[item]-k), SOCidle[k]-SOCidle[recv[item]])
                    partnerlist.append(partner)
        sortedList = sorted(partnerlist, key=lambda tup: tup[3], reverse=True)

        '''if there is no transfer'''
        if len(sortedList) == 0:
            return 0
        else:
            return sortedList[0]

    '''returns list of possible receivers for direction and ID of cell'''
    def findReceivers(self, SOCList, SOCave, ID, direction, maxRange):
        recv = []

        '''calculate limits to not break list index'''
        lwbrd, upbrd =  0, 0
        if ID - maxRange < 0:
            lwbrd = abs(ID - maxRange)
        if ID + maxRange > len(SOCList)-1:
            upbrd = abs(ID + maxRange - len(SOCList)+1)

        '''find receivers in dependence of charge direction'''
        '''
        if(direction > 0):
            for k in range(1, maxRange-upbrd+1):
                if(SOCList[ID+k]<SOCave):
                    recv.append(ID+k)
        else:
            for k in range(1, maxRange-lwbrd+1):
                if(SOCList[ID-k]<SOCave):
                    recv.append(ID-k)
        '''
        for k in range(1, maxRange-upbrd+1):
            if(SOCList[ID+k]<SOCave):
                recv.append(ID+k)
        for k in range(1, maxRange-lwbrd+1):
            if(SOCList[ID-k]<SOCave):
                recv.append(ID-k)
        
        return recv

    '''returns 0 if charge needs to be transferred to the left, function currently not used'''
    def getdirection(self, SOCList, ID):
        nmbCells = len(SOCList)

        if(ID==0):
            return 1
        if(ID==nmbCells-1):
            return 0

        avgLeft = sum(SOCList[0:ID]) / len(SOCList[0:ID])
        avgRight = sum(SOCList[ID+1:nmbCells]) / len(SOCList[ID+1:nmbCells])

        if(avgLeft<avgRight):
            return 0
        else:
            return 1

    '''returns efficiency of a charge transfer in dependence of socs of transmitter t and receiver r'''
    def calc_efficiency(self, soctb, socrb, socte, socre):
        voltBound = 3.4
        voltBound2 = 4.16
        Qfull = 24 * (28568 * 0.25)
        voltSlope = (voltBound2-voltBound)/Qfull

        '''calculate voltages of cells'''
        Vtb = voltBound + Qfull * soctb * voltSlope
        Vte = voltBound + Qfull * socte * voltSlope
        Vrb = voltBound + Qfull * socrb * voltSlope
        Vre = voltBound + Qfull * socre * voltSlope

        '''calculate energies'''
        deltaEt = (Vtb**2 - voltBound**2) / (2*voltSlope) - (Vte**2 - voltBound**2) / (2*voltSlope)
        deltaEr = (Vre**2 - voltBound**2) / (2*voltSlope) - ((Vrb**2 - voltBound**2) / (2*voltSlope))

        return deltaEr/deltaEt

"""
==============================================================
Run multiple simulations while iterating over specified values
==============================================================

**file** : BatchSimulate.py

**date** : 3-July-2015
"""

from .GlobalData import GlobalData
from .BatteryPack import BatteryPack
from .FlowControl import FlowControl
from .SaveData import SaveData
import time
import random
from PyQt5 import QtGui, Qt
import os
from .ActiveBalancing import ActiveBalancing
import inspect
import re
from math import ceil
import numpy as np
import sys
from smartcellsimulator.scsimu.BalancingStrategies import BalancingStrategies

class BatchSimulate():
    """
    Class for simulating multiple scenarios while varying a parameter of the pack.
    
    """
    def __init__(self, batchSettings=None, packSettings=None, balancingSettings=None, paramsDict=None):
        if len(sys.argv)>1: 
            self.batchCounter=int(sys.argv[1])
            self.dofor=False
        else:
            self.batchCounter = 0
            self.dofor=True
        self.app = None
        self.stratDict = self.getStratDict()
        self.paramsList = []
        self.batchGlobalDataList = []
        self.dataSaver = SaveData()
        
        if batchSettings is not None:
            self.batchSettings = batchSettings
        else:
            self.batchSettings = {'seedStart': 1000, 'seedEnd': 1000, 'cellsStart': 96, 'cellsEnd': 96, 'stopDiff': 0.005, 'folder': None}
            
        if packSettings is not None:
            self.packSettings = packSettings
        else:
            self.packSettings = {"CellCapacity": 171408, "InitialSoc": 0.6, "InitialSocDist": 0.1}
            
        if balancingSettings is not None:
            self.balancingSettings = balancingSettings
        else:
            self.balancingSettings = {"transferTime": 10, "transferRate": 1.2}
        
        if paramsDict is not None:
            self.paramsDict = paramsDict
        else:
            self.paramsDict = paramsDictPrototype
        
        if self.batchSettings['folder'] is None:
            try:
                self.app = QtGui.QApplication([])
                self.batchSettings['folder'] = QtGui.QFileDialog.getExistingDirectory(None, 'Open a folder', '~/', QtGui.QFileDialog.ShowDirsOnly)
            except:
                print('Please start a window-manager or provide folder to save data to.')
        else:
            self.app = Qt.QCoreApplication([])

        globalData = GlobalData()
        if os.path.exists(self.batchSettings['folder']):
            for rangeVar in range(globalData.rangeStart,globalData.rangeEnd+1):
                for seed in range(self.batchSettings['seedStart'],self.batchSettings['seedEnd']+1):
                    for numberOfCells in range(ceil(self.batchSettings['cellsStart']/12),ceil(self.batchSettings['cellsEnd']/12+1)):
                        for currentStrategy in sorted(self.paramsDict.keys()):
                            if currentStrategy in self.stratDict.keys():
                                self.paramsList.append([currentStrategy, numberOfCells*12, seed, rangeVar])
                                     
            if self.dofor:
            
                for batchCounter in range(len(self.paramsList)): 
                    param = self.paramsDict[self.paramsList[batchCounter][0]]
                    param["numberOfCells"] = self.paramsList[batchCounter][1]
                    param["randomSeed"] = self.paramsList[batchCounter][2]
                    param["rangeVar"] = self.paramsList[batchCounter][3]
                
                    self.initBatchParams(param,batchCounter)
            else:
                
                param = self.paramsDict[self.paramsList[self.batchCounter][0]]
                param["numberOfCells"] = self.paramsList[self.batchCounter][1]
                param["randomSeed"] = self.paramsList[self.batchCounter][2]
                param["rangeVar"] = self.paramsList[self.batchCounter][3]
           
                self.initBatchParams(param,self.batchCounter)
            
#             print('BATCHCOUNTER:: {}'.format(self.batchCounter))
#             print("Starting simulation with {} Cells, {} strategy at seed {}".format(param["numberOfCells"], param["OptionStrategy"], param["randomSeed"]))
            
            if self.dofor:
                for loopCounter in range(len(self.paramsList)): 
                    self.runBatchSimulation(loopCounter)
            else:
                self.runBatchSimulation(self.batchCounter)
            
#             if self.batchCounter is (len(self.paramsList)-1):
#                 condenseMetaData(self.batchSettings['seedStart'], self.batchSettings['seedEnd'], self.batchSettings['cellsStart'], self.batchSettings['cellsEnd'], self.batchSettings['folder'])
            self.app.exit()
            sys.exit()
            
        else:
            print("No folder selected - please restart.")
            self.app.exit()
            
        if self.app:
            self.app.exec_()
        
        
    def runBatchSimulation(self, batchCounter):
        #try:
        param = self.paramsDict[self.paramsList[batchCounter][0]]
        print("Starting simulation with {} Cells, {} strategy at seed {}".format(param["numberOfCells"], param["OptionStrategy"], param["randomSeed"]))
        if self.dofor:
            globalData=self.batchGlobalDataList[batchCounter]
        else:
            globalData=self.batchGlobalDataList[0] #list index is always 0, as even insert(2) inserts at position 0 if no other elements present
        while not globalData.stopSimulation:
            globalData.env.run(until=globalData.env.now+globalData.minTimeStep)
            #print("Current time: "+str(globalData.env.now))
            
        #finally:
        #    if not globalData.stopSimulation:
                #QtCore.QTimer.singleShot(0, self.runBatchSimulation)
        #    else:
                
        print("Simulation #{} of {} done -> saving data...".format(batchCounter+1, len(self.paramsList)))
        #param = self.paramsDict[self.paramsList[batchCounter][0]]
        param["numberOfCells"] = self.paramsList[batchCounter][1]
        param["randomSeed"] = self.paramsList[batchCounter][2]
        subfolder = "/{}_CELLS".format(param["numberOfCells"])
        file = "/{}_S{}.csv".format(param["OptionStrategy"], param["randomSeed"])
        
        if not os.path.exists(self.batchSettings['folder']+subfolder):
            os.makedirs(self.batchSettings['folder']+subfolder)
        self.dataSaver.saveData(self.batchSettings['folder']+subfolder+file, globalData, True, True, 14500)
        print("Done saving data")
                
                
#                 if self.batchCounter < len(self.paramsList)-1:
#                     self.batchCounter+=1
#                     param = self.paramsDict[self.paramsList[self.batchCounter][0]]
#                     param["numberOfCells"] = self.paramsList[self.batchCounter][1]
#                     param["randomSeed"] = self.paramsList[self.batchCounter][2]
#                     self.initBatchParams(param)
#                     print("\n********************\n")
#                     print("Starting simulation with {} Cells, {} strategy at seed {}".format(param["numberOfCells"], param["OptionStrategy"], param["randomSeed"]))
#                     #self.runBatchSimulation()
#                 else:
#                     condenseMetaData(self.batchSettings['seedStart'], self.batchSettings['seedEnd'], self.batchSettings['cellsStart'], self.batchSettings['cellsEnd'], self.batchSettings['folder'])
#                     print("BatchSimulation done.")
#                     self.app.exit()
                    
    def initBatchParams(self, params, batchCounter):
        """
        Creates a GlobalData for the current simulation and initializes it with the corresponding values.
        
        Args:
            params : List of parameters for the simulation
            batchCounter : number of the batch that is currently processed
        """
        self.globalData = GlobalData()
        self.globalData.window = batchWindow()
        
        self.globalData.starttime=time.time()
        
        self.globalData.OptionRandomSeed = params["randomSeed"]
        random.seed(self.globalData.OptionRandomSeed)
                
        self.globalData.numberOfCells  = params["numberOfCells"]
        self.globalData.cellCapacity   = self.packSettings["CellCapacity"]
        self.globalData.initialSoc     = self.packSettings["InitialSoc"]
        self.globalData.initialSocDist = self.packSettings["InitialSocDist"]
        
        self.globalData.transferTime = self.balancingSettings["transferTime"]
        self.globalData.transferRate = self.balancingSettings["transferRate"]
        
        self.globalData.OptionStopDiff = self.batchSettings['stopDiff']
        
        self.globalData.OptionStrategy =  self.stratDict[params["OptionStrategy"]]
        self.globalData.OptionTimebasedBC = params["OptionTimebasedBC"]
        self.globalData.OptionMinBC = params["OptionMinBC"]
        self.globalData.OptionMaxBC = params["OptionMaxBC"]
        self.globalData.OptionBCperiod = params["OptionBCperiod"]
        self.globalData.OptionLimitTimebasedBC = params["OptionLimitTimebasedBC"]
        self.globalData.OptionLimitBound = params["OptionLimitBound"]/100
        self.globalData.OptionConcurrent = params["OptionConcurrent"]
        self.globalData.OptionConcurrencyDeg = params["OptionConcurrencyDeg"]
        self.globalData.OptionBCAfterTransaction = params["OptionBCAfterTransaction"]

        self.globalData.rangeValue = params["rangeVar"]
        
        self.globalData.barColors = ["#666666" for _ in range(self.globalData.numberOfCells)]
        self.globalData.simDuration = 1000000
        self.globalData.minTimeStep = self.globalData.transferTime
        self.globalData.livePlot = False
        self.globalData.barChartLive = False
        self.globalData.CANchart = True
        
        self.globalData.monitor = Monitor(self.globalData,sampleTime=self.globalData.minTimeStep,refreshTime=self.globalData.numberOfCells*5,livePlot=self.globalData.livePlot)        
        #self.globalData.pack = BatteryPack(self.globalData,0.0,self.globalData.cellCapacity*3600.0,0.1) Old structure
        self.globalData.pack = BatteryPack(self.globalData) #now archBuilder takes care of this
        self.globalData.env.process(self.globalData.pack.supplyLoad.applyLoad())
        FlowControl(self.globalData)
        print("saved globaldata dor batchCounter "+str(batchCounter))
        self.batchGlobalDataList.insert(batchCounter,self.globalData)
        
        
    def getStratDict(self):
        """
        Build a dictionary containing the active balancing strategy request- and acknowledge-functions.        
        """
        membs = inspect.getmembers(BalancingStrategies, predicate=inspect.isfunction)        
        strat_dict = {}
        for memb in membs:
                
            m = re.search(r'AckStr_(?P<strat>\w+)', memb[0])
            if m:
                strat_name = m.group('strat')
                if strat_name in strat_dict:
                    strat_dict[strat_name]['Ack'] = memb[1]
                else:
                    strat_dict[strat_name] = {'Ack': memb[1]}
        
            m = re.search(r'ReqStr_(?P<strat>\w+)', memb[0])
            if m:
                strat_name = m.group('strat')
                if strat_name in strat_dict:
                    strat_dict[strat_name]['Req'] = memb[1]
                    strat_dict[strat_name]['Name'] = strat_name
                else:
                    strat_dict[strat_name] = {'Req': memb[1]}
        return strat_dict
        

paramsDictPrototype={
'Minimum':{
 "numberOfCells":           12, 
 "randomSeed":              1000,
 "OptionStrategy":          'Minimum',
 "OptionTimebasedBC":       False,
 "OptionMinBC":             True,             
 "OptionMaxBC":             True,
 "OptionBCperiod":          1,
 "OptionLimitTimebasedBC":  False,
 "OptionLimitBound":        0.01,
 "OptionConcurrent":        False,
 "OptionConcurrencyDeg":    1,
 "OptionBCAfterTransaction":True  
 },
'Maximum':{
 "numberOfCells":           12, 
 "randomSeed":              1000,
 "OptionStrategy":          'Maximum',
 "OptionTimebasedBC":       False,
 "OptionMinBC":             False,             
 "OptionMaxBC":             False,
 "OptionBCperiod":          1,
 "OptionLimitTimebasedBC":  False,
 "OptionLimitBound":        0.01,
 "OptionConcurrent":        False,
 "OptionConcurrencyDeg":    1,
 "OptionBCAfterTransaction":True  
 },
'MinMax':{
 "numberOfCells":           12, 
 "randomSeed":              1000,
 "OptionStrategy":          'MinMax',
 "OptionTimebasedBC":       False,
 "OptionMinBC":             True,             
 "OptionMaxBC":             True,
 "OptionBCperiod":          1,
 "OptionLimitTimebasedBC":  False,
 "OptionLimitBound":        0.01,
 "OptionConcurrent":        False,
 "OptionConcurrencyDeg":    1,
 "OptionBCAfterTransaction":True  
 },
'BelowAverage':{
 "numberOfCells":           12, 
 "randomSeed":              1000,
 "OptionStrategy":          'BelowAverage',
 "OptionTimebasedBC":       True,
 "OptionMinBC":             False,             
 "OptionMaxBC":             False,
 "OptionBCperiod":          1,
 "OptionLimitTimebasedBC":  False,
 "OptionLimitBound":        0.01,
 "OptionConcurrent":        False,
 "OptionConcurrencyDeg":    1,
 "OptionBCAfterTransaction":False  
 }}

class listWidget:
    """
    Class for reproducing the features of the GUI list used for the simulator
    """
    def __init__(self):
        self.name = 'listWidget'
    def addItem(self, message):
        print(message)
    def scrollToBottom(self):
        pass
        
class batchWindow:
    """
    Class for reproducing windows functionality to batch-processing.
    """
    def __init__(self):
        self.listWidget = listWidget()

class plotDataContainer:
    """
    Virtual graph-plotter for saving simulation data in batch-processing.
    """
    def __init__(self, numberOfCurves, title=None, unit=None):
        self.numberOfCurves = numberOfCurves
        self.unit = unit
        self.title = title
        self.datax = None
        self.datay = []
    
    def appendData(self,valuesx,valuesy):
        """
        Add data to list of values to plot.
        
        Args:
            valuesx : x-axis values to add
            valuesy : y-axis values to add
        """
        if self.datax is None:
            self.datax=np.array(valuesx,ndmin=1)
            for i in range(self.numberOfCurves):
                self.datay.append(np.array(valuesy[i],ndmin=1))
        else:
            self.datax=np.append(self.datax,valuesx)
            for i in range(self.numberOfCurves):
                self.datay[i]=np.append(self.datay[i],valuesy[i]) 

class Monitor(object):
    """
    Class for replicating functionality of monitor-class for batch-processing.    
    """
    def __init__(self, globalData,sampleTime,refreshTime,livePlot):
        self.globalData = globalData
        self.env = globalData.env
        self.updateTime = sampleTime
        self.refreshTime = refreshTime
        self.currentDiff = 0
        if self.updateTime > 0:
            self.socPlot=plotDataContainer(self.globalData.numberOfCells)
            if self.globalData.CANchart:
                self.canMessagePlot = plotDataContainer(1, unit='')
                self.canFreqPlot = plotDataContainer(2, unit='1/s')
                self.canDataPlot = plotDataContainer(1, unit='kByte')
                self.canSpeedPlot = plotDataContainer(2, unit='kBit/s')
                self.globalData.env.process(self.canMessageMonitor())
        self.globalData.env.process(self.cellSOCMonitor())
        self.globalData.env.process(self.timeMonitor())
       
    def cellSOCMonitor(self):
        yield self.env.timeout(1)
        if self.updateTime > 0:            
            while True:
                socArray = []
                for smartCell in self.globalData.pack.smartCells:
                    socArray.append(smartCell.cell.getSoc())
                self.socPlot.appendData(self.env.now,socArray)
                self.currentDiff = abs(min(socArray) - max(socArray))
                yield self.env.timeout(self.updateTime)             
    
    def canMessageMonitor(self):
        if self.updateTime > 0:
            while True:
                self.canMessagePlot.appendData(self.env.now, [self.globalData.canBus.messageCount])
                self.canDataPlot.appendData(self.env.now, [self.globalData.canBus.dataCount/8/1000])
                self.canFreqPlot.appendData(self.env.now, [self.globalData.canBus.freq, self.globalData.canBus.avgFreq])
                self.canSpeedPlot.appendData(self.env.now, [self.globalData.canBus.speed/1000, self.globalData.canBus.avgSpeed/1000])
                yield self.env.timeout(self.updateTime)
                
    def timeMonitor(self):
        while True:
#             sys.stdout.write('\rSimulation time: {}'.format(self.env.now))
#             self.globalData.guiPrint('SocDiff: {}'.format(self.currentDiff))
            if (self.env.now > 0):
                progress = (1-(self.currentDiff/(self.globalData.initialSocDist-self.globalData.OptionStopDiff)))*100
                sys.stdout.write('\rProgress: {0:.2f} %  at simulation time {1} s'.format(progress, self.env.now))
            #gc.collect()
            #objgraph.show_growth()
            yield self.env.timeout(1000)
                
    def refreshViewForce(self):
        pass     
    def eventHandler(self):
        pass

if ( __name__ == '__main__' ):
    batchSimulator = BatchSimulate(folder='C:/batchsimulation/')
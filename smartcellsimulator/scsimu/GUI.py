"""
========================================================
Functionality and appearance of graphical user interface
========================================================

**file** : GUI.py

**date** : 3-July-2015
"""

import PyQt5.uic
# if eclipse generates import errors for PyQt4, add (click new, type PyQt4) it to the forced builtins
# http://stackoverflow.com/questions/8082230/pyqt4-names-showing-as-undefined-in-eclipse-but-it-runs-fine
import random
import time
import os
from PyQt5 import QtGui, QtCore
from .GlobalData import GlobalData
from .BatteryPack import BatteryPack
from .Monitor import Monitor
from .FlowControl import FlowControl
from .SettingsDialog import CurveFollowSettingsDialog, EvaluationSettingsDialog
from .SaveData import SaveData
import inspect
from smartcellsimulator.scsimu.BalancingStrategies import BalancingStrategies
import re
from smartcellsimulator.scsimu.SettingsDialog import CondenseMetadataSettingsDialog,\
    SaveScatterplotSettingsDialog
from configparser import SafeConfigParser


def runSimulation():
    try:
        globalData = myGlobal
        globalData.env.run(until=globalData.env.now+globalData.minTimeStep)
    finally:
        if not globalData.stopSimulation:
            QtCore.QTimer.singleShot(0, runSimulation)


class MainWindow(QtGui.QMainWindow):
    """
    Class inheriting QMainWindow for displaying a graphical user interface to control behavior and settings of the simulation.   
    """

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)

        # load the ui (path acrobatic is necessary to make this work when project is packaged
        print(os.getcwd())
        ui_fname = 'mainwindow.ui'
        if(not os.path.exists(ui_fname)):
            ui_fname = os.path.join('smartcellsimulator/qt/', ui_fname)
        if(not os.path.exists(ui_fname)):
            ui_fname = os.path.join('../', ui_fname)

        print(ui_fname)
        PyQt5.uic.loadUi(ui_fname, self)

        self.globalData = None
        self.dataSaver = SaveData()

        self.followCurveDataFile = None
        self.curveFollowSettings = {
            "currentDivider": 12, "cycleRepetitions": 1}
        self.evaluationSettings = {"NumberOfCells": 5, "CellCapacity": 1.1, "socCell0": 0.9999,
                                   "socCell1": 0.9999, "socCell2": 0.9999, "socCell3": 0.857, "socCell4": 0.905, "OCVlatency": 5.0}

        self.strat_dict = self.getStratDict()
        for key in sorted(self.strat_dict):
            self.cmbStrategy.addItem(key)

    def getStratDict(self):
        """
        Collect active balancing strategies and their function-pointers to be displayed in the GUI.        
        """
        membs = inspect.getmembers(
            BalancingStrategies, predicate=inspect.isfunction)
        strat_dict = {}
        for memb in membs:

            m = re.search(r'AckStr_(?P<strat>\w+)', memb[0])
            if m:
                strat_name = m.group('strat')
                if strat_name in strat_dict:
                    strat_dict[strat_name]['Ack'] = memb[1]
                else:
                    strat_dict[strat_name] = {'Ack': memb[1]}

            m = re.search(r'ReqStr_(?P<strat>\w+)', memb[0])
            if m:
                strat_name = m.group('strat')
                if strat_name in strat_dict:
                    strat_dict[strat_name]['Req'] = memb[1]
                    strat_dict[strat_name]['Name'] = strat_name
                else:
                    strat_dict[strat_name] = {'Req': memb[1]}

        return strat_dict

    def dataInit(self):
        """
        Instanciates and initializes the required globalData file for simulation with the given values from the GUI.
        """

        self.globalData = GlobalData()
        self.globalData.window = self
        self.globalData.starttime = time.time()

        # Set by GUI elements
        self.globalData.OptionRandomSeed = self.spbSeed.value()
        random.seed(self.globalData.OptionRandomSeed)
        self.globalData.numberOfCells = self.spbNumberOfCells.value()
        # set concurrency degree fixed to half of the number of cells (relevant for min, max, minmax)
        self.globalData.OptionConcurrencyDeg = int(
            self.globalData.numberOfCells/2)
        #self.globalData.OptionConcurrencyDeg = self.spbConcurrencyDeg.value()
        self.globalData.OptionConcurrent = self.chbConcurrent.isChecked()

        self.globalData.livePlot = self.chbLivePlot.isChecked()
        self.globalData.CANchart = self.chbCanPlot.isChecked()
        self.globalData.OptionStrategy = self.strat_dict[self.cmbStrategy.currentText(
        )]
        self.globalData.OptionTimebasedBC = self.chbTimebasedBC.isChecked()
        self.globalData.OptionBCperiod = self.spbBCPeriod.value()

        self.globalData.OptionStopDiff = self.spbStopDiff.value()/100

        # Set by ini file
        settingsFile = 'settings.ini'
        if(not os.path.exists(settingsFile)):
            settingsFile = os.path.join('smartcellsimulator/', settingsFile)
        if(not os.path.exists(settingsFile)):
            settingsFile = os.path.join('../', settingsFile)

        parser = SafeConfigParser()
        parser.read(settingsFile)
        print("Loaded *.ini file: ", settingsFile)

        if parser.has_option('balancing_settings', 'transferTime'):
            self.globalData.transferTime = float(
                parser.get('balancing_settings', 'transferTime'))
        else:
            self.globalData.transferTime = 60

        if parser.has_option('balancing_settings', 'transferRate'):
            self.globalData.transferRate = float(
                parser.get('balancing_settings', 'transferRate'))
        else:
            self.globalData.transferRate = 1.2

        if parser.has_option('pack_settings', 'CellCapacity'):
            self.globalData.cellCapacity = float(
                parser.get('pack_settings', 'CellCapacity'))
        else:
            self.globalData.cellCapacity = 9000

        if parser.has_option('pack_settings', 'InitialSoc'):
            self.globalData.initialSoc = float(
                parser.get('pack_settings', 'InitialSoc'))
        else:
            self.globalData.initialSoc = 0.5

        if parser.has_option('pack_settings', 'InitialSocDist'):
            self.globalData.initialSocDist = float(
                parser.get('pack_settings', 'InitialSocDist'))
        else:
            self.globalData.initialSocDist = 0.05

        # Set standard values
        if parser.has_option('broadcast_settings', 'MinBC'):
            self.globalData.OptionMinBC = bool(
                parser.get('broadcast_settings', 'MinBC'))
        elif ((self.cmbStrategy.currentText() == 'Minimum') or (self.cmbStrategy.currentText() == 'MinMax')):
            self.globalData.OptionMinBC = True
        else:
            self.globalData.OptionMinBC = False

        if parser.has_option('broadcast_settings', 'MaxBC'):
            self.globalData.OptionMinBC = bool(
                parser.get('broadcast_settings', 'MaxBC'))
        elif ((self.cmbStrategy.currentText() == 'Maximum') or (self.cmbStrategy.currentText() == 'MinMax')):
            self.globalData.OptionMaxBC = True
        else:
            self.globalData.OptionMaxBC = False

        if parser.has_option('broadcast_settings', 'BCAfterTransaction'):
            self.globalData.OptionBCAfterTransaction = bool(
                parser.get('broadcast_settings', 'BCAfterTransaction'))
        # elif ( (self.cmbStrategy.currentText() == 'Minimum') or (self.cmbStrategy.currentText() == 'Maximum') or (self.cmbStrategy.currentText() == 'MinMax') ):
            self.globalData.OptionBCAfterTransaction = True
        else:
            self.globalData.OptionBCAfterTransaction = False

        if parser.has_option('broadcast_settings', 'LimitBound'):
            self.globalData.OptionLimitTimebasedBC = True
            self.globalData.OptionLimitBound = float(
                parser.get('broadcast_settings', 'LimitBound'))
        else:
            self.globalData.OptionLimitTimebasedBC = False

        if parser.has_option('broadcast_settings', 'StartDiff'):
            self.globalData.OptionStartDiff = float(
                parser.get('broadcast_settings', 'StartDiff'))
        else:
            self.globalData.OptionStartDiff = 0.01

        #
        self.globalData.OptionDiffPlot = False
        self.globalData.barChartLive = False

        # Set by Dialog
        self.globalData.followCurveCurrentDivider = self.curveFollowSettings["currentDivider"]
        self.globalData.followCurveCycleRepetitions = self.curveFollowSettings[
            "cycleRepetitions"]
        self.globalData.evaluationSettings = self.evaluationSettings
        self.globalData.OptionFollowCurve = self.acnFollowCurve.isChecked()
        self.globalData.followCurveDataFile = self.followCurveDataFile

        self.globalData.barColors = [
            "#666666" for _ in range(self.globalData.numberOfCells)]
        self.globalData.simDuration = 1000000
        self.globalData.minTimeStep = 1  # self.globalData.transferTime

        # Initialize Monitor and BatteryPack
        self.globalData.guiPrint('Smart Cell Simulation')

        self.globalData.monitor = Monitor(
            self.globalData, sampleTime=self.globalData.minTimeStep, refreshTime=self.globalData.numberOfCells*5, livePlot=True)
        self.globalData.pack = BatteryPack(self.globalData)

        FlowControl(self.globalData)

        global myGlobal
        myGlobal = self.globalData

    def startSim_clicked(self):
        self.dataInit()
        runSimulation()

    def saveData(self, file=None):
        if (not file):
            file = QtGui.QFileDialog.getSaveFileName(
                self, 'Save as...', '~/', filter='*.csv')
        self.dataSaver.saveData(file, self.globalData,
                                self.globalData.CANchart, True, 15000)

    def followCurve(self, file=None):
        file = "../_loadcycle/SgCycle_Power_kW.csv"
        if not os.path.exists(file):
            file = QtGui.QFileDialog.getOpenFileName(
                self, 'Open Loadcycle...', '~/', filter='*.csv')

        print(file)

        if os.path.exists(file):
            self.acnFollowCurve.setChecked(True)
            self.followCurveDataFile = file
        else:
            self.acnFollowCurve.setChecked(False)
            self.followCurveDataFile = None

    def setCurveFollowSettings(self):
        curveFollowSettings = CurveFollowSettingsDialog.getCurveFollowSettings(
            self, curveFollowSettings=self.curveFollowSettings)
        if curveFollowSettings["Accepted"]:
            self.curveFollowSettings = curveFollowSettings

    def evaluateDemonstrator(self):
        evaluationSettings = EvaluationSettingsDialog.getEvaluationSettings(
            self, self.evaluationSettings)

        if evaluationSettings["Accepted"]:
            self.acnEvaluateDemonstrator.setChecked(True)
            self.evaluationSettings = evaluationSettings
            self.packSettings["NumberOfCells"] = self.evaluationSettings["NumberOfCells"]
            self.packSettings["CellCapacity"] = self.evaluationSettings["CellCapacity"]
        else:
            self.acnEvaluateDemonstrator.setChecked(False)

    def setEvaluationSettings(self):
        evaluationSettings = EvaluationSettingsDialog.getEvaluationSettings(
            self, self.evaluationSettings)
        if evaluationSettings["Accepted"]:
            self.evaluationSettings = evaluationSettings
            self.packSettings["NumberOfCells"] = self.evaluationSettings["NumberOfCells"]
            self.packSettings["CellCapacity"] = self.evaluationSettings["CellCapacity"]

    def condenseMetadata(self):
        condenseMetadataSettings = CondenseMetadataSettingsDialog.getCondenseMetadataSettings(
            self)
        if condenseMetadataSettings['Accepted']:
            seedStart = condenseMetadataSettings['seedStart']
            seedEnd = condenseMetadataSettings['seedEnd']
            cellsStart = condenseMetadataSettings['cellsStart']
            cellsEnd = condenseMetadataSettings['cellsEnd']
            folder = condenseMetadataSettings['folder']
            saveDiodeData = condenseMetadataSettings['saveDiodeData']
            strategies = condenseMetadataSettings['strategies']
            self.dataSaver.condenseMetaData(
                seedStart, seedEnd, cellsStart, cellsEnd, folder, strategies=strategies, saveDiodeData=saveDiodeData)
#             self.dataSaver.saveDiodeData(seedStart, seedEnd, cellsStart, cellsEnd, folder, strategies=strategies)

    def saveScatterplot(self):
        saveScatterplotSettings = SaveScatterplotSettingsDialog.getSaveScatterplotSettings(
            self)
        if saveScatterplotSettings['Accepted']:
            xRow = saveScatterplotSettings['xAxis']
            yRow = saveScatterplotSettings['yAxis']
            folder = saveScatterplotSettings['folder']
            self.dataSaver.saveScatterPlotFile(folder, xRow, yRow)

# Note that running this (probably) won't work anymore with the new directory structure; run bin/startGUI.py instead
# suggestion: change eclipse to start the last run when pressing F11 for convenience

#  if ( __name__ == '__main__' ):
#      app = None
#      if ( not app ):
#          app = QtGui.QApplication([])
#
#      window = MainWindow()
#      #QtCore.QObject.connect(self.ui.pushButton, QtCore.SIGNAL("clicked()"), startSim_clicked)
#
#      window.connect(window.pushButton, QtCore.SIGNAL("clicked()"), startSim_clicked)
#      window.connect(window.btnSaveData, QtCore.SIGNAL("clicked()"), saveData)
#      window.connect(window.btnBatch, QtCore.SIGNAL("clicked()"), batchSimulate)
#      window.show()
#
#      if ( app ):
#          app.exec_()

from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import sys

class LiveplotWindow:
    class __newPlot:
        def __init__(self, liveplotWindow,numberOfCurves, plotTitle="LivePlot",legendName="Curve"):
            if len(liveplotWindow.plots) % liveplotWindow.plotsPerRow == 0:
                liveplotWindow.nextRow()
            self.liveplotWindow=liveplotWindow
            self.datax = None
            self.datay = []
            self.numberOfCurves=numberOfCurves
            self.legendName=legendName
            self.isBar=False
            self.barColors=None
            self.plot = self.liveplotWindow.win.addPlot(title=plotTitle)
            self.legend=self.plot.addLegend(offset=-50)
            self.legend.setParentItem(self.plot)
            self.legendDefined=False
            self.plot.showGrid(True,True,0.5)
            #self.livecheck=liveplotWindow.livecheck
            #self.barcheck=liveplotWindow.barcheck
            
            #self.legend.addItem(item=self.plot.plot[0], name="test")  
              
            
            
            
        def appendData(self,valuesx,valuesy):
            #self.liveplotWindow.globalData.livePlot=self.liveplotWindow.livecheck.isChecked()
            #self.liveplotWindow.livecheck.setChecked(self.liveplotWindow.globalData.livePlot) 
            #self.liveplotWindow.globalData.barChartLive=self.liveplotWindow.barcheck.isChecked()
            #self.liveplotWindow.barcheck.setChecked(self.liveplotWindow.globalData.barChartLive)       
            if self.datax is None:
                self.datax=np.array(valuesx,ndmin=1)
                for i in range(self.numberOfCurves):
                    self.datay.append(np.array(valuesy[i],ndmin=1))
                
            else:
                self.datax=np.append(self.datax,valuesx)
                for i in range(self.numberOfCurves):
                    self.datay[i]=np.append(self.datay[i],valuesy[i]) 
                   
                    
        def setBarData(self,valuesx,valuesy):
            self.isBar=True
            if self.datax is None:
                self.datax=[]
               
                for i in range(self.numberOfCurves):
                    self.datax.append(np.array(valuesx[i],ndmin=1))
                
                for i in range(self.numberOfCurves):
                    self.datay.append(np.array(valuesy[i],ndmin=1))    
            else:    
                for i in range(self.numberOfCurves):
                    self.datax[i]=np.array(valuesx[i],ndmin=1)
                    self.datay[i]=np.array(valuesy[i],ndmin=1)
            self.refreshView()
            
        def setBarColor(self,colorList):
            self.barColors=colorList
           
            
                
            
                
        
        def refreshView(self):
            if self.datax is not None:
                self.plot.plot(clear=True)
                
                for i in range(self.numberOfCurves):
                    if self.isBar:
                        #if not self.liveplotWindow.barcheck.isChecked():
                        #    self.plot.clear()
                        #else:
                        myplot=self.plot.plot(self.datax[i],self.datay[i],pen=pg.mkPen(self.barColors[i],width=10))
                        self.plot.plot(self.datax[i],[0,0.1],pen=pg.mkPen(pg.intColor(i,self.numberOfCurves),width=10))
                    else:
                        myplot=self.plot.plot(self.datax,self.datay[i],pen=(i,self.numberOfCurves))
                        if not self.legendDefined:
                            self.legend.addItem(item=myplot, name=self.legendName + " " + str(i))
                self.legendDefined=True  
                
            
            
        
            
    def __init__(self, globalData):
        self.globalData=globalData
        self.win=globalData.window.graphicsView
        #self.win = pg.GraphicsWindow(title=windowTitle)
        #pg.setConfigOptions(antialias=True)
        #self.win.resize(1000,600)
        self.plots=[]
        self.deletePlots()
        self.plotsPerRow = 1
        
       
    def addPlot(self,numberOfCurves,plotTitle,legendName):
        newPlot=self.__newPlot(self,numberOfCurves,plotTitle,legendName)
        self.plots.append(newPlot)
        return newPlot
    
    def deletePlots(self):
        self.win.clear()
        
                           
    def eventHandler(self):
        ## Start Qt event loop unless running in interactive mode or using pyside.
        if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
            QtGui.QApplication.instance().exec_()
       
    def refreshView(self):
        for plot in self.plots:
            #if not plot.isBar:
            plot.refreshView()
            #self.app.processEvents()           
    
    def doEvents(self): 
        self.app.processEvents()
 
        
    def nextRow(self):
        self.win.nextRow()

    

    
def main(): 
    plotWindow=liveplotWindow(globalData,"Live Plots")
    allPlots = plotWindow.plots;
    for counter in range (1):
        plotWindow.addPlot(4,"Plot "+str(counter),"Curve")
        
    for counter in range (10):
        for plot in allPlots:
            plot.appendData(counter/10.0,[counter,counter/2,counter/5,counter/3])
        plotWindow.refreshView()

    plotWindow.eventHandler()

if __name__ == "__main__":
    main()  

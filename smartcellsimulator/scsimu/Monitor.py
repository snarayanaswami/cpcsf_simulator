"""
===========================================================
Module for monitoring values and parameters of a simulation
===========================================================

**file** : Monitor.py

**date** : 3-July-2015
"""

from .liveplotwindow import LiveplotWindow
import math

class Monitor(object):
    def __init__(self, globalData,sampleTime,refreshTime,livePlot):
        self.globalData=globalData
        self.env = globalData.env
        self.updateTime=sampleTime
        self.refreshTime=refreshTime
        self.currentDiff=0

        if self.updateTime>0:            
            self.plotWindow = LiveplotWindow(self.globalData)
           
            #self.voltagePlot=self.plotWindow.addPlot(self.globalData.numberOfCells, "Cell Voltage","Cell")
            
            self.socPlot = self.plotWindow.addPlot(self.globalData.numberOfCells, "Cell SoC","Cell")
            
            if self.globalData.OptionVoltagePlot:
                self.voltagePlot = self.plotWindow.addPlot(self.globalData.numberOfCells, "Cell Voltage", "Cell")
            
            if self.globalData.OptionDiffPlot:
                self.diffPlot = self.plotWindow.addPlot(3, "SoC difference","")
            
            if self.globalData.barChartLive:
                self.socBarPlot = self.plotWindow.addPlot(self.globalData.numberOfCells, "Cell SoC","Cell")
                self.globalData.env.process(self.socBarMonitor())
            
            if self.globalData.CANchart:
                self.canDataPlot = self.plotWindow.addPlot(1, "Kilobytes sent [kByte]","")
                self.canSpeedPlot = self.plotWindow.addPlot(2, "Bandwidth needed [kBit/s]","")
                self.globalData.env.process(self.canMessageMonitor())
            
            
        self.globalData.env.process(self.cellVoltageMonitor())
        
        self.globalData.env.process(self.timeMonitor())
        self.globalData.env.process(self.refreshView())

    def refreshView(self):
        self.env.timeout(self.refreshTime)
        while True:
            if self.globalData.livePlot:
                self.plotWindow.refreshView()
            yield self.env.timeout(self.refreshTime)
    
    def refreshViewForce(self):
        self.plotWindow.refreshView()
        
    def balancingStateMonitor(self):  
        while True:
            for smartCell in self.globalData.pack.smartCells:
                print (smartCell.cmu.balancingModeList)
                yield self.env.timeout(1)    
      
       
    def cellVoltageMonitor(self):  
        yield self.env.timeout(1)
        if self.updateTime>0:            
            while True:
                voltageArray=[]
                socArray=[]
                for smartCell in self.globalData.pack.smartCells:
                    voltageArray.append(smartCell.cell.getVoltage())
                    socArray.append(smartCell.cell.getSoc())
                
                self.currentDiff = abs(min(socArray) - max(socArray))
                self.currentDiff = abs(min(socArray) - max(socArray))
                if self.globalData.OptionDiffPlot:
                    self.diffPlot.appendData(self.env.now,[self.globalData.OptionStartDiff, self.globalData.OptionStopDiff, self.currentDiff])
                if self.globalData.OptionVoltagePlot:
                    self.voltagePlot.appendData(self.env.now, voltageArray)
                self.socPlot.appendData(self.env.now,socArray)
                
                yield self.env.timeout(self.updateTime)
             
    def socBarMonitor(self):  
        if self.updateTime>0:
                while True:
                    if self.globalData.barChartLive:
                        idArray=[]
                        socArray=[]
                        for smartCell in self.globalData.pack.smartCells:
                            socArray.append([0,smartCell.cell.getSoc()])
                            idArray.append([smartCell.objectId,smartCell.objectId])
                        
                        self.socBarPlot.setBarColor(self.globalData.barColors)
                        self.socBarPlot.setBarData(idArray,socArray)
                    
                    yield self.env.timeout(self.refreshTime)
                
             
    
    def canMessageMonitor(self):
        if self.updateTime>0:
            while True:                
                self.canDataPlot.appendData(self.env.now,[self.globalData.canBus.dataCount/8/1000])
                self.canSpeedPlot.appendData(self.env.now, [self.globalData.canBus.speed/1000, self.globalData.canBus.avgSpeed/1000])
                
                yield self.env.timeout(self.updateTime)
                
                
    def timeMonitor(self):
        while True:
            self.globalData.guiPrint("Simulation time: "+str(self.env.now))
            progress = (1-(self.currentDiff/(self.globalData.initialSocDist-self.globalData.OptionStopDiff)))*100
            self.globalData.guiPrint('Progress: {0:.2f} %'.format(progress))

            yield self.env.timeout(500)
                
        
                
    def eventHandler(self):
        if self.updateTime>0:
            #if not self.livePlot:
            self.plotWindow.refreshView()
            #self.globalData.plotWindow.eventHandler()
            self.plotWindow.eventHandler()

"""
===================================================
Structure being passed for inter-cell communication
===================================================

**file** :  CANmessage.py

**date** : 3-July-2015
"""

class CANmessage(object):
    def __init__(self,identifier,data):
        self.identifier = identifier
        self.data = data
        self.queueTime = None
        self.transmitTime = None
        self.receiveTime = None
'''
Created on 10 Feb, 2015

@author: matthias
'''

import inspect
from smartcellsimulator.scsimu.ActiveBalancing import ActiveBalancing
import re
# from collections import namedtuple
# strat_tuple = namedtuple('strat_tuple', ['Req', 'Ack'])

def run():
    membs = inspect.getmembers(ActiveBalancing, predicate=inspect.isfunction)
    print(membs)
    
    strat_dict = {}
    for memb in membs:
        m = re.search(r'_AckStr_(?P<strat>\w+)', memb[0])
        if m:
            strat_name = m.group('strat')
            if strat_name in strat_dict:
                strat_dict[strat_name]['Ack'] = memb[1]
            else:
                 strat_dict[strat_name] = {'Ack': memb[1]}
    
        m = re.search(r'_ReqStr_(?P<strat>\w+)', memb[0])
        if m:
            strat_name = m.group('strat')
            if strat_name in strat_dict:
                strat_dict[strat_name]['Req'] = memb[1]
            else:
                 strat_dict[strat_name] = {'Req': memb[1]}
    
    print(strat_dict)
        
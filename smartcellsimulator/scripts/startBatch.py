import platform
from smartcellsimulator.scsimu.BatchSimulate import BatchSimulate
import os

def run():
    #Configuration for batchprocessing


    batchSettings = {'seedStart': 1, 'seedEnd': 100, 'cellsStart': 96, 'cellsEnd': 96, 'stopDiff': 0.002, 'folder': None}
    packSettings = {'CellCapacity': 171408, 'InitialSoc': 0.6, 'InitialSocDist': 0.1}
    balancingSettings = {'transferTime': 30, 'transferRate': 12}

    batchSettings['folder'] = os.path.expanduser('~/simulations/')
    
    paramsDict={
    '_Minimum':{
     "numberOfCells":           12,
     "randomSeed":              1000,
     "OptionStrategy":          'Minimum',
     "OptionTimebasedBC":       False,
     "OptionMinBC":             True,
     "OptionMaxBC":             False,
     "OptionBCperiod":          15,
     "OptionLimitTimebasedBC":  False,
     "OptionLimitBound":        0.01,
     "OptionConcurrent":        True,
     "OptionConcurrencyDeg":    48,
     "OptionBCAfterTransaction":True  
     },
    '_Maximum':{
     "numberOfCells":           96,
     "randomSeed":              1000,
     "OptionStrategy":          'Maximum',
     "OptionTimebasedBC":       False,
     "OptionMinBC":             False,             
     "OptionMaxBC":             True,
     "OptionBCperiod":          15,
     "OptionLimitTimebasedBC":  False,
     "OptionLimitBound":        0.01,
     "OptionConcurrent":        True,
     "OptionConcurrencyDeg":    48,
     "OptionBCAfterTransaction":True  
     },
    '_MinMax':{
     "numberOfCells":           96,
     "randomSeed":              1000,
     "OptionStrategy":          'MinMax',
     "OptionTimebasedBC":       False,
     "OptionMinBC":             True,             
     "OptionMaxBC":             True,
     "OptionBCperiod":          15,
     "OptionLimitTimebasedBC":  False,
     "OptionLimitBound":        0.01,
     "OptionConcurrent":        True,
     "OptionConcurrencyDeg":    48,
     "OptionBCAfterTransaction":True  
     },
    '_BelowAverage':{
     "numberOfCells":           96,
     "randomSeed":              1000,
     "OptionStrategy":          'BelowAverage',
     "OptionTimebasedBC":       False,
     "OptionMinBC":             True,             
     "OptionMaxBC":             True,
     "OptionBCperiod":          15,
     "OptionLimitTimebasedBC":  False,
     "OptionLimitBound":        0.01,
     "OptionConcurrent":        True,
     "OptionConcurrencyDeg":    48,
     "OptionBCAfterTransaction":True  
     },
    '_Boundary':{
     "numberOfCells":           96,
     "randomSeed":              1000,
     "OptionStrategy":          'Boundary',
     "OptionTimebasedBC":       False,
     "OptionMinBC":             True,
     "OptionMaxBC":             True,
     "OptionBCperiod":          15,
     "OptionLimitTimebasedBC":  False,
     "OptionLimitBound":        0.01,
     "OptionConcurrent":        True,
     "OptionConcurrencyDeg":    48,
     "OptionBCAfterTransaction":True
     },
    '_NNMaxMin':{
     "numberOfCells":           96,
     "randomSeed":              1000,
     "OptionStrategy":          'NNMaxMin',
     "OptionTimebasedBC":       False,
     "OptionMinBC":             True,
     "OptionMaxBC":             True,
     "OptionBCperiod":          15,
     "OptionLimitTimebasedBC":  False,
     "OptionLimitBound":        0.01,
     "OptionConcurrent":        True,
     "OptionConcurrencyDeg":    48,
     "OptionBCAfterTransaction":True
     },
     '_NNModeBC': {
      "numberOfCells": 12,
      "randomSeed": 1000,
      "OptionStrategy": 'NNModeBC',
      "OptionTimebasedBC": False,
      "OptionMinBC": True,
      "OptionMaxBC": True,
      "OptionBCperiod": 15,
      "OptionLimitTimebasedBC": False,
      "OptionLimitBound": 0.01,
      "OptionConcurrent": True,
      "OptionConcurrencyDeg": 6,
      "OptionBCAfterTransaction": True
     },
     'NNAdapt': {
      "numberOfCells": 12,
      "randomSeed": 1000,
      "OptionStrategy": 'NNAdapt',
      "OptionTimebasedBC": False,
      "OptionMinBC": True,
      "OptionMaxBC": True,
      "OptionBCperiod": 15,
      "OptionLimitTimebasedBC": False,
      "OptionLimitBound": 0.01,
      "OptionConcurrent": True,
      "OptionConcurrencyDeg": 6,
      "OptionBCAfterTransaction": True
     },
     #disabled
    '_Passive_':{
     "numberOfCells":           96,
     "randomSeed":              1000,
     "OptionStrategy":          'Passive',
     "OptionTimebasedBC":       False,
     "OptionMinBC":             True,
     "OptionMaxBC":             True,
     "OptionBCperiod":          15,
     "OptionLimitTimebasedBC":  False,
     "OptionLimitBound":        0.01,
     "OptionConcurrent":        False,
     "OptionConcurrencyDeg":    24,
     "OptionBCAfterTransaction":True
     }}

    batchSimulator = BatchSimulate(batchSettings=batchSettings, packSettings=packSettings, balancingSettings=balancingSettings, paramsDict=paramsDict)
    
if ( __name__ == '__main__' ):
    run()

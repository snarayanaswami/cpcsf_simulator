'''
Created on 6 Apr 2015

@author: matthias.kauer
'''

# from ..transferdyn.dynconstpwm import DynConstPWM
# from ..transferdyn.dynconstpwm import DynConstPWM
from ..transferdyn import DynConstPWM, DynConstPWMStep, TransferResistance, TransferSpec
# from ..transferdyn.archkutkut import ArchKutkut
from ..transferdyn.timer import Timer
import numpy as np
import pandas as pd

def pwm_comparison(peakCurrents, socPairs, resistances, balTimes):
    dyns = []; stepdyns=[]
#         dp = DynBoxParams(Qfull=0.01*3600)
    
    for c in range(2):
        dyns.append(DynConstPWM(diodeCurrCorrection=True))
        stepdyns.append(DynConstPWMStep())
    
#     ret = np.zeros(2, dtype={'names':['col1', 'col2']})
#     ret.dtype.names = ('speedup', 'maxerr')
#     print(ret) 
    df = pd.DataFrame(index=balTimes, columns=['steptime', 'cftime', 'speedup', 'maxerr', 'maxrelerr'])
    for balTime in balTimes:
        maxerr = 0.0
        maxrelerr = 0.0
        steptime = 0.0
        cftime = 0.0
        num_sim = 0
        for socPair in socPairs:
            for iPeak in peakCurrents:
                for resistance in resistances:
                    num_sim += 1
                    assert isinstance(resistance, TransferResistance)
                    print(balTime, socPair, iPeak, resistance)
                    
                    for c in range(len(socPair)):
                        dyns[c].setSoc(socPair[c])
                        stepdyns[c].setSoc(socPair[c])
                        
#                         ts = TransferSpec(iPeakAmps=iPeak, send=0, recv=1, measure='T', amount=balTime)
#                     dyns[0].sendBalancing(toCell=dyns[1], resistances=resistance, iPeakAmps=iPeak, tSec=balTime)
                    with Timer() as tim:
                        dyns[0].sendBalancing(toCell=dyns[1], resistances=resistance, iPeakAmps=iPeak, tSec=balTime)
                    cftime += tim.interval
                    with Timer() as timstep:
                        stepdyns[0].sendBalancing(toCell=stepdyns[1], resistances=resistance, iPeakAmps=iPeak, tSec=balTime)
                    steptime += timstep.interval
                    
                    for c in range(2):
                        soccf = dyns[c].getSoc()
                        socstep = stepdyns[c].getSoc()
                        
                        err = abs(soccf - socstep)
                        maxerr = max(maxerr, err)
                    
                    dQCf = dyns[0].getCharge() - dyns[1].getCharge() 
                    dQStep = stepdyns[0].getCharge() - stepdyns[1].getCharge() 
                    
                    relerr = abs(dQCf - dQStep)/abs(dQStep)
                    maxrelerr = max(relerr, maxrelerr)
                    
        avg_speedup = steptime / cftime
        df.ix[balTime] = (steptime/num_sim, cftime/num_sim, avg_speedup, maxerr, maxrelerr)
        
    print(df)
    df.to_csv('pwm_comparison.csv', sep=';')
                        
                        
        
        
def run_med_pwmcomparison(): #about 10min on my machine
    resis = [TransferResistance(Rtx=1.3, Rrx=1.4),
        TransferResistance(Rtx=0.5, Rrx=0.6), TransferResistance(Rtx=0.01, Rrx=0.012)]
    peakCurrents = [0.25, 1.0, 2.0]
    socPairs = [(0.4,0.3), (0.8, 0.2), (0.45, 0.65)]
#     balTimes = [0.25, 1.0, 4.0, 10.0]
    balTimes = [10.0]
    
    pwm_comparison(peakCurrents=peakCurrents, socPairs=socPairs, resistances=resis, balTimes=balTimes)
    
def run_mini_pwmcomparison():
    resis = [TransferResistance(Rtx=0.5, Rrx=0.6)]
    
    pwm_comparison(peakCurrents=[1.0], socPairs=[(0.3, 0.4)], resistances=resis, balTimes=[0.25])
    
from smartcellsimulator.scsimu.GUI import *

def printUI(val):
    print('triggered')
    print(val)

#  if ( __name__ == '__main__' ):
def run():
    app = None
    if ( not app ):
        app = QtGui.QApplication([])
    
    window = MainWindow()
    #QtCore.QObject.connect(self.ui.pushButton, QtCore.SIGNAL("clicked()"), startSim_clicked)
    
#    window.connect(window.pushButton, QtCore.SIGNAL("clicked()"), window.startSim_clicked)
    window.pushButton.clicked.connect( window.startSim_clicked)
#    window.connect(window.btnSaveData, QtCore.SIGNAL("triggered()"), window.saveData)
    window.btnSaveData.triggered.connect( window.saveData)
#    window.connect(window.acnFollowCurve, QtCore.SIGNAL("triggered()"), window.followCurve)
    window.acnFollowCurve.triggered.connect( window.followCurve)
#    window.connect(window.acnCurveFollowSettings, QtCore.SIGNAL("triggered()"), window.setCurveFollowSettings)
    window.acnCurveFollowSettings.triggered.connect( window.setCurveFollowSettings)
#    window.connect(window.acnEvaluateDemonstrator, QtCore.SIGNAL("triggered()"), window.evaluateDemonstrator)
    window.acnEvaluateDemonstrator.triggered.connect( window.evaluateDemonstrator)
#    window.connect(window.acnEvaluationSettings, QtCore.SIGNAL("triggered()"), window.setEvaluationSettings)
    window.acnEvaluationSettings.triggered.connect( window.setEvaluationSettings)
#    window.connect(window.acnCondenseMetadata, QtCore.SIGNAL("triggered()"), window.condenseMetadata)
    window.acnCondenseMetadata.triggered.connect( window.condenseMetadata)
#    window.connect(window.acnSaveScatterplot, QtCore.SIGNAL("triggered()"), window.saveScatterplot)
    window.acnSaveScatterplot.triggered.connect( window.saveScatterplot)
        
#     window.connect(window.spbSeed, QtCore.SIGNAL("valueChanged()"), printUI)
    window.spbSeed.valueChanged.connect(printUI)
    
    window.show()
    
    if ( app ):
        app.exec_()
        
if ( __name__ == '__main__' ):
    run()
# Cyber-Physical Co-Simulation Framework (CPCSF) for Smart Cells in Scalable Battery Packs

Purpose
-----------
The Cyber-physical Co-Simulation Framework (CPCSF) has been developed for the system-level design and analysis of smart cells that enable scalable battery pack and Battery Management System (BMS) architectures. In contrast to conventional cells in battery packs, where all cells are monitored and controlled centrally, each smart cell is equipped with its own electronics in the form of a Cell Management Unit (CMU). The CMU maintains the cell in a safe and healthy operating state, while system-level battery management functions are performed by cooperation of the smart cells via communication. Here, the smart cells collaborate in a self-organizing fashion without a central controller instance. This enables maximum scalability and modularity, significantly simplifying integration of battery packs. However, for this emerging architecture, system-level design methodologies and tools have not been investigated yet. By contrast, components are developed individually and then manually tested in a hardware development platform. Consequently, the systematic design of the hardware/software architecture of smart cells requires a cyber-physical multi-level co-simulation of the network of smart cells which has to include all the components from the software, electronic, electric and electrochemical domains. This comprises distributed BMS algorithms running on the CMUs, the communication network, control circuitry, cell balancing hardware and battery cell behavior. For this purpose, we introduce the CPCSF which enables rapid design and analysis of smart cell hardware/software architectures. The CPCSF has been applied to investigate request-driven active cell balancing strategies that make use of the decentralized system architecture. If operated in parallel batch mode instead of performing single simulation runs in the Graphical User Interface (GUI), an exhaustive analysis can be performed on a realistic 21.6 kW h Electric Vehicle (EV) battery pack containing 96 smart cells in series. Here, the CPCSF is able to simulate hundreds of balancing runs together with all system characteristics, using the implemented request-driven balancing strategies at highest accuracy within an overall time frame of several hours. Consequently, the provided CPCSF for the first time allows to quantitatively and qualitatively analyze the behavior of smart cell architectures for real-world applications.

Technical Description and Reference
-----------
For a technical description of the simulator architecture and the active cell balancing strategies, please refer to the following publication:

*Sebastian Steinhorst, Matthias Kauer, Arne Meeuw, Swaminathan Narayanaswamy, Martin Lukasiewycz, Samarjit Chakraborty. "Cyber-Physical Co-Simulation Framework for Smart Cells in Scalable Battery Packs". In: ACM Transactions on Design Automation of Electronic Systems (TODAES), 21(4), pp. 62:1–62:26. DOI: [10.1145/2891407](http://dx.doi.org/10.1145/2891407)*

A PDF of this paper and further papers regarding smart battery cells and active cell balancing can be found here: [http://sebastian.steinhorst.info/publications](http://sebastian.steinhorst.info/publications).

In case you are using the simulator in your research and would like to cite it, please reference the paper as follows:

```
#!bibtex

@article{skmnlc:2016,
  author     = { Sebastian Steinhorst and Matthias Kauer and Arne Meeuw and Swaminathan Narayanaswamy and Martin Lukasiewycz and Samarjit Chakraborty },
  title      = { Cyber-Physical Co-Simulation Framework for Smart Cells in Scalable Battery Packs },
  journal    = { {ACM} Transactions on Design Automation of Electronic Systems (TODAES) },
  doi        = { 10.1145/2891407 },
  numpages   = { 26 },
  articleno  = { 62 },
  pages      = { 62:1--62:26 },
  issn       = { 1084-4309 },
  year       = { 2016 },
  month      = { 5 },
  number     = { 4 },
  volume     = { 21 },
  issue_date = { June 2016 },
}

```

Installation and Prerequisites
-----------
The simulator is implemented in Python 3 using the [SimPy discrete event simulation package](https://simpy.readthedocs.org/en/latest/) for the process-based Discrete-Event Simulation (DES) architecture.
After cloning the repository from https://bitbucket.org/steinhorst/cyber-physical-co-simulation-framework-for-smart-battery-cells.git we have to make sure there is a Python 3 installation on your system together with the required packages. As Python brings a comfortable package manager, this is easy to accomplish. After installing a Python 3 distribution, such as the Anaconda Python distribution for Windows from [https://www.continuum.io/downloads](https://www.continuum.io/downloads) (make sure you download the Python 3 version), the easiest way to install missing packages is to start the simulator GUI in the “bin” directory using the command
“python startGUI.py”.
Assuming that not all required packages are installed, python will report a missing module. In our case of using the Anaconda distribution on Windows, only [PyQtGraph](http://www.pyqtgraph.org) and the above mentioned SimPy were missing. They can be installed via the python package manager command line tool “pip” by executing:
```
pip pyqtgraph
```
and
```
pip simpy
```

###Used Libraries
* [SimPy](https://simpy.readthedocs.org/en/latest/)
* [transferdyn (statically integrated)](https://bitbucket.org/matthias_kauer/transferdyn)
* [PyQt](https://sourceforge.net/projects/pyqt/)
* [PyQtGraph](http://www.pyqtgraph.org/)


How to run
-----------
After installing Python and the required package dependencies, we can try again to start the simulator GUI in the ”bin” directory using the command
```
python startGUI.py
```
a GUI window should show up, which by pressing the “Start Simulation” button will run a first simulation which should look like this after it completed:

![CPCSF_Screenshot.png](https://bitbucket.org/repo/Abz4xd/images/299989761-CPCSF_Screenshot.png) 

Batch Simulation
-----------
In order to have a batch simulation across several random seed values and hence different battery cell SoC variations, you have to run the script “startParallelBatch.py” in the ”bin” folder with the argument of the number of overall runs. This number is the product of the range of random seeds and the number of strategies. If we want to batch-simulate 2 strategies, e.g., Below Average and MinMax, and set in “smartcellsimulator/scripts/startBatch.py” the seed range from 1000 to 1009, then it’s 10 runs per strategy and hence 20 runs overall. If you are on Windows, so it would be line 10 in the “startParallelBatch.py” file. On the command line, the command would be “python startParallelBatch.py 20”. In line 15, you have to specify the folder where to save the simulation results. The folder given must exist. That should get you going. On a linux machine, it’s lines 17 and 22 to modify, respectively.

License
-----------
MIT, Copyright (c) 2016 Sebastian Steinhorst, Matthias Kauer, Arne Meeuw, Swaminathan Narayanaswamy, Martin Lukasiewycz, Samarjit Chakraborty, TUM CREATE

Acknowledgement
-----------
The work for the initial release has been performed in [TUM CREATE](http://tum-create.edu.sg/) and was financially supported by the Singapore National Research Foundation under its Campus for Research Excellence and Technological Enterprise (CREATE) program.
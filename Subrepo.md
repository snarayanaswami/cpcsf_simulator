###Subrepo setup
Your first clone will fail with a 404 error in the subrepository. Fix your hgrc as described and ```hg update tip --clean``` to obtain a working copy.

####Our case
Since I had messed with the subrepo paths in the past, we need additional fixes to access old commits again.


Add trivial path to .hgsub (which is part of repo and should be defined already):
```
smartcellsimulator/transferdyn = smartcellsimulator/transferdyn
```
then specify in .hg/hgrc which won't be shared (I prefer local version inside the repository over user-wide, but both should work):
```
[subpaths]
^smartcellsimulator/transferdyn=https://bitbucket.org/matthias_kauer/transferdyn
https://bitbucket.org/matthias_kauer/CellTransferDynamicsSimple=https://bitbucket.org/matthias_kauer/transferdyn
https://bitbucket.org/matthias_kauer/celltransferdynamicssimple=https://bitbucket.org/matthias_kauer/transferdyn
```

During ```hg update``` to old commits, you may still have to tinker; I suggest the following:

* Delete subrepos first and/or use ```hg update --clean```
* Choose remote (i.e. of the commit you're updating to) when asked
* Consider deactivating keyring extension (created errors with complicated repo setups for me)

I tested for this a small number of commits along the tree. Let me (matthias) know if you still run into trouble anyway.

####Good idea for the future
If you must rename subrepos, don't rename them to another subrepo (transferdyn used to be tfdynsimple here).
Add trivial path to .hgsub:
```
smartcellsimulator/transferdyn = smartcellsimulator/transferdyn
```
then specify in .hg/hgrc:
```
[subpaths]
^smartcellsimulator/transferdyn=https://bitbucket.org/matthias_kauer/transferdyn
```

#### Documentation links
* [Official Subrepo documentation](http://mercurial.selenic.com/wiki/Subrepository)
* [Official Subrepo remapping plan (partially implemented now)](http://mercurial.selenic.com/wiki/SubrepoRemappingPlan)
* [Explanation for subpaths change in 2.0](http://selenic.com/hg/rev/91dc8878f888) - prefix w/ ^ if you don't want your path to be parsed prematurely (if https:// appears in center of your url, this is probably an issue); also see this [SO answer from mercurial developer](http://stackoverflow.com/a/9033610)


####Alternatives to subrepo

* [Hg guest repo extension](https://bitbucket.org/selinc/guestrepo) must be installed for all collaborators, I believe.
* [Projrc extension](http://mercurial.selenic.com/wiki/ProjrcExtension)


###Understanding how mercurial subrepositories work
[Official Subrepo documentation](http://mercurial.selenic.com/wiki/Subrepository)
In particular, I'd like to highlight the following.
- You can use your subrepo as you were used to. If you commit to this shell repo, a snapshot of the subrepo will be saved in '.hgsubstate'. This ensures that updating the outer (shell or top) repository leads to consistent versions among the subrepositories. On update, mercurial will try to pull the inner repos and update them accordingly. On push, all inner repos will be pushed as well.
- Don't commit code to the shell repo!
- It is a good idea to use "hg pull --update" when pulling the top repo (no personal experience, doc 2.5)

